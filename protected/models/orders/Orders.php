<?php


class Orders extends CActiveRecord
{
	public $columns = array();

	public function tableName()
	{
		return 'orders';
	}

	public function rules()
	{

		return array(
			array('user_id, current_status', 'required'),
			array('user_id, current_status', 'numerical', 'integerOnly' => true),
			array('id, user_id, manager_id, current_status', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'certificates' => array(self::HAS_MANY, 'OrderCertificates', 'order_id'),
			'statuses' => array(self::HAS_MANY, 'OrderStatuses', 'order_id'),
			'manager' => array(self::BELONGS_TO, 'Users', 'manager_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}


	public function attributeLabels()
	{
		return array(
			'id' => 'ID заказа',
			'user_id' => 'Пользователь',
			'manager_id' => 'Менеджер',
			'current_status' => 'Текущий статус',
		);
	}

	public function search()
	{

		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('manager_id', $this->manager_id);
		$criteria->compare('current_status', $this->current_status);

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function afterSave()
	{
		$status = new OrderStatuses();
		$status->order_id = $this->id;
		$status->status = $this->current_status;
		$status->date_add = date('Y-m-d H:i:s');
		$status->save();

	}
}
