<?php


class OrderStatuses extends CActiveRecord
{
	public function tableName()
	{
		return 'order_statuses';
	}

	public function rules()
	{
		return array(
			array('order_id, date_add', 'required'),
			array('order_id, status', 'numerical', 'integerOnly' => true),
			array('id, order_id, status, date_add', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'order' => array(self::BELONGS_TO, 'Orders', 'order_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'order_id' => 'Связь с таблицей заказов',
			'status' => 'статус заказа: 1 - Новый, 2 - Оплачен, 3 - Отказ',
			'date_add' => 'дата статуса',
		);
	}

	public function search()
	{

		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->compare('order_id', $this->order_id);
		$criteria->compare('status', $this->status);
		$criteria->compare('date_add', $this->date_add, true);

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
		$this->date_add = date('Y-m-d H:i:s');

		return true;
	}
}
