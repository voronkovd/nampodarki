<?php

class OrderCertificates extends CActiveRecord
{

	public function tableName()
	{
		return 'order_certificates';
	}

	public function rules()
	{

		return array(
			array('order_id, price_id', 'required'),
			array('order_id, certificate_id, price_id', 'numerical', 'integerOnly' => true),
			array('id, order_id, certificate_id, price_id, price_value', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'certificate' => array(self::BELONGS_TO, 'Certificates', 'certificate_id'),
			'order' => array(self::BELONGS_TO, 'Orders', 'order_id'),
			'price' => array(self::BELONGS_TO, 'CertificatePrices', 'price_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'order_id' => 'Связь с таблицей заказов',
			'certificate_id' => 'Связь с таблицей сертификатов',
			'price_id' => 'Связь с таблицей цен',
		);
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->compare('order_id', $this->order_id);
		$criteria->compare('certificate_id', $this->certificate_id);
		$criteria->compare('price_id', $this->price_id);

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
