<?php

class PartnerGalleries extends CActiveRecord
{

	public $file_names = array();

	public $columns = array();

	public function tableName()
	{
		return 'partner_galleries';
	}

	public function rules()
	{
		return array(
			array('partner_id', 'numerical', 'integerOnly' => true),
			array('file_name', 'file', 'types' => 'jpg,png,gif', 'allowEmpty' => false),
			array('id, file_name, partner_id', 'safe', 'on' => 'search'),
		);
	}


	public function relations()
	{
		return array('partner' => array(self::BELONGS_TO, 'Partners', 'partner_id'));
	}

	public function attributeLabels()
	{
		return array(
			'file_names' => 'Изображение (одно или несколько)',
			'partner_id' => 'Партнер',
			'file_name' => 'Изображение'
		);
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		if (Yii::app()->user->role != Users::ADMIN || Yii::app()->user->role != Users::MANAGER) {
			$user = Users::model()->findByPk(Yii::app()->user->id);
			$criteria->compare('t.partner_id', $user->partner_id);
		}

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
		if ($this->partner_id == 0) {
			$this->partner_id = null;
		}

		return true;
	}

	public function beforeDelete()
	{
		$path = Yii::app()->getBasePath() . '/../images/gallery/';
		unlink($path . $this->file_name);

		return true;
	}

	public function getImage($options = array())
	{
		$return = null;
		if (!empty($this->file_name)) {
			$url = Yii::app()->baseUrl . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'gallery'
				. DIRECTORY_SEPARATOR;
			$return = CHtml::image($url . $this->file_name, '', $options);
		}

		return $return;
	}

	public function image($width = 100, $height = 100, $method = 'adaptiveResize')
	{
		if ($this->image) {
			return CHtml::image(
				Yii::app()->baseUrl . '/' .
				ImageHelper::thumb(
					$width,
					$height,
					'images/gallery/' . $this->image,
					array('method' => $method)
				)
			);
		}
	}
}
