<?php

class Partners extends CActiveRecord
{
	public $columns = array();

	public function tableName()
	{
		return 'partners';
	}

	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'unique'),
			array('name', 'length', 'min' => 3, 'max' => 100),
			array('status', 'numerical', 'integerOnly' => true, 'max' => 2, 'min' => 1),
			array('image_name', 'file', 'types' => 'jpg', 'allowEmpty' => false),
			array('id, name, status', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'certificates' => array(self::HAS_MANY, 'Certificates', 'partner_id'),
			'news' => array(self::HAS_MANY, 'News', 'partner_id'),
			'galleries' => array(self::HAS_MANY, 'PartnerGalleries', 'partner_id'),
			'sellouts' => array(self::HAS_MANY, 'Sellouts', 'partner_id'),
			'users' => array(self::HAS_MANY, 'Users', 'partner_id'),
		);
	}

	public function attributeLabels()
	{
		return array('name' => 'Наименование', 'image_name' => 'Логотип', 'status' => 'Активен');
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->select = 't.id, t.name, t.image_name, t.status';
		$criteria->with = array(
			'certificates' => array('select' => 'certificates.id'),
			'news' => array('select' => 'news.id'),
			'galleries' => array('select' => 'galleries.id'),
			'sellouts' => array('select' => 'sellouts.id'),
			'users' => array('select' => 'users.id'),
		);
		$criteria->compare('t.name', $this->name, true, true);
		$criteria->compare('t.status', $this->status);

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public function scopes()
	{
		return array(
			'public' => array(
				'select' => 'id, name, image_name',
				'condition' => 'status = ' . Yii::app()->params['visible_status_yes']
			),
			'filter' => array('select' => 'id, name')
		);
	}

	public function getImage($options = array())
	{
		$return = null;
		if (!empty($this->image_name)) {
			$url = Yii::app()->baseUrl . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'partners'
				. DIRECTORY_SEPARATOR;
			$return = CHtml::image($url . $this->image_name, $this->name, $options);
		}

		return $return;
	}

	public function  countAll($column)
	{
		if (isset($column)) {
			return count($column);
		} else {
			return 0;
		}
	}

	public function image($width = 100, $height = 100, $method = 'adaptiveResize')
	{
		if ($this->image) {
			return CHtml::image(
				Yii::app()->baseUrl . '/' .
				ImageHelper::thumb(
					$width,
					$height,
					'images/partners/' . $this->image,
					array('method' => $method)
				),
				$this->name
			);
		}
	}
}