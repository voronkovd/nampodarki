<?php

class CertificatePriceRelations extends CActiveRecord
{

	public function tableName()
	{
		return 'certificate_price_relations';
	}

	public function rules()
	{
		return array(
			array('price_id, certificate_id', 'required'),
			array('price_id, certificate_id', 'numerical', 'integerOnly' => true),
			array('id, price_id, certificate_id', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'certificate' => array(self::BELONGS_TO, 'Certificates', 'certificate_id'),
			'price' => array(self::BELONGS_TO, 'CertificatePrices', 'price_id'),
		);
	}

	public function attributeLabels()
	{
		return array();
	}


	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->compare('price_id', $this->price_id);
		$criteria->compare('certificate_id', $this->certificate_id);

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function add($keys, $id)
	{
		$keys_array = explode(',', $keys);
		$criteria = new CDbCriteria();
		$criteria->compare('certificate_id', $id);
		self::model()->deleteAll($criteria);
		if (!empty($keys)) {
			foreach ($keys_array as $key => $value) {
				$model = new CertificatePrices();
				$model->price = trim($value);
				if ($model->save()) {
					$add = new self;
					$add->certificate_id = $id;
					$add->price_id = $model->getPrimaryKey();
					$add->save();
				} else {
					$criteria = new CDbCriteria();
					$criteria->compare('price', trim($value));
					$k = CertificatePrices::model()->find($criteria);
					$add = new self;
					$add->certificate_id = $id;
					$add->price_id = $k->id;
					$add->save();
				}
			}
		}
	}

}