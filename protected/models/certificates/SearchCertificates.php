<?php

class SearchCertificates extends CFormModel
{
	public $start;
	public $end;

	public function rules()
	{
		return array(
			array('start', 'required'),
			array('start, end', 'numerical', 'integerOnly' => true),
		);
	}

	public function  getResult()
	{
		$criteria = new CDbCriteria();
		$criteria->addBetweenCondition('price', $this->start, $this->end, 'AND');
		$prices = CertificatePrices::model()->findAll($criteria);
		$certificate_ids = array();
		foreach ($prices as $price) {
			if (!empty($price->certificate)) {
				foreach ($price->certificate as $certificate) {
					$certificate_ids[] = $certificate->id;
				}
			}
		}
		$criteria = new CDbCriteria;
		$criteria->select = 't.*';
		$criteria->addInCondition('t.id', $certificate_ids, 'AND');
		$criteria->compare('t.status', Yii::app()->params['visible_status_yes']);
		$criteria->order = 't.id';

		return new CActiveDataProvider('Certificates', array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => 25)
		));
	}

}