<?php

class WhenIsCertificateRelations extends CActiveRecord
{

	public function tableName()
	{
		return 'when_is_certificate_relations';
	}

	public function rules()
	{
		return array(
			array('when_id, certificate_id', 'required'),
			array('when_id, certificate_id', 'numerical', 'integerOnly' => true),
			array('id, when_id, certificate_id', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'certificate' => array(self::BELONGS_TO, 'Certificates', 'certificate_id'),
			'when' => array(self::BELONGS_TO, 'WhenIsCertificates', 'when_id'),
		);
	}

	public function attributeLabels()
	{
		return array();
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
