<?php

class WhoIsCertificateRelations extends CActiveRecord
{

	public function tableName()
	{
		return 'who_is_certificate_relations';
	}

	public function rules()
	{
		return array(
			array('who_id, certificate_id', 'required'),
			array('who_id, certificate_id', 'numerical', 'integerOnly' => true),
			array('id, who_id, certificate_id', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'certificate' => array(self::BELONGS_TO, 'Certificates', 'certificate_id'),
			'who' => array(self::BELONGS_TO, 'WhoIsCertificates', 'who_id'),
		);
	}

	public function attributeLabels()
	{
		return array();
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}