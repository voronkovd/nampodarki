<?php

class WhenIsCertificates extends CActiveRecord
{
	public $columns = array();

	public function tableName()
	{
		return 'when_is_certificates';
	}

	public function rules()
	{
		return array(
			array('name, date_when', 'required'),
			array('name', 'length', 'max' => 100),
			array('id, name, date_when', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array('whenIsRelations' => array(self::HAS_MANY, 'WhenIsCertificateRelations', 'when_id'));
	}

	public function attributeLabels()
	{
		return array('name' => 'Тэг когда', 'date_when' => 'Дата');
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->select = 't.id, t.name, t.date_when';
		$criteria->compare('name', $this->name, true);
		$criteria->compare('date_when', $this->date_when, true);

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function beforeDelete()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('when_id', $this->id);
		WhenIsCertificateRelations::model()->deleteAll($criteria);

		return true;
	}

	public static function getAll()
	{
		$return = array();
		$model = self::model()->findAll();
		if (!empty($model)) {
			foreach ($model as $data) {
				$return[$data['id']] = $data['name'] . ' (' . $data['date_when'] . ')';
			}
		}

		return $return;
	}
}