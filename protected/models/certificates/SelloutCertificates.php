<?php

class SelloutCertificates extends CActiveRecord
{
	public $certificates = array();

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'sellout_certificates';
	}

	public function rules()
	{
		return array(
			array('sellout_id, certificate_price_relation_id', 'required'),
			array('sellout_id, certificate_price_relation_id', 'numerical', 'integerOnly' => true),
			array('id, sellout_id, certificate_price_relation_id', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'certificate' => array(self::BELONGS_TO, 'CertificatePriceRelations', 'certificate_price_relation_id'),
			'sellout' => array(self::BELONGS_TO, 'Sellouts', 'sellout_id'),
		);
	}

	public function attributeLabels()
	{
		return array('sellout_id' => 'Sellout', 'certificates' => 'Сертификаты');
	}

	public function search()
	{

		$criteria = new CDbCriteria;
		$criteria->compare('sellout_id', $this->sellout_id);
		$criteria->compare('certificate_price_relation_id', $this->certificate_id);

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}
}