<?php

class Categories extends CActiveRecord
{
	public $columns = array();

	public function tableName()
	{
		return 'categories';
	}

	public function rules()
	{

		return array(
			array('name', 'required'),
			array('status', 'numerical', 'integerOnly' => true),
			array('name', 'length', 'max' => 100, 'min' => 3),
			array('id, name, status', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{

		return array('whenIsRelations' => array(self::HAS_MANY, 'CertificateCategories', 'category_id'));
	}

	public function attributeLabels()
	{
		return array('name' => 'Наименование категории', 'status' => 'Публично');
	}


	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->select = 't.id, t.name, t.status';
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('t.status', $this->status);

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function scopes()
	{
		return array(
			'public' => array(
				'select' => 'id, name',
				'condition' => 'status = ' . Yii::app()->params['visible_status_yes']
			)
		);
	}

	public function beforeDelete()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('certificate_categories_id', $this->id);
		Certificates::model()->deleteAll($criteria);

		return true;
	}

	public static function getAll()
	{
		$return = array();
		$model = self::model()->findAll();
		if (!empty($model)) {
			foreach ($model as $data) {
				$return[$data['id']] = $data['name'];
			}
		}

		return $return;
	}
}