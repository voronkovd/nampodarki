<?php

class CertificatePrices extends CActiveRecord
{
	public $columns = array();

	public function tableName()
	{
		return 'certificate_prices';
	}

	public function rules()
	{
		return array(
			array('price', 'numerical'),
			array('price', 'unique'),
			array('id, price', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{

		return array(
			'certificate' => array(self::HAS_MANY, 'CertificatePriceRelations', 'price_id'),
			'order' => array(self::HAS_MANY, 'OrderCertificates', 'price_id'),
		);
	}

	public function attributeLabels()
	{
		return array('price' => 'Цена');
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('price', $this->price);

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function beforeDelete()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('price_id', $this->id);
		CertificatePriceRelations::model()->deleteAll($criteria);
		OrderCertificates::model()->deleteAll($criteria);

		return true;
	}

	public function suggestTags($keyword, $limit = 20)
	{
		$tags = $this->findAll(
			array(
				'condition' => 'price LIKE :keyword',
				'order' => 'price',
				'limit' => $limit,
				'params' => array(
					':keyword' => '%' . strtr($keyword, array('%' => '\%', '_' => '\_', '\\' => '\\\\')) . '%',
				),
			)
		);
		$names = array();
		foreach ($tags as $tag) {
			$names[] = $tag->price;
		}

		return $names;
	}
}