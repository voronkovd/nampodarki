<?php

class Sellouts extends CActiveRecord
{

	public $columns = array();

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'sellouts';
	}

	public function rules()
	{
		return array(
			array('name, lifetime', 'required'),
			array('discount, status', 'numerical', 'integerOnly' => true),
			array('name', 'length', 'max' => 100),
			array('anons', 'length', 'max' => 255),
			array('image', 'file', 'types' => 'jpg, png, gif', 'allowEmpty' => false),
			array(
				'id, name, anons, image, discount, status, date_add, lifetime, partner_id, user_id',
				'safe',
				'on' => 'search'
			),
		);
	}

	public function relations()
	{
		return array(
			'certificates' => array(self::HAS_MANY, 'SelloutCertificates', 'sellout_id'),
			'partnerUser' => array(self::BELONGS_TO, 'PartnerUsers', 'partner_user_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'name' => 'Название акции',
			'anons' => 'Анонс',
			'image' => 'Изображение',
			'discount' => 'Скидка (%)',
			'status' => 'Статус',
			'date_add' => 'Дата добавления',
			'lifetime' => 'Дата окончания акции',
			'partner_id' => 'Партнер',
		);
	}

	public function search()
	{

		$criteria = new CDbCriteria;
		$criteria->compare('name', $this->name, true);
		$criteria->compare('anons', $this->anons, true);
		$criteria->compare('image', $this->image, true);
		$criteria->compare('discount', $this->discount);
		$criteria->compare('status', $this->status);
		$criteria->compare('date_add', $this->date_add, true);
		$criteria->compare('lifetime', $this->lifetime, true);
		$criteria->compare('user_id', $this->user_id);
		if (Yii::app()->user->role != Users::ADMIN || Yii::app()->user->role != Users::MANAGER) {
			$user = Users::model()->findByPk(Yii::app()->user->id);
			$criteria->compare('partner_id', $user->partner_id);
		}

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public static function getLastThree()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('status', Yii::app()->params['visible_status_yes']);
		$criteria->limit = 3;
		$criteria->order = 'id DESC';

		return self::model()->findAll($criteria);
	}

	public function beforeSave()
	{
		if ($this->isNewRecord) {
			$this->date_add = date('Y-m-d H:i:s');
			$this->user_id = Yii::app()->user->id;
		}
		if ($this->partner_id == 0) {
			$this->partner_id = null;
		}

		return true;
	}

	public function getImage($options = array())
	{
		$return = null;
		if (!empty($this->image)) {
			$url = Yii::app()->baseUrl . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'sellouts'
				. DIRECTORY_SEPARATOR;
			$return = CHtml::image($url . $this->image, $this->name, $options);
		}

		return $return;
	}

	public function beforeDelete()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('sellout_id', $this->id);
		SelloutCertificates::model()->deleteAll($criteria);
		$path = Yii::app()->getBasePath() . '/../images/sellouts/';
		unlink($path . $this->image);

		return true;
	}

	public function image($width = 100, $height = 100, $method = 'adaptiveResize')
	{
		if ($this->image) {
			return CHtml::image(
				Yii::app()->baseUrl . '/' .
				ImageHelper::thumb(
					$width,
					$height,
					'images/sellouts/' . $this->image,
					array('method' => $method)
				),
				$this->name
			);
		}
	}
}