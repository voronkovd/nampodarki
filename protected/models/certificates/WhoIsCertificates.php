<?php

class WhoIsCertificates extends CActiveRecord
{
	public $columns = array();

	public function tableName()
	{
		return 'who_is_certificates';
	}

	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'length', 'max' => 100),
			array('id, name', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{

		return array('whoIsRelations' => array(self::HAS_MANY, 'WhoIsCertificateRelations', 'who_id'));
	}

	public function attributeLabels()
	{
		return array('name' => 'Тэг Кому');
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->select = 't.id, t.name';
		$criteria->compare('name', $this->name, true);

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function beforeDelete()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('who_id', $this->id);
		WhoIsCertificateRelations::model()->deleteAll($criteria);

		return true;
	}
}