<?php

class CertificateCategories extends CActiveRecord
{

	public function tableName()
	{
		return 'certificate_categories';
	}

	public function rules()
	{
		return array(
			array('category_id, certificate_id', 'required'),
			array('category_id, certificate_id', 'numerical', 'integerOnly' => true),
			array('id, category_id, certificate_id', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'certificate' => array(self::BELONGS_TO, 'Certificates', 'certificate_id'),
			'category' => array(self::BELONGS_TO, 'Categories', 'category_id'),
		);
	}

	public function attributeLabels()
	{
		return array();
	}

	public function search()
	{

		$criteria = new CDbCriteria;

		return new CActiveDataProvider($this, array('criteria' => $criteria));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
