<?php

class Certificates extends CActiveRecord
{
	public $columns = array();
	public $who = array();
	public $when = array();
	public $category = array();
	public $prices = array();

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'certificates';
	}

	public function rules()
	{
		return array(
			array('name, lifetime', 'required'),
			array('status, delivery, partner_id', 'numerical', 'integerOnly' => true),
			array('name', 'length', 'max' => 100),
			array('desc', 'length', 'max' => 255),
			array('image', 'file', 'types' => 'jpg, png, gif', 'allowEmpty' => false),
			array(
				'id, name, desc, image, status, delivery, date_add, lifetime, partner_id, user_id',
				'safe',
				'on' => 'search'
			)
		);
	}

	public function relations()
	{

		return array(
			'price' => array(self::HAS_MANY, 'CertificatePriceRelations', 'certificate_id'),
			'categories' => array(self::HAS_MANY, 'CertificateCategories', 'certificate_id'),
			'partner' => array(self::BELONGS_TO, 'Partners', 'partner_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'order' => array(self::HAS_MANY, 'OrderCertificates', 'certificate_id'),
			'sellout' => array(self::HAS_MANY, 'SelloutCertificates', 'certificate_id'),
			'whenIs' => array(self::HAS_MANY, 'WhenIsCertificateRelations', 'certificate_id'),
			'whoIs' => array(self::HAS_MANY, 'WhoIsCertificateRelations', 'certificate_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'name' => 'Наименование',
			'desc' => 'Описание',
			'image' => 'Изображение',
			'status' => 'Активен',
			'delivery' => 'Наличие доставки',
			'date_add' => 'Дата добавления',
			'lifetime' => 'Дата окончания',
			'category' => 'Категория',
			'partner_id' => 'Партнер',
			'who' => 'Тэг "Кому"',
			'when' => 'Тэг "Когда"',
			'prices' => 'Цены'

		);
	}

	public function search()
	{

		$criteria = new CDbCriteria;
		$criteria->select = 't.*';
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('t.desc', $this->desc, true);
		$criteria->compare('t.status', $this->status);
		$criteria->compare('t.delivery', $this->delivery);
		$criteria->compare('t.date_add', $this->date_add, true);
		$criteria->compare('t.lifetime', $this->lifetime, true);
		if (Yii::app()->user->role != Users::ADMIN || Yii::app()->user->role != Users::MANAGER) {
			$user = Users::model()->findByPk(Yii::app()->user->id);
			$criteria->compare('t.partner_id', $user->partner_id);
		}
		$criteria->order = 't.id';

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public function beforeSave()
	{
		if ($this->isNewRecord) {
			$this->date_add = date('Y-m-d H:i:s');
			$this->user_id = Yii::app()->user->id;
		}
		if ($this->partner_id == 0) {
			$this->partner_id = null;
		}

		return true;
	}

	public function afterSave()
	{

		$criteria = new CDbCriteria();
		$criteria->compare('certificate_id', $this->id);
		WhenIsCertificateRelations::model()->deleteAll($criteria);
		foreach ($this->when as $key => $whe) {
			if (!empty($whe)) {
				$when = new WhenIsCertificateRelations();
				$when->when_id = $whe;
				$when->certificate_id = $this->id;
				$when->save();
			}
		}
		WhoIsCertificateRelations::model()->deleteAll($criteria);
		foreach ($this->who as $key => $who) {
			if (!empty($who)) {
				$when = new WhoIsCertificateRelations();
				$when->who_id = $who;
				$when->certificate_id = $this->id;
				$when->save();
			}
		}
		foreach ($this->category as $key => $category) {
			if (!empty($category)) {
				$when = new CertificateCategories();
				$when->category_id = $category;
				$when->certificate_id = $this->id;
				$when->save();
			}
		}

		return true;
	}

	public function scopes()
	{
		if (Yii::app()->user->role == Users::PARTNER_MANAGER || Yii::app()->user->role == Users::PARTNER_ADMIN) {
			$user = Users::model()->findByPk(Yii::app()->user->id);
			$condition = 'status = ' . Yii::app(
				)->params['visible_status_yes'] . ' AND partner_id=' . $user->partner_id;
		} else {
			$condition = 'status = ' . Yii::app()->params['visible_status_yes'];
		}

		return array(
			'public' => array(
				'select' => 'id, name',
				'condition' => $condition
			)
		);
	}

	public function getImage($options = array())
	{
		$return = null;
		if (!empty($this->image)) {
			$url = Yii::app()->baseUrl . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'certificates'
				. DIRECTORY_SEPARATOR;
			$return = CHtml::image($url . $this->image, $this->name, $options);
		}

		return $return;
	}

	public function beforeDelete()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('certificate_id', $this->id);
		CertificatePriceRelations::model()->deleteAll($criteria);
		OrderCertificates::model()->deleteAll($criteria);
		SelloutCertificates::model()->deleteAll($criteria);
		WhenIsCertificateRelations::model()->deleteAll($criteria);
		WhoIsCertificateRelations::model()->deleteAll($criteria);
		$path = Yii::app()->getBasePath() . '/../images/certificates/';
		unlink($path . $this->image);

		return true;
	}

	public function getPrices()
	{
		if (!empty($this->price)) {
			$key = array();
			foreach ($this->price as $keys) {
				$key[] = $keys->price->price;
			}
			$keys = null;
			$keys = implode(', ', $key);
		} else {
			$keys = '';
		}

		$this->prices = $keys;
	}

	public function image($width = 100, $height = 100, $method = 'adaptiveResize')
	{
		return CHtml::image(
			Yii::app()->baseUrl . '/' .
			ImageHelper::thumb(
				$width,
				$height,
				'images/certificates/' . $this->image,
				array('method' => $method)
			),
			$this->name
		);
	}
}