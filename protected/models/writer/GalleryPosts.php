<?php


class GalleryPosts extends CActiveRecord
{

	public function tableName()
	{
		return 'gallery_posts';
	}

	public function rules()
	{

		return array(
			array('posts_id, image_name', 'required'),
			array('posts_id, status', 'numerical', 'integerOnly' => true),
			array('image_name', 'length', 'max' => 36),
			array('id, posts_id, image_name, status', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array('posts' => array(self::BELONGS_TO, 'Posts', 'posts_id'));
	}


	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'posts_id' => 'Связь с таблицей партнеров',
			'image_name' => 'Изображения галлереи',
			'status' => 'Статус: 1 - активен, 2 - заблокирован',
		);
	}


	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->compare('posts_id', $this->posts_id);
		$criteria->compare('image_name', $this->image_name, true);
		$criteria->compare('status', $this->status);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getImage($width = 100, $height = 100, $method = 'adaptiveResize')
	{
		return CHtml::image(
			Yii::app()->baseUrl . '/' . ImageHelper::thumb(
				$width,
				$height,
				'images/gallery/' . $this->image_name,
				array('method' => $method)
			)
		);
	}
}
