<?php

class Commentaries extends CActiveRecord
{
	public $columns = array();

	public function tableName()
	{
		return 'commentaries';
	}

	public function rules()
	{
		return array(
			array('content, date_add', 'required'),
			array('parent_id, user_id', 'numerical', 'integerOnly' => true),
			array('id, parent_id, content, date_add, user_id', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'parent' => array(self::BELONGS_TO, 'Commentaries', 'parent_id')
		);
	}

	public function attributeLabels()
	{
		return array(
			'parent_id' => 'Родитель комментария',
			'content' => 'Комментарий',
			'date_add' => 'Дата добавления',
			'user_id' => 'Пользователь',
		);
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->select = 't.id, t.parent_id, t.content, t.date_add';
		$criteria->with = array('user');
		$criteria->compare('id', $this->id);
		$criteria->compare('parent_id', $this->parent_id);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('date_add', $this->date_add, true);
		$criteria->compare('user_id', $this->user_id);

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public function beforeSave()
	{
		$this->date_add = date('Y-m-d H:i:s');

		return true;
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getParent()
	{
		if ($this->parent_id == 0) {
			return '';
		} else {
			return UrlHelper::cutString($this->parent->content);
		}
	}
}
