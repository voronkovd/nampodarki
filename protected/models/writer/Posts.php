<?php


class Posts extends CActiveRecord
{
	public $columns = array();

	public function tableName()
	{
		return 'posts';
	}

	public function rules()
	{
		return array(
			array('title, anons, content', 'required'),
			array('title', 'length', 'max' => 50),
			array('anons', 'length', 'max' => 128),
			array('image', 'file', 'types' => 'jpg, png, gif', 'allowEmpty' => true),
			array('id, title, anons, content, date_add, image', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array();
	}

	public function attributeLabels()
	{
		return array(
			'title' => 'Заголовок',
			'anons' => 'Анонс',
			'content' => 'Текст статьи',
			'date_add' => 'Дата добавления',
			'image' => 'Изображение'
		);
	}


	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->select = 't.id, t.title, t.anons, t.content, t.date_add';
		$criteria->compare('id', $this->id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('anons', $this->anons, true);
		$criteria->compare('t.image', $this->image, true);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('date_add', $this->date_add, true);

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
		if ($this->isNewRecord) {
			$this->date_add = date('Y-m-d H:i:s');
		}

		return true;
	}

	public function getDistribution()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'DATE(date_add)=' . date('Y-m-d');

		return self::model()->findAll($criteria);
	}

	public static function getLastThree()
	{
		$criteria = new CDbCriteria;
		$criteria->limit = 3;
		$criteria->order = 'id DESC';

		return self::model()->findAll($criteria);
	}

	public function image($width = 100, $height = 100, $method = 'adaptiveResize')
	{
		if ($this->image) {
			return CHtml::image(
				Yii::app()->baseUrl . '/' .
				ImageHelper::thumb(
					$width,
					$height,
					'images/posts/' . $this->image,
					array('method' => $method)
				),
				$this->title
			);
		}
	}

	public function getImages()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('news_id', $this->id);

		return new CActiveDataProvider('GalleryPosts', array(
			'criteria' => $criteria,
		));
	}
}
