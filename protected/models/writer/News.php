<?php

class News extends CActiveRecord
{
	public $columns = array();

	public function tableName()
	{
		return 'news';
	}

	public function rules()
	{
		return array(
			array('title, anons, content, date_add', 'required'),
			array('partner_id', 'numerical', 'integerOnly' => true),
			array('title', 'length', 'max' => 50),
			array('anons', 'length', 'max' => 128),
			array('image', 'file', 'types' => 'jpg, png, gif', 'allowEmpty' => True),
			array('id, title, anons, content, date_add, partner_id, image', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'partner' => array(self::BELONGS_TO, 'Partners', 'partner_id'),
			'galleries' => array(self::HAS_MANY, 'GalleryNews', 'news_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'title' => 'Заголовок',
			'anons' => 'Анонс',
			'content' => 'Текст новости',
			'date_add' => 'Дата добавления',
			'partner_id' => 'Партнер',
			'image' => 'Изображение'
		);
	}


	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->select = 't.id, t.title, t.anons, t.date_add';
		$criteria->with = array(
			'partner' => array('select' => 'name')
		);
		$criteria->compare('t.title', $this->title, true);
		$criteria->compare('t.anons', $this->anons, true);
		$criteria->compare('t.image', $this->image, true);
		$criteria->compare('t.date_add', $this->date_add, true);
		if (Yii::app()->user->role != Users::ADMIN || Yii::app()->user->role != Users::MANAGER) {
			$user = Users::model()->findByPk(Yii::app()->user->id);
			$criteria->compare('t.partner_id', $user->partner_id);
		}

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
		if ($this->partner_id == '' || empty($this->partner_id)) {
			$this->partner_id = null;
		}

		return true;
	}

	public function getDistribution()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'DATE(date_add)=' . date('Y-m-d');

		return self::model()->findAll($criteria);
	}

	public static function getLastThree()
	{
		$criteria = new CDbCriteria;
		$criteria->limit = 3;
		$criteria->order = 'id DESC';

		return self::model()->findAll($criteria);
	}

	public function image($width = 100, $height = 100, $method = 'adaptiveResize')
	{
		if ($this->image) {
			return CHtml::image(
				Yii::app()->baseUrl . '/' .
				ImageHelper::thumb(
					$width,
					$height,
					'images/news/' . $this->image,
					array('method' => $method)
				),
				$this->title
			);
		}
	}

	public function getImages()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('news_id', $this->id);

		return new CActiveDataProvider('GalleryNews', array(
			'criteria' => $criteria,
		));
	}
}
