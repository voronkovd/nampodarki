<?php

class Users extends CActiveRecord
{
	const AUTH_USER = 1;
	const MANAGER = 2;
	const PARTNER_MANAGER = 3;
	const PARTNER_ADMIN = 4;
	const ADMIN = 5;
	public $columns = array();

	public function tableName()
	{
		return 'users';
	}

	public function rules()
	{

		return array(
			array('email', 'unique'),
			array('email, password', 'required'),
			array('email', 'email'),
			array('status, partner_id', 'numerical', 'integerOnly' => true),
			array('email', 'length', 'max' => 100),
			array('password', 'length', 'max' => 32),
			array('id, email, role, status, partner_id', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{

		return array(
			'certificates' => array(self::HAS_MANY, 'Certificates', 'user_id'),
			'commentaries' => array(self::HAS_MANY, 'Commentaries', 'user_id'),
			'ordersManager' => array(self::HAS_MANY, 'Orders', 'manager_id'),
			'ordersUser' => array(self::HAS_MANY, 'Orders', 'user_id'),
			'sellouts' => array(self::HAS_MANY, 'Sellouts', 'user_id'),
			'profile' => array(self::HAS_ONE, 'UserProfiles', 'user_id'),
			'partner' => array(self::BELONGS_TO, 'Partners', 'partner_id'),
		);
	}

	public function attributeLabels()
	{
		return array('email' => 'Email', 'password' => 'Пароль', 'role' => 'Роль', 'status' => 'Активен');
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->select = 't.id, t.email, t.role, t.status, t.partner_id';
		$criteria->with = array(
			'profile' => array('select' => 'profile.surname, profile.name, profile.patronymic, profile.phone'),
			'partner' => array('select' => 'partner.name'),
		);
		$criteria->compare('t.email', $this->email, true);
		$criteria->compare('t.role', $this->role);
		$criteria->compare('t.status', $this->status);
		if (Yii::app()->user->role != Users::ADMIN || Yii::app()->user->role != Users::MANAGER) {
			$user = self::model()->findByPk(Yii::app()->user->id);
			$criteria->compare('t.partner_id', $user->partner_id);
		}

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
		if (strlen($this->password) != 32) {
			$this->password = md5(Yii::app()->params['salt'] . $this->password);
		}

		return parent::beforeSave();
	}
}
