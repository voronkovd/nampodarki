<?php


class RegistrationForm extends CFormModel
{
	public $email;
	public $password;
	public $surname;
	public $name;
	public $patronymic;
	public $phone;
	public $role;


	public function rules()
	{
		return array(
			array('email, password, surname, name, patronymic, phone', 'required'),
			array('email', 'email'),
			array('password', 'length', 'min' => 6, 'max' => 12),
			array('surname', 'length', 'min' => 2, 'max' => 20),
			array('name', 'length', 'min' => 2, 'max' => 15),
			array('patronymic', 'length', 'min' => 6, 'max' => 15),
			array('phone', 'length', 'min' => 11, 'max' => 11),
			array('phone, role', 'numerical', 'integerOnly' => true),
		);
	}

	public function attributeLabels()
	{
		return array(
			'email' => 'Email',
			'password' => 'Пароль',
			'surname' => 'Фамилия',
			'name' => 'Имя',
			'patronymic' => 'Отчество',
			'phone' => 'Телефон (89110000000)',
			'role' => 'Роль',

		);
	}

	public function SaveUser()
	{
		$currentUser = Users::model()->findByPk(Yii::app()->user->id);
		$user = new Users();
		$user->email = $this->email;
		$user->password = $this->password;
		$user->role = $this->role;
		$user->partner_id = $currentUser->partner_id;
		$user->save();
		$profile = new UserProfiles();
		$profile->surname = $this->surname;
		$profile->name = $this->name;
		$profile->patronymic = $this->patronymic;
		$profile->phone = $this->phone;
		$profile->user_id = $user->id;
		$profile->save();
	}

	public function RegisterUser()
	{
		$user = new Users();
		$user->email = $this->email;
		$user->password = $this->password;
		$user->save();
		$profile = new UserProfiles();
		$profile->surname = $this->surname;
		$profile->name = $this->name;
		$profile->patronymic = $this->patronymic;
		$profile->phone = $this->phone;
		$profile->user_id = $user->id;
		if ($profile->save()) {
			return true;
		}
	}
}
