<?php

class UserProfiles extends CActiveRecord
{

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'user_profiles';
	}

	public function rules()
	{
		return array(
			array('surname, name, patronymic, phone, user_id', 'required'),
			array('news_subscription, certificates_subscription, user_id', 'numerical', 'integerOnly' => true),
			array('surname, name, patronymic', 'length', 'max' => 50),
			array('phone', 'length', 'max' => 11),
			array('id, surname, name, patronymic, phone, user_id', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array('user' => array(self::BELONGS_TO, 'Users', 'user_id'));
	}

	public function attributeLabels()
	{
		return array();
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));
	}
}