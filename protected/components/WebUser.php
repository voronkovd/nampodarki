<?php

class WebUser extends CWebUser
{

	private $_model = null;

	public function getRole()
	{
		if ($user = $this->getModel()) {
			return $user->role;
		} else {
			return null;
		}
	}

	public function getPartner_id()
	{
		if ($user = $this->getModel()) {
			return $user->partner_id;
		} else {
			return null;
		}
	}

	function getLogin()
	{
		if (!$this->isGuest) {
			$user = Users::model()->findByPk($this->id, array('select' => 'email'));

			return $user->login;
		} else {
			return null;
		}
	}

	private function getModel()
	{
		if (!$this->isGuest && $this->_model === null) {
			$this->_model = Users::model()->findByPk($this->id, array('select' => 'role'));
		}

		return $this->_model;
	}
}