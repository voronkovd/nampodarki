<?php

class UserIdentity extends CUserIdentity
{

	const ERROR_DELETE_USER = 200;

	protected $_id;

	public function authenticate()
	{
		$user = Users::model()->find('LOWER(email)=?', array(strtolower($this->username)));
		if (($user === null) or (md5(Yii::app()->params['salt'] . $this->password) !== $user->password)) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		} else {
			if (0 == $user->status) {
				$this->errorCode = self::ERROR_DELETE_USER;
			} else {
				$this->_id = $user->id;
				$this->username = $user->email;
				$this->errorCode = self::ERROR_NONE;
			}
		}

		return !$this->errorCode;
	}

	public function getId()
	{
		return $this->_id;
	}

}