<?php

Class UrlHelper
{

	public static $rustable = array(
		'а' => 'a',
		'б' => 'b',
		'в' => 'v',
		'г' => 'g',
		'д' => 'd',
		'е' => 'e',
		'ё' => 'yo',
		'ж' => 'zh',
		'з' => 'z',
		'и' => 'i',
		'й' => 'y',
		'к' => 'k',
		'л' => 'l',
		'м' => 'm',
		'н' => 'n',
		'о' => 'o',
		'п' => 'p',
		'р' => 'r',
		'с' => 's',
		'т' => 't',
		'у' => 'u',
		'ф' => 'f',
		'х' => 'h',
		'ц' => 'c',
		'ч' => 'ch',
		'ш' => 'sh',
		'щ' => 'sch',
		'ь' => '\'',
		'ы' => 'y',
		'ъ' => '#',
		'э' => 'je',
		'ю' => 'yu',
		'я' => 'ya',
		'А' => 'A',
		'Б' => 'B',
		'В' => 'V',
		'Г' => 'G',
		'Д' => 'D',
		'Е' => 'E',
		'Ё' => 'Yo',
		'Ж' => 'Zh',
		'З' => 'Z',
		'И' => 'I',
		'Й' => 'Y',
		'К' => 'K',
		'Л' => 'L',
		'М' => 'M',
		'Н' => 'N',
		'О' => 'O',
		'П' => 'P',
		'Р' => 'R',
		'С' => 'S',
		'Т' => 'T',
		'У' => 'U',
		'Ф' => 'F',
		'Х' => 'H',
		'Ц' => 'C',
		'Ч' => 'Ch',
		'Ш' => 'Sh',
		'Щ' => 'Sch',
		'Ь' => '-',
		'Ы' => 'Y',
		'Ъ' => '#',
		'Э' => 'Je',
		'Ю' => 'Yu',
		'Я' => 'Ya',
	);

	public static function rusEncode($str = null, $spacechar = '-')
	{
		if ($str) {
			$str = strtolower(strtr($str, self::$rustable));
			$str = preg_replace('~[^-a-z0-9_]+~u', $spacechar, $str);
			$str = trim($str, $spacechar);

			return $str;
		} else {
			return;
		}
	}

	public static function cutString($string, $length = 65)
	{
		if ($length && strlen($string) > $length) {
			$str = strip_tags($string);
			$str = substr($str, 0, $length);
			$pos = strrpos($str, ' ');

			return substr($str, 0, $pos) . '';
		}

		return $string;
	}

}