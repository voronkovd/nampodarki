<?php

class ImageHelper
{

	const THUMB_DIR = '.tmb';


	public static function thumb($width, $height, $img, $options = null)
	{
		if (!file_exists($img)) {
			$img = str_replace('\\', '/', YiiBase::getPathOfAlias('webroot') . $img);
			if (!file_exists($img)) {
				return null;
			}
		}

		$quality = 80;
		$method = 'adaptiveResize';

		if ($options) {
			extract($options, EXTR_IF_EXISTS);
		}

		$pathinfo = pathinfo($img);
		$thumb_name =
			"thumb_" . $pathinfo['filename'] . '_' . $method . '_' . $width . '_' . $height . '.' .
			$pathinfo['extension'];
		$thumb_path = $pathinfo['dirname'] . '/' . self::THUMB_DIR . '/';
		if (!file_exists($thumb_path)) {
			mkdir($thumb_path);
		}

		if (!file_exists($thumb_path . $thumb_name) || filemtime($thumb_path . $thumb_name) < filemtime($img)) {

			Yii::import('ext.phpThumb.PhpThumbFactory');
			$options = array('jpegQuality' => $quality);
			$thumb = PhpThumbFactory::create($img, $options);
			$thumb->{$method}($width, $height);
			$thumb->save($thumb_path . $thumb_name);
		}

		$relative_path = str_replace(YiiBase::getPathOfAlias('webroot'), '', $thumb_path . $thumb_name);

		return $relative_path;
	}

}
