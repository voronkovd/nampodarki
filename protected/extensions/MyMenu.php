<?php

class MyMenu extends CMenu
{

	protected function isItemActive($item, $route)
	{
		$http = new CHttpRequest();
		if ($http->getRequestUri() == $item['url'][0]) {
			return true;
		} else {
			return false;
		}
	}
}