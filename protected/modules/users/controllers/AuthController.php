<?php

class AuthController extends Controller
{


	public function beforeAction($action)
	{
		if (!empty(Yii::app()->user->id)) {
			$this->redirect('/');
		}

		return parent::beforeAction($action);
	}

	public function actionLogin()
	{
		$model = new LoginForm();
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			if ($model->validate() && $model->login()) {
				$this->redirect(Yii::app()->baseUrl . '/');
			}
		}
		$this->render('login', array('model' => $model));
	}

	public function actionRegistration()
	{
		$model = new RegistrationForm();
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'register-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		if (isset($_POST['RegistrationForm'])) {
			$model->attributes = $_POST['RegistrationForm'];
			if ($model->validate() && $model->RegisterUser()) {
				$this->redirect(Yii::app()->baseUrl . '/');
			}
		}
		$this->render('register', array('model' => $model));
	}


	public function actionForgotPassword()
	{
		$model = new ForgotForm();
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'forgot-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		if (isset($_POST['ForgotForm'])) {
			$model->attributes = $_POST['ForgotForm'];
			if ($model->validate() && $model->sendNewPassword()) {
				$this->redirect(Yii::app()->baseUrl . '/');
			}
		}
		$this->render('forgot', array('model' => $model));
	}
}
