<?php

class UserController extends Controller
{

	public $layout = '//layouts/main';

	public function filters()
	{
		return array('accessControl', 'postOnly + delete');
	}

	public function accessRules()
	{
		return array(
			array('allow', 'actions' => array('self', 'update', 'ChangePassword', 'logout'), 'roles' => array(Users::AUTH_USER)),
			array('deny', 'users' => array('*')),
		);
	}

	public function actionSelf()
	{
		$this->render('self', array('model' => Users::model()->findByPk(Yii::app()->user->id)));
	}

	public function actionUpdate()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('user_id', Yii::app()->user->id);
		$model = UserProfiles::model()->find($criteria);
		if (empty($model)) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		$this->performAjaxValidation($model);
		if (isset($_POST['UserProfiles'])) {
			$model->attributes = $_POST['UserProfiles'];
			if ($model->save()) {
				$this->redirect(Yii::app()->createUrl('self'));
			}
		}
		$this->render('profile', array('model' => $model));

	}

	public function actionChangePassword()
	{
		$model = Users::model()->findByPk(Yii::app()->user->id);
		$this->performAjaxValidation($model);
		if (isset($_POST['Users'])) {
			$model->password = $_POST['Users']['password'];
			if ($model->save()) {
				$this->redirect(Yii::app()->createUrl('self'));
			}
		}
		$this->render('update', array('model' => $model));
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}
