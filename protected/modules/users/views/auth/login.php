<?php $form = $this->beginWidget(
    'CActiveForm', array(
        'id' => 'login-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    )
); ?>

<?php echo $form->labelEx($model, 'email'); ?>
<br/>
<br/>
<?php echo $form->textField($model, 'email'); ?>
<?php echo $form->error($model, 'email'); ?>
<br/>
<br/>
<?php echo $form->labelEx($model, 'password'); ?>
<br/>
<br/>
<?php echo $form->passwordField($model, 'password'); ?>
<?php if (!empty($model->errors)): ?>
    <br/>
    <br/>
    <?php echo $form->error($model, 'password'); ?>
<?php endif; ?>
<br/>
<br/>
<?php echo CHtml::submitButton('Войти'); ?>
<br />
<?php echo  CHtml::link('Регистрация', Yii::app()->createUrl('registration')); ?>
<br />
<?php echo  CHtml::link('Забыли пароль?', Yii::app()->createUrl('forgotpassword')); ?>
<?php $this->endWidget(); ?>
