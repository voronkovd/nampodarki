<?php $form = $this->beginWidget(
	'CActiveForm',
	array(
		'id' => 'register-form',
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
		),
	)
); ?>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->labelEx($model, 'email'); ?>
<?php echo $form->textField($model, 'email', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'email'); ?>
<br />
<?php echo $form->labelEx($model, 'password'); ?>
<?php echo $form->passwordField($model, 'password', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'password'); ?>
<br/>
<?php echo $form->labelEx($model, 'surname'); ?>
<?php echo $form->textField($model, 'surname', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'surname'); ?>
<br/>
<?php echo $form->labelEx($model, 'name'); ?>
<?php echo $form->textField($model, 'name', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'name'); ?>
<br/>
<?php echo $form->labelEx($model, 'patronymic'); ?>
<?php echo $form->textField($model, 'patronymic', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'patronymic'); ?>
<br/>
<?php echo $form->labelEx($model, 'phone'); ?>
<?php echo $form->textField($model, 'phone', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'phone'); ?>
<br/>
<br/>
<?php echo CHtml::submitButton('Зарегестрироваться'); ?>
<?php $this->endWidget(); ?>
