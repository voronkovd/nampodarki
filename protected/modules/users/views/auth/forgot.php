<?php $form = $this->beginWidget(
	'CActiveForm',
	array(
		'id' => 'login-form',
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
		),
	)
); ?>
<?php echo $form->labelEx($model, 'email'); ?>
<br/>
<br/>
<?php echo $form->textField($model, 'email'); ?>
<?php echo $form->error($model, 'email'); ?>
<br/>
<br/>
<?php if (!empty($model->errors)): ?>
	<br/>
	<br/>
<?php endif; ?>
<br/>
<br/>
<?php echo CHtml::submitButton('Отправить'); ?>
<br/>
<?php echo CHtml::link('Регистрация', Yii::app()->createUrl('registration')); ?>
<?php $this->endWidget(); ?>
