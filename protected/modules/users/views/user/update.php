<?php $form = $this->beginWidget('CActiveForm', array('id' => 'users-form', 'enableAjaxValidation' => true)); ?>
<?php echo $form->labelEx($model, 'password'); ?>
<?php echo $form->passwordField($model, 'password', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'password'); ?>
	<br/>
<?php echo CHtml::submitButton('Изменить', array('class' => 'btn')); ?>
<?php $this->endWidget(); ?>