<?php $form = $this->beginWidget('CActiveForm', array('id' => 'users-form', 'enableAjaxValidation' => true)); ?>
<?php echo $form->errorSummary($model); ?>
<?php echo $form->labelEx($model, 'surname'); ?>
<?php echo $form->textField($model, 'surname', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'surname'); ?>
<?php echo $form->errorSummary($model); ?>
<?php echo $form->labelEx($model, 'name'); ?>
<?php echo $form->textField($model, 'name', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'name'); ?>
<?php echo $form->errorSummary($model); ?>
<?php echo $form->labelEx($model, 'patronymic'); ?>
<?php echo $form->textField($model, 'patronymic', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'patronymic'); ?>
<?php echo $form->errorSummary($model); ?>
<?php echo $form->labelEx($model, 'phone'); ?>
<?php echo $form->textField($model, 'phone', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'phone'); ?>
	<br/>
<?php echo CHtml::submitButton('Изменить', array('class' => 'btn')); ?>
<?php $this->endWidget(); ?>