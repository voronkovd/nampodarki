<?php

class ForgotForm extends CFormModel
{
	public $email;

	protected $_user = null;

	public function rules()
	{
		return array(
			array('email', 'required'),
			array('email', 'issetEmail'),
		);
	}

	public function attributeLabels()
	{
		return array('email' => 'Email');
	}


	public function issetEmail($attribute, $params)
	{
		if (!$this->hasErrors()) {
			$criteria = new CDbCriteria();
			$criteria->compare('email', $this->email);
			$this->_user = Users::model()->find($criteria);
			if (empty($this->_user)) {
				$this->addError('email', 'Email не найден в базе');
			}
		}
	}


	public function sendNewPassword()
	{
		$new_password = md5(microtime());
		$this->_user->password = $new_password;
		$this->_user->save();
		$name = '=?UTF-8?B?"Нам подарки"?=';
		$subject = '=?UTF-8?B?Восстановление пароля на сайт "Нам подарки"?=';
		$headers = "From: $name <robot@nampodarki.ru>\r\n" .
			"Reply-To: robot@nampodarki.ru\r\n" .
			"MIME-Version: 1.0\r\n" .
			"Content-Type: text/html; charset=UTF-8";
		$body = "Здравствуйте, {$this->_user->profile->surname} {$this->_user->profile->name} {$this->_user->profile->patronymic}!<br />";
		$body = $body . " Ваш новый пароль на сайт: " . $new_password;
		mail($this->_user->email, $subject, $body, $headers);

		return true;
	}
}
