<?php

class AdminModule extends CWebModule
{
	public function init()
	{
		$this->setImport(array('admin.models.*', 'admin.components.*'));
		Yii::app()->theme = 'admin';
		Yii::app()->clientscript
			->registerCssFile(Yii::app()->theme->baseUrl . '/css/bootstrap.css')
			->registerCssFile(Yii::app()->theme->baseUrl . '/css/theme.css')
			->registerCoreScript('jquery')
			->registerCoreScript('jquery.ui');
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {

			return true;
		} else {
			return false;
		}
	}
}