<?php
$form = $this->beginWidget('CActiveForm', array('id' => 'who-is-certificates-form', 'enableAjaxValidation' => true)); ?>
<?php echo $form->errorSummary($model); ?>
<?php echo $form->labelEx($model, 'name'); ?>
<?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 100)); ?>
<?php echo $form->error($model, 'name'); ?>
<br/>
<?php echo CHtml::link(
	'Отмена',
	Yii::app()->createUrl('admin/whoIsCertificates'),
	array('class' => 'btn btn-danger')
); ?>
<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', array('class' => 'btn')); ?>
<?php $this->endWidget(); ?>
