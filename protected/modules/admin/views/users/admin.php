<?php
$this->breadcrumbs = array('Пользователи');
$this->widget(
	'zii.widgets.grid.CGridView',
	array('id' => 'users-grid', 'dataProvider' => $model->search(), 'filter' => $model, 'columns' => $model->columns)
);