<?php $this->breadcrumbs = array('Пользователи' => array('index'), 'Редактирование'); ?>
	<h3>Редактировать пользователя <?php echo $model->email; ?></h3>
<?php $form = $this->beginWidget('CActiveForm', array('id' => 'users-form', 'enableAjaxValidation' => true)); ?>
<?php echo $form->errorSummary($model); ?>
<?php echo $form->labelEx($model, 'email'); ?>
<?php echo $form->textField($model, 'email', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'email'); ?>
<?php if (Yii::app()->user->role == Users::ADMIN) {
	$roles = Yii::app()->params['user_roles'];
} else {
	$roles = Yii::app()->params['partner_roles'];
} ?>
<?php echo $form->labelEx($model, 'role'); ?>
<?php echo $form->dropDownList($model, 'role', $roles); ?>
<?php echo $form->error($model, 'role'); ?>
<?php echo $form->labelEx($model, 'password'); ?>
<?php echo $form->passwordField($model, 'password', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'password'); ?>
<?php echo $form->labelEx($model, 'status'); ?>
<?php echo $form->dropDownList($model, 'status', Yii::app()->params['visible_statuses']); ?>
<?php echo $form->error($model, 'status'); ?>
	<br/>
<?php echo CHtml::link('Отмена', Yii::app()->createUrl('admin/users'), array('class' => 'btn btn-danger')); ?>
<?php echo CHtml::submitButton('Изменить', array('class' => 'btn')); ?>
<?php $this->endWidget(); ?>