<?php $this->breadcrumbs = array('Пользователи' => array('index'), 'Добавить'); ?>
	<h3>Добавить пользователя</h3>
<?php $form = $this->beginWidget(
	'CActiveForm',
	array(
		'id' => 'users-form',
		'enableAjaxValidation' => true,
		'htmlOptions' => array('autocomplete' => 'off')
	)
); ?>
<?php echo $form->errorSummary($model); ?>
<?php echo $form->labelEx($model, 'email'); ?>
<?php echo $form->textField($model, 'email', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'email'); ?>
<?php if (Yii::app()->user->role == Users::ADMIN) {
	$roles = Yii::app()->params['user_roles'];
} else {
	$roles = Yii::app()->params['partner_roles'];
} ?>
	<br/>
<?php echo $form->labelEx($model, 'role'); ?>
<?php echo $form->dropDownList($model, 'role', $roles); ?>
<?php echo $form->error($model, 'role'); ?>
	<br/>

<?php echo $form->labelEx($model, 'password'); ?>
<?php echo $form->passwordField($model, 'password', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'password'); ?>
	<br/>
<?php echo $form->labelEx($model, 'surname'); ?>
<?php echo $form->textField($model, 'surname', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'surname'); ?>
	<br/>
<?php echo $form->labelEx($model, 'name'); ?>
<?php echo $form->textField($model, 'name', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'name'); ?>
	<br/>
<?php echo $form->labelEx($model, 'patronymic'); ?>
<?php echo $form->textField($model, 'patronymic', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'patronymic'); ?>
	<br/>
<?php echo $form->labelEx($model, 'phone'); ?>
<?php echo $form->textField($model, 'phone', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'phone'); ?>
	<br/>
<?php echo CHtml::link('Отмена', Yii::app()->createUrl('admin/users'), array('class' => 'btn btn-danger')); ?>
<?php echo CHtml::submitButton('Добавить', array('class' => 'btn')); ?>
<?php $this->endWidget(); ?>