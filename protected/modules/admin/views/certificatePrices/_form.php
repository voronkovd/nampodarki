<?php $form = $this->beginWidget(
	'CActiveForm',
	array('id' => 'certificate-prices-form', 'enableAjaxValidation' => true)
); ?>
<?php echo $form->errorSummary($model); ?>
<?php echo $form->labelEx($model, 'price'); ?>
<?php echo $form->textField($model, 'price'); ?>
<?php echo $form->error($model, 'price'); ?>
	<br/>
	<br/>
<?php echo CHtml::link(
	'Отмена',
	Yii::app()->createUrl('admin/certificatePrices'),
	array('class' => 'btn btn-danger')
); ?>
<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', array('class' => 'btn')); ?>
<?php $this->endWidget(); ?>