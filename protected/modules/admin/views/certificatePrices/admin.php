<?php
$this->breadcrumbs = array('Цены на сертификаты');
$this->widget(
	'zii.widgets.grid.CGridView',
	array('id' => 'certificate-prices-grid', 'dataProvider' => $model->search(), 'columns' => $model->columns)
);