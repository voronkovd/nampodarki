<?php
$this->breadcrumbs = array('Партнеры');
$this->widget(
	'zii.widgets.grid.CGridView',
	array('id' => 'partners-grid', 'dataProvider' => $model->search(), 'columns' => $model->columns)
);