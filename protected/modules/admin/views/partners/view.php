<?php $this->breadcrumbs = array('Партнер' => array('index'), $model->name); ?>
<h3>Партнер #<?php echo $model->name; ?></h3>
<b>Статистика Партнера:</b>
<br/>
<br/>
<?php
$this->widget(
	'zii.widgets.CDetailView',
	array(
		'data' => $model,
		'attributes' => array(
			array(
				'label' => '<b>Колличество сотрудников:</b>',
				'type' => 'raw',
				'value' => $model->countAll($model->users)
			),
			array(
				'label' => 'Колличество изображений в галерее:',
				'type' => 'raw',
				'value' => $model->countAll($model->galleries)
			),
			array(
				'label' => 'Колличество новостей:',
				'type' => 'raw',
				'value' => $model->countAll($model->news)
			),
			array(
				'label' => 'Колличество сертификатов:',
				'type' => 'raw',
				'value' => $model->countAll($model->certificates)
			),
			array(
				'label' => 'Колличество акций:',
				'type' => 'raw',
				'value' => $model->countAll($model->sellouts)
			),
		),
	)
); ?>
<br/>
<br/>
<?php if (!empty($model->users)): ?>
	<b>Сотрудники:</b>
	<br/>
	<br/>
	<table class="table table-bordered">
		<thead>
		<tr>
			<th>ФИО:</th>
			<th>Телефон:</th>
			<th>Email:</th>
			<th>Роль:</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<?php foreach ($model->users as $user): ?>
				<td><?php echo $user->profile->surname . " " . $user->profile->name . " " . $user->profile->patronymic; ?></td>
				<td><?php echo $user->profile->phone; ?></td>
				<td><?php echo $user->email; ?></td>
				<td><?php echo Yii::app()->params["user_roles"][$user->role]; ?></td>
			<?php endforeach; ?>
		</tr>
		</tbody>
	</table>
	<br/>
	<br/>
<?php endif; ?>

