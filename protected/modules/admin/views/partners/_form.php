<?php
$form = $this->beginWidget(
	'CActiveForm',
	array(
		'id' => 'partners-form',
		'enableAjaxValidation' => true,
		'htmlOptions' => array('enctype' => 'multipart/form-data')
	)
);
?>
<?php echo $form->errorSummary($model); ?>
<?php echo $form->labelEx($model, 'name'); ?>
<?php echo $form->textField($model, 'name', array('size' => 60, 'maxLength' => 100)); ?>
<?php echo $form->error($model, 'name'); ?>
	<br/>

<?php echo $form->labelEx($model, 'image_name'); ?>
<?php echo $form->fileField($model, 'image_name'); ?>
<?php echo $form->error($model, 'image_name'); ?>
<?php if (!$model->isNewRecord || !empty($model->image_name)): ?>
	<?php echo CHtml::image(
		Yii::app()->baseUrl . '/images/partners/' . $model->image_name,
		$model->name,
		array('width' => '200')
	); ?>
<?php endif; ?>
	<br/>
	<br/>
<?php echo $form->labelEx($model, 'status'); ?>
<?php echo $form->dropDownList($model, 'status', Yii::app()->params['visible_statuses']); ?>
<?php echo $form->error($model, 'status'); ?>
	<br/>
<?php echo CHtml::link('Отмена', Yii::app()->createUrl('admin/partners'), array('class' => 'btn btn-danger')); ?>
<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', array('class' => 'btn')); ?>
<?php $this->endWidget(); ?>