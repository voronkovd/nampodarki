<?php
$this->breadcrumbs = array('Акции');
$this->widget(
	'zii.widgets.grid.CGridView',
	array('id' => 'sellouts-grid', 'dataProvider' => $model->search(), 'columns' => $model->columns)
);