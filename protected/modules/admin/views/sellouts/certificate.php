<?php
$this->breadcrumbs = array('Акции' => array('index'), 'Сертификаты');
$form = $this->beginWidget(
	'CActiveForm',
	array('id' => 'certificates-sellout')
); ?>
	<br/>
<?php echo $form->errorSummary($model); ?>
<?php echo $form->labelEx($model, 'certificates'); ?>
<?php
$this->widget(
	'ext.dynatree.DynaTree',
	array(
		'attribute' => 'SelloutCertificates[certificates]',
		'data' => $data,
		'selection' => $active,
	)
); ?>
	<br/>
	<br/>
<?php echo CHtml::link(
	'Отмена',
	Yii::app()->createUrl('admin/certificates'),
	array('class' => 'btn btn-danger')
); ?>
<?php echo CHtml::submitButton('Добавить / Изменить', array('class' => 'btn')); ?>
<?php $this->endWidget(); ?>