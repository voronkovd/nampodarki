<?php
$this->breadcrumbs = array('Заказы');
$this->widget(
	'zii.widgets.grid.CGridView',
	array('id' => 'orders-grid', 'dataProvider' => $model->search(), 'columns' => $model->columns)
);