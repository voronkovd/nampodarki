<?php $this->breadcrumbs = array('Заказы' => array('index'), 'Подробнее...'); ?>
	<h3>Заказ #<?php echo $model->id; ?></h3>
	<b>Информация о покупателе:</b>
<?php
$this->widget(
	'zii.widgets.CDetailView',
	array(
		'data' => $model,
		'attributes' => array(
			array(
				'label' => 'ФИО:',
				'type' => 'raw',
				'value' => $model->user->profile->surname . " " . $model->user->profile->name . " " . $model->user->profile->patronymic
			),
			array(
				'label' => 'Email:',
				'type' => 'raw',
				'value' => $model->user->email
			),
			array(
				'label' => 'Контактный Телефон:',
				'type' => 'raw',
				'value' => $model->user->profile->phone
			),
		),
	)
); ?>
	<br/>
	<b>Информация о заказе:</b>
<?php
$this->widget(
	'zii.widgets.CDetailView',
	array(
		'data' => $model,
		'attributes' => array(
			array(
				'label' => 'Менеджер:',
				'type' => 'raw',
				'value' => !empty($model->manager_id) ? $model->user->profile->surname . " " . $model->user->profile->name . " " . $model->user->profile->patronymic : ""
			),
			array(
				'label' => 'Текущий статус:',
				'type' => 'raw',
				'value' => Yii::app()->params["orders_roles"][$model->current_status]
			),
		),
	)
); ?>
	<br/>
	<b>Товары в заказе:</b>
	<table class="table table-bordered">
		<thead>
		<tr>
			<th>Наименование сертификата:</th>
			<th>Цена (руб.):</th>
			<th>Цена на момент заказа (руб.):</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<?php $summ = 0.00;
			foreach ($model->certificates as $certificate): ?>
				<td><?php echo $certificate->certificate->name; ?></td>
				<td><?php echo $certificate->price->price; ?></td>
				<td><?php $summ = $summ + $certificate->price_value;
					echo $certificate->price_value; ?></td>
			<?php endforeach; ?>
		</tr>
		</tbody>
		<tfoot>
		<tr>
			<td colspan="2"><b>Итого:</b></td>
			<td><?php echo floatval($summ); ?></td>
		</tr>
		</tfoot>
	</table>
	<br/>
	<b>История статусов:</b>
	<table class="table table-bordered">
		<thead>
		<tr>
			<th>Статус:</th>
			<th>Дата добавления:</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<?php foreach ($model->statuses as $status): ?>
				<td><?php echo Yii::app()->params['orders_roles'][$status->status]; ?></td>
				<td><?php echo $status->date_add; ?></td>
			<?php endforeach; ?>
		</tr>
		</tbody>
	</table>
	<br/>
<?php if ($model->current_status == Yii::app()->params['order_new']): ?>
	<a class="btn btn-danger" onclick="if(!confirm('Вы уверены, что хотите отклонить?')) return false;"
	   href="<?php echo Yii::app()->createUrl(
		   'admin/orders/update',
		   array('id' => $model->id, 'order' => Yii::app()->params['order_cancel'])
	   ); ?>">Отклонить
		заказ</a>
	<a class="btn"
	   href="<?php echo Yii::app()->createUrl(
		   'admin/orders/update',
		   array('id' => $model->id, 'order' => Yii::app()->params['order_purchased'])
	   ); ?>"
	   onclick="if(!confirm('Заказ оплачен?')) return false;">Оплачен</a>
<?php endif; ?>