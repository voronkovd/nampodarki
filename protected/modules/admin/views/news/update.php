<?php $this->breadcrumbs = array('Новости' => array('index'), 'Редактировать ' . $model->title); ?>
	<h3>Редактировать новость <?php echo $model->title; ?></h3>
<?php $this->renderPartial('_form', array('model' => $model)); ?>