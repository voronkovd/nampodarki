<?php $form = $this->beginWidget(
	'CActiveForm',
	array(
		'id' => 'news-form',
		'enableAjaxValidation' => true,
		'htmlOptions' => array('enctype' => 'multipart/form-data')
	)
); ?>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->labelEx($model, 'title'); ?>
<?php echo $form->textField($model, 'title', array('size' => 50, 'maxlength' => 50)); ?>
<?php echo $form->error($model, 'title'); ?>
	<br/>
<?php if (!$model->isNewRecord || !empty($model->image)){ ?>
	<?php echo CHtml::image(
		Yii::app()->baseUrl . '/images/news/' . $model->image,
		$model->title,
		array('width' => '200')
	); } ?>
	<br />
<?php echo $form->labelEx($model, 'image'); ?>
<?php echo $form->fileField($model, 'image'); ?>
<?php echo $form->error($model, 'image'); ?>
	<br/>
<?php echo $form->labelEx($model, 'anons'); ?>
<?php echo $form->textArea($model, 'anons', array('rows' => 6, 'cols' => 50)); ?>
<?php echo $form->error($model, 'anons'); ?>
	<br/>

<?php
if (!empty($model->galleries)) {
	$provider = $model->getImages();
	$this->widget(
		'zii.widgets.grid.CGridView',
		array(
			'id' => 'categories-grid',
			'dataProvider' => $provider,
			'summaryText' => '',
			'columns' => array(
				array('name' => 'image_name', 'type' => 'raw', 'value' => '$data->getImage()'),
				array
				(
					'class' => 'CButtonColumn',
					'template' => '{delete}',
					'htmlOptions' => array('width' => '50px;'),
					'header' => '',
					'deleteConfirmation' => 'Удалить?',
					'updateButtonImageUrl' => false,
					'deleteButtonImageUrl' => false,
					'deleteButtonOptions' => array('class' => 'icon-trash', 'title' => 'Удалить'),
					'buttons' => array(
						'delete' => array(
							'label' => ' ',
							'url' => 'Yii::app()->createUrl("/admin/news/deleteImage", array("id" => $data->id))',
						),
					),
				),
			),
		)
	);
}
?>
	<label for="gallery">Галлерея</label>
<?php $this->widget(
	'CMultiFileUpload',
	array(
		'name' => 'Images[gallery]',
		'accept' => 'jpg',
		'duplicate' => 'Дубликат!',
		'denied' => 'Неверный тип файла',
		'options' => array(),
	)
); ?>
	<br/>
<?php echo $form->labelEx($model, 'content'); ?>
<?php
$this->widget(
	'ext.redactor.ERedactorWidget',
	array(
		'model' => $model,
		'attribute' => 'content',
		'options' => array(
			'lang' => 'ru',
		),
	)
);
?>
<?php echo $form->error($model, 'content'); ?>
	<br/>
<?php echo $form->labelEx($model, 'date_add'); ?>
<?php $this->widget(
	'zii.widgets.jui.CJuiDatePicker',
	array(
		'attribute' => 'date_add',
		'model' => $model,
		'options' => array(
			'dateFormat' => 'yy-mm-dd',
			'showAnim' => 'fold',
		),
		'htmlOptions' => array(
			'style' => 'height:20px;'
		),
	)
); ?>
<?php echo $form->error($model, 'date_add'); ?>
	<br/>
<?php if (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER) { ?>
	<?php echo $form->labelEx($model, 'partner_id'); ?>
	<?php echo $form->dropDownList(
		$model,
		'partner_id',
		array_merge(array(0 => ''), CHtml::listData(Partners::model()->public()->findAll(), 'id', 'name'))
	); ?>
	<?php echo $form->error($model, 'partner_id'); ?>
<?php
} else {
	$user = Users::model()->findByPk(Yii::app()->user->id);
	echo CHtml::hiddenField('News[partner_id]', $user->partner_id);
} ?>
	<br/>
	<br/>
<?php echo CHtml::link(
	'Отмена',
	Yii::app()->createUrl('admin/news'),
	array('class' => 'btn btn-danger')
); ?>
<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', array('class' => 'btn')); ?>
<?php $this->endWidget(); ?>