<?php
$this->breadcrumbs = array('Новости');
$this->widget(
	'zii.widgets.grid.CGridView',
	array('id' => 'news-grid', 'dataProvider' => $model->search(), 'columns' => $model->columns)
);