<?php
$this->breadcrumbs = array('Галлерея');
$this->widget(
	'zii.widgets.grid.CGridView',
	array('id' => 'partner-galleries-grid', 'dataProvider' => $model->search(), 'columns' => $model->columns)
);