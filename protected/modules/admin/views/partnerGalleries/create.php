<?php $this->breadcrumbs = array('Галлерея' => array('index'), 'Добавить'); ?>
	<h3>Добавить изображение</h3>
<?php $form = $this->beginWidget(
	'CActiveForm',
	array(
		'id' => 'partner-galleries-form',
		'enableAjaxValidation' => true,
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
		),
	)
); ?>
<?php echo $form->errorSummary($model); ?>

<?php echo $form->labelEx($model, 'file_names'); ?>
<?php $this->widget(
	'CMultiFileUpload',
	array(
		'model' => $model,
		'attribute' => 'file_names',
		'accept' => 'jpg|gif|png',
		'duplicate' => 'Дубликат!',
		'denied' => 'Неверный тип файла',
		'options' => array(),
	)
); ?>
<?php echo $form->error($model, 'file_names'); ?>
	<br/>
<?php if (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER) { ?>
	<?php echo $form->labelEx($model, 'partner_id'); ?>
	<?php echo $form->dropDownList(
		$model,
		'partner_id',
		array_merge(array(0 => ''), CHtml::listData(Partners::model()->public()->findAll(), 'id', 'name'))
	); ?>
	<?php echo $form->error($model, 'partner_id'); ?>
<?php
} else {
	$user = Users::model()->findByPk(Yii::app()->user->id);
	echo CHtml::hiddenField('News[partner_id]', $user->partner_id);
} ?>
	<br/>
	<br/>
<?php echo CHtml::link(
	'Отмена',
	Yii::app()->createUrl('admin/partnerGalleries'),
	array('class' => 'btn btn-danger')
); ?>
<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', array('class' => 'btn')); ?>
<?php $this->endWidget(); ?>