<?php
$this->breadcrumbs = array('Категории сертификатов');
$this->widget(
	'zii.widgets.grid.CGridView',
	array('id' => 'certificate-categories-grid', 'dataProvider' => $model->search(), 'columns' => $model->columns)
);