<?php $form = $this->beginWidget(
	'CActiveForm',
	array('id' => 'when-is-certificates-form', 'enableAjaxValidation' => true)
); ?>
<?php echo $form->errorSummary($model); ?>
<?php echo $form->labelEx($model, 'name'); ?>
<?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 100)); ?>
<?php echo $form->error($model, 'name'); ?>
	<br/>
<?php echo $form->labelEx($model, 'date_when'); ?>
<?php $this->widget(
	'zii.widgets.jui.CJuiDatePicker',
	array(
		'attribute' => 'date_when',
		'model' => $model,
		'options' => array(
			'dateFormat' => 'mm-dd',
			'showAnim' => 'fold',
		),
		'htmlOptions' => array(
			'style' => 'height:20px;'
		),
	)
); ?>
<?php echo $form->error($model, 'date_when'); ?>
	<br/>
<?php echo CHtml::link(
	'Отмена',
	Yii::app()->createUrl('admin/whenIsCertificates'),
	array('class' => 'btn btn-danger')
); ?>
<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', array('class' => 'btn')); ?>
<?php $this->endWidget(); ?>