<?php $this->breadcrumbs = array('Posts' => array('index'), 'Редактировать ' . $model->title); ?>
	<h3>Редактировать статью <?php echo $model->title; ?></h3>
<?php $this->renderPartial('_form', array('model' => $model)); ?>