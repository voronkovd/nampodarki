<?php $this->breadcrumbs = array('Статьи');
$this->widget(
	'zii.widgets.grid.CGridView',
	array('id' => 'posts-grid', 'dataProvider' => $model->search(), 'columns' => $model->columns)
);