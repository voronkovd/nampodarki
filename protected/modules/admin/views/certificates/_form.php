<?php $form = $this->beginWidget(
	'CActiveForm',
	array(
		'id' => 'certificates-form',
		'enableAjaxValidation' => true,
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
		),
	)
); ?>
<?php echo $form->errorSummary($model); ?>
	<br/>
<?php echo $form->labelEx($model, 'who'); ?>
<?php
$this->widget(
	'ext.EchMultiSelect.EchMultiSelect',
	array(
		'model' => $model,
		'dropDownAttribute' => 'who',
		'value' => $model->who,
		'data' => CHtml::listData(WhoIsCertificates::model()->findAll(), 'id', 'name'),
	)
);
?>
	<br/>
<?php echo $form->labelEx($model, 'when'); ?>
<?php
$this->widget(
	'ext.EchMultiSelect.EchMultiSelect',
	array(
		'model' => $model,
		'dropDownAttribute' => 'when',
		'value' => $model->when,
		'data' => WhenIsCertificates::getAll()
	)
);
?>
	<br/>
<?php echo $form->labelEx($model, 'category'); ?>
<?php
$this->widget(
	'ext.EchMultiSelect.EchMultiSelect',
	array(
		'model' => $model,
		'dropDownAttribute' => 'category',
		'value' => $model->when,
		'data' => Categories::getAll()
	)
);
?>
	<br/>
<?php if (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER) { ?>
	<?php echo $form->labelEx($model, 'partner_id'); ?>
	<?php echo $form->dropDownList(
		$model,
		'partner_id',
		array_merge(array(0 => ''), CHtml::listData(Partners::model()->public()->findAll(), 'id', 'name'))
	); ?>
	<?php echo $form->error($model, 'partner_id'); ?>
<?php
} else {
	$user = Users::model()->findByPk(Yii::app()->user->id);
	echo CHtml::hiddenField('News[partner_id]', $user->partner_id);
} ?>
	<br/>
<?php echo $form->labelEx($model, 'name'); ?>
<?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 100)); ?>
<?php echo $form->error($model, 'name'); ?>
	<br/>
<?php if (!$model->isNewRecord || !empty($model->image)): ?>
	<?php echo CHtml::image(
		Yii::app()->baseUrl . '/images/certificates/' . $model->image,
		$model->name,
		array('width' => '200')
	); ?>
<?php endif; ?>
<?php echo $form->labelEx($model, 'image'); ?>
<?php echo $form->fileField($model, 'image'); ?>
<?php echo $form->error($model, 'image'); ?>
	<br/>
<?php echo $form->labelEx($model, 'desc'); ?>
<?php echo $form->textArea($model, 'desc', array('size' => 60, 'maxlength' => 255)); ?>
<?php echo $form->error($model, 'desc'); ?>
	<br/>
<?php echo $form->labelEx($model, 'lifetime'); ?>
<?php $this->widget(
	'zii.widgets.jui.CJuiDatePicker',
	array(
		'attribute' => 'lifetime',
		'model' => $model,
		'options' => array(
			'dateFormat' => 'yy-mm-dd',
			'showAnim' => 'fold',
		),
		'htmlOptions' => array(
			'style' => 'height:20px;'
		),
	)
); ?>
<?php echo $form->error($model, 'lifetime'); ?>
	<br/>
<?php echo $form->labelEx($model, 'status'); ?>
<?php echo $form->dropDownList($model, 'status', Yii::app()->params['visible_statuses']); ?>
<?php echo $form->error($model, 'status'); ?>
	<br/>
<?php echo $form->labelEx($model, 'delivery'); ?>
<?php echo $form->checkBox($model, 'delivery'); ?>
<?php echo $form->error($model, 'delivery'); ?>
	<br/>
<?php echo $form->labelEx($model, 'prices'); ?>
<?php $this->widget(
	'CAutoComplete',
	array(
		'model' => $model,
		'attribute' => 'prices',
		'value' => $model->getPrices(),
		'url' => Yii::app()->createUrl('admin/certificates/prices'),
		'multiple' => true,
		'htmlOptions' => array('rows' => 6, 'cols' => 50),
	)
);
?>

<?php echo $form->error($model, 'prices'); ?>
	<br/>
	<br/>
<?php echo CHtml::link(
	'Отмена',
	Yii::app()->createUrl('admin/certificates'),
	array('class' => 'btn btn-danger')
); ?>
<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', array('class' => 'btn')); ?>
<?php $this->endWidget(); ?>