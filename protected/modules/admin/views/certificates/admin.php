<?php
$this->breadcrumbs = array('Сертификаты');
$this->widget(
	'zii.widgets.grid.CGridView',
	array('id' => 'certificates-grid', 'dataProvider' => $model->search(), 'columns' => $model->columns)
);