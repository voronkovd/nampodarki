<?php

class PostsController extends Controller
{
	public $layout = '//layouts/column2';

	public function filters()
	{
		return array('accessControl', 'postOnly + delete');
	}

	public function beforeAction()
	{
		$this->menu = array(
			array(
				'label' => 'Список новостей',
				'url' => array('/admin/news/'),

			),
			array(
				'label' => 'Добавить новость',
				'url' => array('/admin/news/create')
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::MANAGER || Yii::app()->user->role == Users::ADMIN)
			),
			array(
				'label' => 'Статьи',
				'url' => array('/admin/posts/'),
				'visible' => (Yii::app()->user->role == Users::MANAGER || Yii::app()->user->role == Users::ADMIN)
			),
			array(
				'label' => 'Добавить статью',
				'url' => array('/admin/posts/create'),
				'visible' => (Yii::app()->user->role == Users::MANAGER || Yii::app()->user->role == Users::ADMIN)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::MANAGER || Yii::app()->user->role == Users::ADMIN)
			),
			array(
				'label' => 'Комментарии',
				'url' => array('/admin/commentaries/'),
				'visible' => (Yii::app()->user->role == Users::MANAGER || Yii::app()->user->role == Users::ADMIN)
			),
		);

		return true;

	}

	public function accessRules()
	{
		return array(
			array('allow', 'actions' => array('index', 'create', 'update', 'delete'), 'roles' => array(Users::MANAGER)),
			array('deny', 'users' => array('*'))
		);
	}

	public function actionCreate()
	{
		$model = new Posts;
		$this->performAjaxValidation($model);
		if (isset($_POST['Posts'])) {
			$model->attributes = $_POST['Posts'];
			$rnd = rand(0, 9999);
			$uploadedFile = CUploadedFile::getInstance($model, 'image');
			$fileName = "{$rnd}-{$uploadedFile}";
			$model->image = $fileName;
			if ($model->save()) {
				$path = Yii::app()->getBasePath() . '/../images/posts/';
				$uploadedFile->saveAs($path . $fileName);
				$id = $model->getPrimaryKey();
				if (!empty($_FILES['Images']['tmp_name']['gallery'])) {
					foreach ($_FILES['Images']['tmp_name']['gallery'] as $key => $filename) {
						if (!empty($filename)) {
							$name = md5(microtime()) . '.jpg';
							$model = new GalleryPosts();
							$model->news_id = $id;
							$model->image_name = $name;
							if ($model->save()) {
								move_uploaded_file(
									$filename,
									Yii::app()->getBasePath() . '/../images/gallery/' . $name
								);
							}
						}
					}
				}
				$this->redirect(array('/admin/news'));
			}
		}
		$this->title = 'Добавить статью';
		$this->render('create', array('model' => $model));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$this->performAjaxValidation($model);
		if (isset($_POST['Posts'])) {
			$model->attributes = $_POST['Posts'];
			$uploadedFile = CUploadedFile::getInstance($model, 'image');
			if ($uploadedFile != null) {
				$rnd = rand(0, 9999);
				$fileName = "{$rnd}-{$uploadedFile}";
				$uploadedFile->saveAs(Yii::app()->basePath . '/../images/posts/' . $fileName, true);
				$model->image = $fileName;
			}
			if (!empty($_FILES['Images']['tmp_name']['gallery'])) {
				foreach ($_FILES['Images']['tmp_name']['gallery'] as $key => $filename) {
					if (!empty($filename)) {
						$name = md5(microtime()) . '.jpg';
						$model = new GalleryPosts();
						$model->news_id = $id;
						$model->image_name = $name;
						if ($model->save()) {
							move_uploaded_file(
								$filename,
								Yii::app()->getBasePath() . '/../images/gallery/' . $name
							);
						}
					}
				}
			}
			if ($model->save()) {
				$this->redirect(array('/admin/posts'));
			}
		}
		$this->title = 'Редактировать статью ' . $model->title;
		$this->render('update', array('model' => $model));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function actionIndex()
	{
		$model = new Posts('search');
		$model->unsetAttributes();
		$model->columns = array(
			array('name' => 'title'),
			array(
				'name' => 'anons',
				'value' => 'UrlHelper::cutString($data->anons, 50)',
			),
			array(
				'name' => 'content',
				'value' => 'UrlHelper::cutString($data->content)'
			),
			'date_add',
			array
			(
				'class' => 'CButtonColumn',
				'template' => '{update}<span style="margin-left: 10px;">{delete}</span>',
				'htmlOptions' => array(
					'style' => 'width:30px;',
					'class' => 'text_align_center'
				),
				'header' => '',
				'updateButtonImageUrl' => false,
				'deleteButtonImageUrl' => false,
				'updateButtonOptions' => array(
					'class' => 'icon-edit',
					'title' => 'Редактировать'
				),
				'deleteButtonOptions' => array(
					'class' => 'icon-remove',
					'title' => 'Удалить'
				),
				'buttons' => array(
					'update' => array(
						'label' => '',
						'url' => 'Yii::app()->createUrl("admin/posts/update", array("id"=>$data->id))',
					),
					'delete' => array(
						'label' => '',
					),
				),
			)
		);
		if (isset($_GET['Posts'])) {
			$model->attributes = $_GET['Posts'];
		}
		$this->title = 'Статьи';
		$this->render('admin', array('model' => $model));
	}

	public function loadModel($id)
	{
		$model = Posts::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}

		return $model;
	}


	public function actionDeleteImage($id)
	{
		GalleryPosts::model()->findByPk($id)->delete();
		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'posts-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
