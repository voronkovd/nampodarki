<?php

class CertificatesController extends Controller
{
	public $layout = '//layouts/column2';

	public function beforeAction()
	{
		$this->menu = array(
			array(
				'label' => 'Категории сертификатов',
				'url' => array('/admin/certificateCategories/'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Добавить категорию',
				'url' => array('/admin/certificateCategories/create'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array('label' => 'Сертификаты', 'url' => array('/admin/certificates/')),
			array('label' => 'Добавить сертификат', 'url' => array('/admin/certificates/create')),
			array('label' => '<hr>'),
			array(
				'label' => 'Список "кому"',
				'url' => array('/admin/whoIsCertificates/'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Добавить "кому"',
				'url' => array('/admin/whoIsCertificates/create'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Список "когда"',
				'url' => array('/admin/whenIsCertificates/'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Добавить "когда"',
				'url' => array('/admin/whenIsCertificates/create'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array('label' => 'Акции', 'url' => array('/admin/sellouts/')),
			array('label' => 'Добавить акцию', 'url' => array('/admin/sellouts/create')),
			array('label' => '<hr>'),
			array('label' => 'Цены', 'url' => array('/admin/certificatePrices/')),
			array('label' => 'Добавить цену', 'url' => array('/admin/certificatePrices/create'))
		);

		return true;

	}

	public function filters()
	{
		return array('accessControl', 'postOnly + delete');
	}


	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions' => array('index', 'create', 'update', 'delete', 'Prices'),
				'roles' => array(Users::MANAGER, Users::PARTNER_MANAGER)
			),
			array('deny', 'users' => array('*')),
		);
	}

	public function actionCreate()
	{
		$model = new Certificates;
		$this->performAjaxValidation($model);
		if (isset($_POST['Certificates'])) {
			$model->attributes = $_POST['Certificates'];
			if ($model->validate()) {
				if (isset($_POST['Certificates']['when'])) {
					$model->when = $_POST['Certificates']['when'];
				}
				if (isset($_POST['Certificates']['who'])) {
					$model->who = $_POST['Certificates']['who'];
				}
				if (isset($_POST['Certificates']['prices'])) {
					$model->prices = $_POST['Certificates']['prices'];
				}
				$rnd = rand(0, 9999);
				$uploadedFile = CUploadedFile::getInstance($model, 'image');
				$fileName = "{$rnd}-{$uploadedFile}";
				$model->image = $fileName;
				if ($model->save()) {
					$posts_keys = new CertificatePriceRelations();
					$posts_keys->add($model->prices, $model->getPrimaryKey());
					$path = Yii::app()->getBasePath() . '/../images/certificates/';
					$uploadedFile->saveAs($path . $fileName);
					$this->redirect(array('/admin/certificates'));
				}
			}
		}
		$this->title = 'Добавить сертификат';
		$this->render('create', array('model' => $model));
	}

	public function actionUpdate($id)
	{
		$model = Certificates::model()->findByPk($id);
		$criteria = new CDbCriteria();
		$criteria->compare('certificate_id', $id);
		$when = WhenIsCertificateRelations::model()->findAll($criteria);
		$who = WhoIsCertificateRelations::model()->findAll($criteria);
		$model->who = array_values(CHtml::listData($who, 'id', 'who_id'));
		$model->when = array_values(CHtml::listData($when, 'id', 'when_id'));

		$this->performAjaxValidation($model);
		if (isset($_POST['Certificates'])) {
			$model->attributes = $_POST['Certificates'];
			if (isset($_POST['Certificates']['when'])) {
				$model->when = $_POST['Certificates']['when'];
			}
			if (isset($_POST['Certificates']['who'])) {
				$model->who = $_POST['Certificates']['who'];
			}
			if (isset($_POST['Certificates']['prices'])) {
				$model->prices = $_POST['Certificates']['prices'];
			}
			if ($model->validate()) {
				$uploadedFile = CUploadedFile::getInstance($model, 'image');
				if ($model->save()) {
					$posts_keys = new CertificatePriceRelations();
					$posts_keys->add($model->prices, $model->getPrimaryKey());
					if ($uploadedFile != null) {
						$rnd = rand(0, 9999);
						$fileName = "{$rnd}-{$uploadedFile}";
						$uploadedFile->saveAs(Yii::app()->basePath . '/../images/certificates/' . $fileName, true);
						$model->image = $fileName;
						$model->save();
					}
					$this->redirect(array('/admin/certificates'));
				}
			}
		}
		$this->title = 'Изменить сертификат';
		$this->render('update', array('model' => $model));
	}

	public function actionIndex()
	{
		$model = new Certificates('search');
		$model->unsetAttributes();
		$model->columns = array(
			array(
				'name' => 'image',
				'type' => 'raw',
				'value' => '$data->getImage(array("width"=>"100"))',
			),
			'name',
			array(
				'name' => 'status',
				'value' => 'Yii::app()->params["visible_statuses"][$data->status]'
			),
			'date_add',
			'lifetime',
			array
			(
				'class' => 'CButtonColumn',
				'template' => '<span style="padding-right: 10px;">{update}</span>{delete}',
				'htmlOptions' => array(
					'style' => 'width:30px;',
					'class' => 'text_align_center'
				),
				'header' => '',
				'updateButtonImageUrl' => false,
				'deleteButtonImageUrl' => false,
				'deleteConfirmation' => 'Вы действительно хотите удалить сертификат?',
				'deleteButtonOptions' => array(
					'class' => 'icon-remove',
					'title' => 'Удалить'
				),
				'updateButtonOptions' => array(
					'class' => 'icon-edit',
					'title' => 'Редактировать'
				),
				'buttons' => array(
					'update' => array(
						'label' => '',
						'url' => 'Yii::app()->createUrl("admin/certificates/update", array("id"=>$data->id))',
					),
					'delete' => array(
						'label' => '',
					),
				),
			),
		);
		if (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER) {
			array_push(
				$model->columns,
				array(
					'header' => 'Организация',
					'value' => '!empty($data->partner->name)?$data->partner->name:""'
				)
			);
		}
		if (isset($_GET['Certificates'])) {
			$model->attributes = $_GET['Certificates'];
		}
		$this->title = 'Сертификаты';
		$this->render('admin', array('model' => $model));
	}

	public function actionDelete($id)
	{
		Certificates::model()->findByPk($id)->delete();
		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'certificates-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionPrices()
	{
		if (isset($_GET['q']) && ($keyword = trim($_GET['q'])) !== '') {
			$tags = CertificatePrices::model()->suggestTags($keyword);
			if ($tags !== array()) {
				echo implode("\n", $tags);
			}
		}
	}
}