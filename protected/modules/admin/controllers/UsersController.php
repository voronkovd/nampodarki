<?php

class UsersController extends Controller
{
	public $layout = '//layouts/column2';

	public function filters()
	{
		return array('accessControl', 'postOnly + delete');
	}

	public function beforeAction()
	{
		$this->menu = array(
			array(
				'label' => 'Партнеры',
				'url' => array('/admin/partners/'),
				'visible' => Yii::app()->user->role == Users::ADMIN
			),
			array(
				'label' => 'Добавить партнера',
				'url' => array('/admin/partners/create'),
				'visible' => Yii::app()->user->role == Users::ADMIN
			),
			array('label' => '<hr>', 'visible' => Yii::app()->user->role == Users::ADMIN),
			array('label' => 'Галерея', 'url' => array('/admin/PartnerGalleries/')),
			array(
				'label' => 'Добавить изображения',
				'url' => array('/admin/PartnerGalleries/create')
			),
			array('label' => '<hr>'),
			array('label' => 'Пользователи', 'url' => array('/admin/Users/')),
			array('label' => 'Добавить пользователя', 'url' => array('/admin/users/create')),
		);

		return true;

	}

	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions' => array('index', 'create', 'update', 'profile'),
				'roles' => array(Users::ADMIN, Users::PARTNER_ADMIN)
			),
			array('deny', 'users' => array('*'))
		);
	}

	public function actionCreate()
	{
		$model = new RegistrationForm();
		$this->performAjaxValidation($model);
		if (isset($_POST['RegistrationForm'])) {
			$model->attributes = $_POST['RegistrationForm'];
			if ($model->validate()) {
				$model->SaveUser();
				$this->redirect(Yii::app()->createUrl('admin/users'));
			}
		}
		$this->title = 'Добавить пользователя';
		$this->render('create', array('model' => $model));
	}

	public function actionUpdate($id)
	{
		$model = Users::model()->findByPk($id);
		$this->performAjaxValidation($model);
		if (isset($_POST['Users'])) {
			$model->attributes = $_POST['Users'];
			$model->role = $_POST['Users']['role'];
			if ($model->save()) {
				$this->redirect(Yii::app()->createUrl('admin/users'));
			}
		}
		$this->title = 'Изменить пользователя ' . $model->email;
		$this->render('update', array('model' => $model));
	}

	public function actionProfile($id)
	{
		$criteria = new CDbCriteria();
		$criteria->compare('user_id', $id);
		$model = UserProfiles::model()->find($criteria);
		if (empty($model)) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		$this->performAjaxValidation($model);
		if (isset($_POST['UserProfiles'])) {
			$model->attributes = $_POST['UserProfiles'];
			if ($model->save()) {
				$this->redirect(Yii::app()->createUrl('admin/users'));
			}
		}
		$this->title = 'Изменить профиль пользователя ' . $model->name;
		$this->render('profile', array('model' => $model));
	}

	public function actionIndex()
	{
		$model = new Users('search');
		$model->unsetAttributes();
		if (Yii::app()->user->role == Users::ADMIN) {
			$roles = Yii::app()->params['user_roles'];
		} else {
			$roles = Yii::app()->params['partner_roles'];
		}
		$model->columns = array(
			'email',
			array(
				'name' => 'role',
				'filter' => $roles,
				'value' => 'Yii::app()->params["user_roles"][$data->role]'
			),
			array(
				'name' => 'status',
				'filter' => Yii::app()->params['visible_statuses'],
				'value' => 'Yii::app()->params["visible_statuses"][$data->status]'
			),
			array(
				'header' => 'ФИО',
				'value' => '$data->profile->surname." ".$data->profile->name." ".$data->profile->patronymic'
			),
			array('header' => 'Телефон', 'value' => '$data->profile->phone'),
			array
			(
				'class' => 'CButtonColumn',
				'template' => '{update}',
				'htmlOptions' => array(
					'style' => 'width:30px;',
					'class' => 'text_align_center'
				),
				'header' => 'Профиль',
				'updateButtonImageUrl' => false,
				'updateButtonOptions' => array(
					'class' => 'icon-edit',
					'title' => 'Редактировать'
				),
				'viewButtonImageUrl' => false,
				'buttons' => array(
					'update' => array(
						'label' => '',
						'url' => 'Yii::app()->createUrl("admin/users/profile", array("id"=>$data->id))',
					),
				),
			),
			array
			(
				'class' => 'CButtonColumn',
				'template' => '{update}',
				'htmlOptions' => array(
					'style' => 'width:30px;',
					'class' => 'text_align_center'
				),
				'header' => 'Аккаунт',
				'updateButtonImageUrl' => false,
				'updateButtonOptions' => array(
					'class' => 'icon-edit',
					'title' => 'Редактировать'
				),
				'viewButtonImageUrl' => false,
				'buttons' => array(
					'update' => array(
						'label' => '',
						'url' => 'Yii::app()->createUrl("admin/users/update", array("id"=>$data->id))',
					),
				),
			),
		);
		if (Yii::app()->user->role == Users::ADMIN) {
			array_push(
				$model->columns,
				array(
					'header' => 'Организация',
					'value' => '!empty($data->partner->name)?$data->partner->name:""'
				)
			);
		}
		if (isset($_GET['Users'])) {
			$model->attributes = $_GET['Users'];
		}
		$this->title = 'Пользователи';
		$this->render('admin', array('model' => $model));
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}