<?php

class WhoIsCertificatesController extends Controller
{
	public $layout = '//layouts/column2';

	public function beforeAction()
	{
		$this->menu = array(
			array(
				'label' => 'Категории сертификатов',
				'url' => array('/admin/certificateCategories/'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Добавить категорию',
				'url' => array('/admin/certificateCategories/create'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array('label' => 'Сертификаты', 'url' => array('/admin/certificates/')),
			array('label' => 'Добавить сертификат', 'url' => array('/admin/certificates/create')),
			array('label' => '<hr>'),
			array(
				'label' => 'Список "кому"',
				'url' => array('/admin/whoIsCertificates/'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Добавить "кому"',
				'url' => array('/admin/whoIsCertificates/create'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Список "когда"',
				'url' => array('/admin/whenIsCertificates/'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Добавить "когда"',
				'url' => array('/admin/whenIsCertificates/create'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array('label' => 'Акции', 'url' => array('/admin/sellouts/')),
			array('label' => 'Добавить акцию', 'url' => array('/admin/sellouts/create')),
			array('label' => '<hr>'),
			array('label' => 'Цены', 'url' => array('/admin/certificatePrices/')),
			array('label' => 'Добавить цену', 'url' => array('/admin/certificatePrices/create'))
		);

		return true;

	}

	public function filters()
	{
		return array('accessControl', 'postOnly + delete');
	}

	public function accessRules()
	{
		return array(
			array('allow', 'actions' => array('index', 'create', 'update', 'delete'), 'roles' => array(Users::MANAGER)),
			array('deny', 'users' => array('*')),
		);
	}

	public function actionCreate()
	{
		$model = new WhoIsCertificates;
		$this->performAjaxValidation($model);
		if (isset($_POST['WhoIsCertificates'])) {
			$model->attributes = $_POST['WhoIsCertificates'];
			if ($model->save()) {
				$this->redirect(Yii::app()->createUrl("admin/WhoIsCertificates"));
			}
		}
		$this->title = 'Добавить "кому"';
		$this->render('create', array('model' => $model));
	}

	public function actionUpdate($id)
	{
		$model = WhoIsCertificates::model()->findByPk($id);
		$this->performAjaxValidation($model);
		if (isset($_POST['WhoIsCertificates'])) {
			$model->attributes = $_POST['WhoIsCertificates'];
			if ($model->save()) {
				$this->redirect(Yii::app()->createUrl("admin/WhoIsCertificates"));
			}
		}
		$this->title = 'Изменить "кому" ' . $model->name;
		$this->render('update', array('model' => $model));
	}

	public function actionIndex()
	{
		$model = new WhoIsCertificates('search');
		$model->unsetAttributes();
		$model->columns = array(
			'name',
			array
			(
				'class' => 'CButtonColumn',
				'template' => '{update}<span style="margin-left: 10px;">{delete}</span>',
				'htmlOptions' => array(
					'style' => 'width:30px;',
					'class' => 'text_align_center'
				),
				'header' => '',
				'updateButtonImageUrl' => false,
				'deleteButtonImageUrl' => false,
				'updateButtonOptions' => array(
					'class' => 'icon-edit',
					'title' => 'Редактировать'
				),
				'deleteButtonOptions' => array(
					'class' => 'icon-remove',
					'title' => 'Удалить'
				),
				'buttons' => array(
					'update' => array(
						'label' => '',
						'url' => 'Yii::app()->createUrl("admin/WhoIsCertificates/update", array("id"=>$data->id))',
					),
					'delete' => array(
						'label' => '',
					),
				),
			)
		);
		if (isset($_GET['WhoIsCertificates'])) {
			$model->attributes = $_GET['WhoIsCertificates'];
		}
		$this->title = 'Список "кому"';
		$this->render('admin', array('model' => $model));
	}

	public function actionDelete($id)
	{
		WhoIsCertificates::model()->findByPk($id)->delete();
		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'who-is-certificates-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
