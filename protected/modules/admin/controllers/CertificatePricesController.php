<?php

class CertificatePricesController extends Controller
{

	public $layout = '//layouts/column2';

	public function filters()
	{
		return array('accessControl', 'postOnly + delete');
	}

	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions' => array('index', 'create', 'update'),
				'roles' => array(Users::MANAGER, Users::PARTNER_MANAGER)
			),
			array('deny', 'users' => array('*')),
		);
	}

	public function beforeAction()
	{
		$this->menu = array(
			array(
				'label' => 'Категории сертификатов',
				'url' => array('/admin/certificateCategories/'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Добавить категорию',
				'url' => array('/admin/certificateCategories/create'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array('label' => 'Сертификаты', 'url' => array('/admin/certificates/')),
			array('label' => 'Добавить сертификат', 'url' => array('/admin/certificates/create')),
			array('label' => '<hr>'),
			array(
				'label' => 'Список "кому"',
				'url' => array('/admin/whoIsCertificates/'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Добавить "кому"',
				'url' => array('/admin/whoIsCertificates/create'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Список "когда"',
				'url' => array('/admin/whenIsCertificates/'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Добавить "когда"',
				'url' => array('/admin/whenIsCertificates/create'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array('label' => 'Акции', 'url' => array('/admin/sellouts/')),
			array('label' => 'Добавить акцию', 'url' => array('/admin/sellouts/create')),
			array('label' => '<hr>'),
			array('label' => 'Цены', 'url' => array('/admin/certificatePrices/')),
			array('label' => 'Добавить цену', 'url' => array('/admin/certificatePrices/create'))
		);

		return true;

	}

	public function actionCreate()
	{
		$model = new CertificatePrices;
		$this->performAjaxValidation($model);
		if (isset($_POST['CertificatePrices'])) {
			$model->attributes = $_POST['CertificatePrices'];
			if ($model->save()) {
				$this->redirect(array('/admin/certificatePrices'));
			}
		}
		$this->title = 'Добавить цену';
		$this->render('create', array('model' => $model));
	}


	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$this->performAjaxValidation($model);

		if (isset($_POST['CertificatePrices'])) {
			$model->attributes = $_POST['CertificatePrices'];
			if ($model->save()) {
				$this->redirect(array('/admin/certificatePrices'));
			}
		}
		$this->title = 'Изменить цену';
		$this->render('update', array('model' => $model));
	}

	public function actionIndex()
	{
		$model = new CertificatePrices('search');
		$model->unsetAttributes();
		$model->columns = array(
			'price',
			array
			(
				'class' => 'CButtonColumn',
				'template' => '<span style="padding-right: 10px;">{update}</span>{delete}',
				'htmlOptions' => array(
					'style' => 'width:30px;',
					'class' => 'text_align_center'
				),
				'header' => '',
				'updateButtonImageUrl' => false,
				'deleteButtonImageUrl' => false,
				'deleteConfirmation' => 'Вы действительно хотите удалить эту цену?',
				'deleteButtonOptions' => array(
					'class' => 'icon-remove',
					'title' => 'Удалить'
				),
				'updateButtonOptions' => array(
					'class' => 'icon-edit',
					'title' => 'Редактировать'
				),
				'buttons' => array(
					'update' => array(
						'label' => '',
						'url' => 'Yii::app()->createUrl("admin/certificatePrices/update", array("id"=>$data->id))',
					),
					'delete' => array(
						'label' => '',
					),
				),
			),
		);
		if (isset($_GET['CertificatePrices'])) {
			$model->attributes = $_GET['CertificatePrices'];
		}
		$this->title = 'Цены';
		$this->render('admin', array('model' => $model));
	}

	public function loadModel($id)
	{
		$model = CertificatePrices::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}

		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'certificate-prices-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
