<?php

class CertificateCategoriesController extends Controller
{

	public $layout = '//layouts/column2';

	public function beforeAction()
	{
		$this->menu = array(
			array(
				'label' => 'Категории сертификатов',
				'url' => array('/admin/certificateCategories/'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Добавить категорию',
				'url' => array('/admin/certificateCategories/create'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array('label' => 'Сертификаты', 'url' => array('/admin/certificates/')),
			array('label' => 'Добавить сертификат', 'url' => array('/admin/certificates/create')),
			array('label' => '<hr>'),
			array(
				'label' => 'Список "кому"',
				'url' => array('/admin/whoIsCertificates/'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Добавить "кому"',
				'url' => array('/admin/whoIsCertificates/create'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Список "когда"',
				'url' => array('/admin/whenIsCertificates/'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Добавить "когда"',
				'url' => array('/admin/whenIsCertificates/create'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array('label' => 'Акции', 'url' => array('/admin/sellouts/')),
			array('label' => 'Добавить акцию', 'url' => array('/admin/sellouts/create')),
			array('label' => '<hr>'),
			array('label' => 'Цены', 'url' => array('/admin/certificatePrices/')),
			array('label' => 'Добавить цену', 'url' => array('/admin/certificatePrices/create'))
		);

		return true;

	}

	public function filters()
	{
		return array('accessControl', 'postOnly + delete');
	}

	public function accessRules()
	{
		return array(
			array('allow', 'actions' => array('index', 'create', 'update', 'delete'), 'roles' => array(Users::MANAGER)),
			array('deny', 'users' => array('*'))
		);
	}


	public function actionCreate()
	{
		$model = new Categories;
		$this->performAjaxValidation($model);
		if (isset($_POST['Categories'])) {
			$model->attributes = $_POST['Categories'];
			if ($model->save()) {
				$this->redirect(Yii::app()->createUrl('admin/certificateCategories'));
			}
		}
		$this->title = 'Добавить категорию сертификатов';
		$this->render('create', array('model' => $model));
	}

	public function actionUpdate($id)
	{
		$model = Categories::model()->findByPk($id);
		$this->performAjaxValidation($model);
		if (isset($_POST['Categories'])) {
			$model->attributes = $_POST['Categories'];
			if ($model->save()) {
				$this->redirect(Yii::app()->createUrl('admin/certificateCategories'));
			}
		}
		$this->title = 'Изменить категорию сертификатов ' . $model->name;
		$this->render('update', array('model' => $model));
	}

	public function actionIndex()
	{
		$model = new Categories('search');
		$model->columns = array(
			'name',
			array(
				'name' => 'status',
				'filter' => Yii::app()->params['visible_statuses'],
				'value' => 'Yii::app()->params["visible_statuses"][$data->status]'
			),
			array
			(
				'class' => 'CButtonColumn',
				'template' => '<span style="padding-right: 10px;">{update}</span>{delete}',
				'htmlOptions' => array(
					'style' => 'width:30px;',
					'class' => 'text_align_center'
				),
				'header' => '',
				'updateButtonImageUrl' => false,
				'deleteButtonImageUrl' => false,
				'deleteConfirmation' => 'Удалив категорию, вы удалите все связанные с ней данные. Вы действительно хотите это сделать?',
				'deleteButtonOptions' => array(
					'class' => 'icon-remove',
					'title' => 'Удалить'
				),
				'updateButtonOptions' => array(
					'class' => 'icon-edit',
					'title' => 'Редактировать'
				),
				'buttons' => array(
					'update' => array(
						'label' => '',
						'url' => 'Yii::app()->createUrl("admin/certificateCategories/update", array("id"=>$data->id))',
					),
					'delete' => array(
						'label' => '',
					),
				),
			),
		);
		$model->unsetAttributes();
		if (isset($_GET['Categories'])) {
			$model->attributes = $_GET['Categories'];
		}
		$this->title = 'Категории сертификатов';
		$this->render('admin', array('model' => $model));
	}

	public function actionDelete($id)
	{
		CertificateCategories::model()->findByPk($id)->delete();
		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'certificate-categories-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}