<?php

class PartnerGalleriesController extends Controller
{

	public $layout = '//layouts/column2';

	public function filters()
	{
		return array('accessControl', 'postOnly + delete');
	}

	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions' => array('index', 'delete', 'create'),
				'roles' => array(Users::MANAGER, Users::PARTNER_MANAGER)
			),
			array('deny', 'users' => array('*')),
		);
	}

	public function beforeAction()
	{
		$this->menu = array(
			array(
				'label' => 'Партнеры',
				'url' => array('/admin/partners/'),
				'visible' => Yii::app()->user->role == Users::ADMIN
			),
			array(
				'label' => 'Добавить партнера',
				'url' => array('/admin/partners/create'),
				'visible' => Yii::app()->user->role == Users::ADMIN
			),
			array('label' => '<hr>', 'visible' => Yii::app()->user->role == Users::ADMIN),
			array('label' => 'Галерея', 'url' => array('/admin/PartnerGalleries/')),
			array(
				'label' => 'Добавить изображения',
				'url' => array('/admin/PartnerGalleries/create')
			),
			array('label' => '<hr>'),
			array('label' => 'Пользователи', 'url' => array('/admin/Users/')),
			array('label' => 'Добавить пользователя', 'url' => array('/admin/users/create')),
		);

		return true;

	}

	public function actionCreate()
	{
		$model = new PartnerGalleries;
		$this->performAjaxValidation($model);
		if (isset($_POST['PartnerGalleries'])) {
			$images = CUploadedFile::getInstances($model, 'file_names');
			if (isset($images) && count($images) > 0) {
				foreach ($images as $image => $pic) {
					$img_add = new PartnerGalleries();
					$img_add->partner_id = $_POST['PartnerGalleries']['partner_id'];
					$rnd = rand(0, 9999);
					$fileName = "{$rnd}-{$pic->name}";
					$img_add->file_name = $fileName;
					if ($img_add->save()) {
						$path = Yii::app()->getBasePath() . '/../images/gallery/';
						$pic->saveAs($path . $fileName);

					}
				}
			}
			$this->redirect(array('/admin/partnerGalleries'));
		}
		$this->title = 'Добавить изображение';
		$this->render('create', array('model' => $model));
	}


	public function actionDelete($id)
	{
		PartnerGalleries::model()->findByPk($id)->delete();
		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}


	public function actionIndex()
	{
		$model = new PartnerGalleries('search');
		$model->unsetAttributes();
		$model->columns = array(
			array(
				'name' => 'file_name',
				'type' => 'raw',
				'filter' => false,
				'value' => '$data->getImage(array("width"=>"100"))',
			),
			array
			(
				'class' => 'CButtonColumn',
				'template' => '{delete}',
				'htmlOptions' => array(
					'style' => 'width:30px;',
					'class' => 'text_align_center'
				),
				'header' => '',
				'deleteButtonImageUrl' => false,
				'deleteButtonOptions' => array(
					'class' => 'icon-remove',
					'title' => 'Удалить'
				),
				'buttons' => array(
					'delete' => array(
						'label' => '',
					),
				),
			)
		);
		if (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER) {
			array_push(
				$model->columns,
				array(
					'header' => 'Организация',
					'value' => '!empty($data->partner->name)?$data->partner->name:""'
				)
			);
		}
		if (isset($_GET['PartnerGalleries'])) {
			$model->attributes = $_GET['PartnerGalleries'];
		}
		$this->title = 'Галлерея';
		$this->render('admin', array('model' => $model));
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'partner-galleries-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
