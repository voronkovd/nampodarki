<?php

class SelloutsController extends Controller
{

	public $layout = '//layouts/column2';

	public function beforeAction()
	{
		$this->menu = array(
			array(
				'label' => 'Категории сертификатов',
				'url' => array('/admin/certificateCategories/'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Добавить категорию',
				'url' => array('/admin/certificateCategories/create'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array('label' => 'Сертификаты', 'url' => array('/admin/certificates/')),
			array('label' => 'Добавить сертификат', 'url' => array('/admin/certificates/create')),
			array('label' => '<hr>'),
			array(
				'label' => 'Список "кому"',
				'url' => array('/admin/whoIsCertificates/'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Добавить "кому"',
				'url' => array('/admin/whoIsCertificates/create'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Список "когда"',
				'url' => array('/admin/whenIsCertificates/'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => 'Добавить "когда"',
				'url' => array('/admin/whenIsCertificates/create'),
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER)
			),
			array('label' => 'Акции', 'url' => array('/admin/sellouts/')),
			array('label' => 'Добавить акцию', 'url' => array('/admin/sellouts/create')),
			array('label' => '<hr>'),
			array('label' => 'Цены', 'url' => array('/admin/certificatePrices/')),
			array('label' => 'Добавить цену', 'url' => array('/admin/certificatePrices/create'))
		);

		return true;

	}

	public function filters()
	{
		return array('accessControl', 'postOnly + delete');
	}

	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions' => array('index', 'create', 'update', 'certificate'),
				'roles' => array(Users::MANAGER, Users::PARTNER_MANAGER)
			),
			array('deny', 'users' => array('*')),
		);
	}

	public function actionCreate()
	{
		$model = new Sellouts;
		$this->performAjaxValidation($model);
		if (isset($_POST['Sellouts'])) {
			$model->attributes = $_POST['Sellouts'];
			$rnd = rand(0, 9999);
			$uploadedFile = CUploadedFile::getInstance($model, 'image');
			$fileName = "{$rnd}-{$uploadedFile}";
			$model->image = $fileName;
			if ($model->save()) {
				$path = Yii::app()->getBasePath() . '/../images/sellouts/';
				$uploadedFile->saveAs($path . $fileName);
				$this->redirect(array('/admin/sellouts/'));
			}
		}
		$this->title = 'Добавить акцию';
		$this->render('create', array('model' => $model));
	}

	public function actionUpdate($id)
	{
		$model = Sellouts::model()->findByPk($id);
		$this->performAjaxValidation($model);
		if (isset($_POST['Sellouts'])) {
			$model->attributes = $_POST['Sellouts'];
			$rnd = rand(0, 9999);
			$uploadedFile = CUploadedFile::getInstance($model, 'image');
			$fileName = "{$rnd}-{$uploadedFile}";
			$model->image = $fileName;
			if ($model->save()) {
				if ($uploadedFile != null) {
					$rnd = rand(0, 9999);
					$fileName = "{$rnd}-{$uploadedFile}";
					$uploadedFile->saveAs(Yii::app()->basePath . '/../images/sellouts/' . $fileName, true);
					$model->image = $fileName;
					$model->save();
				}
				$this->redirect(array('/admin/sellouts/'));
			}
		}
		$this->title = 'Изменить акцию';
		$this->render('update', array('model' => $model));
	}

	public function actionIndex()
	{
		$model = new Sellouts('search');
		$model->unsetAttributes();
		$model->columns = array(
			array(
				'name' => 'image',
				'type' => 'raw',
				'value' => '$data->getImage(array("width"=>"100"))',
			),
			'name',
			array(
				'name' => 'status',
				'value' => 'Yii::app()->params["visible_statuses"][$data->status]'
			),
			'date_add',
			'discount',
			'lifetime',
			array
			(
				'class' => 'CButtonColumn',
				'template' => '{update}<span style="margin-left: 10px;">{view}</span>',
				'htmlOptions' => array(
					'style' => 'width:30px;',
					'class' => 'text_align_center'
				),
				'header' => '',
				'updateButtonImageUrl' => false,
				'viewButtonImageUrl' => false,
				'updateButtonOptions' => array(
					'class' => 'icon-edit',
					'title' => 'Редактировать'
				),
				'viewButtonOptions' => array(
					'class' => 'icon-cog',
					'title' => 'Сертификаты'
				),
				'buttons' => array(
					'update' => array(
						'label' => '',
						'url' => 'Yii::app()->createUrl("admin/sellouts/update", array("id"=>$data->id))',
					),
					'view' => array(
						'label' => '',
						'url' => 'Yii::app()->createUrl("admin/sellouts/certificate", array("id"=>$data->id))',

					),
				),
			),
		);
		if (isset($_GET['Sellouts'])) {
			$model->attributes = $_GET['Sellouts'];
		}
		$this->title = 'Акции';
		$this->render('admin', array('model' => $model));
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'sellouts-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionCertificate($id)
	{
		$model = new SelloutCertificates();
		$criteria = new CDbCriteria();
		$criteria->compare('sellout_id', $id);
		$certificates = Certificates::model()->public()->findAll();
		$data = array();
		$children = array();
		foreach ($certificates as $certificate) {
			if ($certificate->price) {
				foreach ($certificate->price as $price) {
					$children[] = array('id' => $price->id, 'title' => $price->price->price);
				}
			}
			$data[] = array(
				'id' => 0,
				'title' => $certificate->name,
				'icon' => false,
				'children' => $children
			);
		}
		if (isset($_POST['SelloutCertificates'])) {
			SelloutCertificates::model()->deleteAll($criteria);
			foreach ($_POST['SelloutCertificates']['certificates'] as $certificate_id) {
				if (!empty($certificate_id)) {
					$mode = new SelloutCertificates();
					$mode->sellout_id = $id;
					$mode->certificate_price_relation_id = $certificate_id;
					$mode->save();
				}
			}
		}
		$certificates = SelloutCertificates::model()->findAll($criteria);
		$certificates = array_values(
			CHtml::listData($certificates, 'certificate_price_relation_id', 'certificate_price_relation_id')
		);
		$this->title = 'Сертификаты акции';
		$this->render('certificate', array('model' => $model, 'data' => $data, 'active' => $certificates));
	}
}