<?php

class PartnersController extends Controller
{

	public $layout = '//layouts/column2';

	public function beforeAction()
	{
		$this->menu = array(
			array(
				'label' => 'Партнеры',
				'url' => array('/admin/partners/'),
				'visible' => Yii::app()->user->role == Users::ADMIN
			),
			array(
				'label' => 'Добавить партнера',
				'url' => array('/admin/partners/create'),
				'visible' => Yii::app()->user->role == Users::ADMIN
			),
			array('label' => '<hr>', 'visible' => Yii::app()->user->role == Users::ADMIN),
			array('label' => 'Галерея', 'url' => array('/admin/PartnerGalleries/')),
			array(
				'label' => 'Добавить изображения',
				'url' => array('/admin/PartnerGalleries/create')
			),
			array('label' => '<hr>'),
			array('label' => 'Пользователи', 'url' => array('/admin/Users/')),
			array('label' => 'Добавить пользователя', 'url' => array('/admin/users/create')),
		);

		return true;

	}

	public function filters()
	{
		return array('accessControl', 'postOnly + delete');
	}

	public function accessRules()
	{
		return array(
			array('allow', 'actions' => array('index', 'create', 'update', 'view'), 'roles' => array(Users::ADMIN)),
			array('deny', 'users' => array('*'))
		);
	}

	public function actionCreate()
	{

		$model = new Partners();
		$this->performAjaxValidation($model);
		$post = $_POST;
		if (isset($post['Partners'])) {
			$model->attributes = $post['Partners'];
			if ($model->validate()) {
				$rnd = rand(0, 9999);
				$uploadedFile = CUploadedFile::getInstance($model, 'image_name');
				$fileName = "{$rnd}-{$uploadedFile}";
				$model->image_name = $fileName;
				if ($model->save()) {
					$path = Yii::app()->getBasePath() . '/../images/partners/';
					$uploadedFile->saveAs($path . $fileName);
					$this->redirect(array('/admin/partners'));
				}
			}
		}
		$this->title = 'Добавить партнера';
		$this->render('create', array('model' => $model));
	}

	public function actionUpdate($id)
	{
		$model = Partners::model()->findByPk($id);
		$this->performAjaxValidation($model);
		$post = $_POST;
		if (isset($post['Partners'])) {
			$model->attributes = $post['Partners'];
			if ($model->validate()) {
				$uploadedFile = CUploadedFile::getInstance($model, 'image_name');
				if ($model->save()) {
					if ($uploadedFile != null) {
						$rnd = rand(0, 9999);
						$fileName = "{$rnd}-{$uploadedFile}";
						$uploadedFile->saveAs(Yii::app()->basePath . '/../images/partners/' . $fileName, true);
						$model->image_name = $fileName;
						$model->save();
					}
					$this->redirect(array('/admin/partners'));
				}
			}
		}
		$this->title = 'Изменить партнера ' . $model->name;
		$this->render('update', array('model' => $model));
	}

	public function actionIndex()
	{
		$model = new Partners('search');
		$model->columns = array(
			array(
				'name' => 'image_name',
				'type' => 'raw',
				'filter' => false,
				'value' => '$data->getImage(array("width"=>"100"))',
			),
			array(
				'name' => 'name',
				'type' => 'raw',
				'value' => 'CHtml::link($data->name, Yii::app()->createUrl("admin/partners/view", array("id"=>$data->id)))'
			),
			array(
				'name' => 'status',
				'filter' => Yii::app()->params['visible_statuses'],
				'value' => 'Yii::app()->params["visible_statuses"][$data->status]'
			),
			array
			(
				'class' => 'CButtonColumn',
				'template' => '{update}',
				'htmlOptions' => array(
					'style' => 'width:30px;',
					'class' => 'text_align_center'
				),
				'header' => '',
				'updateButtonImageUrl' => false,
				'updateButtonOptions' => array(
					'class' => 'icon-edit',
					'title' => 'Редактировать'
				),
				'buttons' => array(
					'update' => array(
						'label' => '',
						'url' => 'Yii::app()->createUrl("admin/partners/update", array("id"=>$data->id))',
					),
				),
			),
		);
		$model->unsetAttributes();
		if (isset($_GET['Partners'])) {
			$model->attributes = $_GET['Partners'];
		}
		$this->title = 'Партнеры';
		$this->render('admin', array('model' => $model));

	}

	public function actionView($id)
	{
		$this->render('view', array('model' => Partners::model()->findByPk($id)));
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'partners-form'):
			echo CActiveForm::validate($model);
			Yii::app()->end();
		endif;
	}
}