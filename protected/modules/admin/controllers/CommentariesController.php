<?php

class CommentariesController extends Controller
{

	public $layout = '//layouts/column2';

	public function filters()
	{
		return array('accessControl', 'postOnly + delete');
	}

	public function beforeAction()
	{
		$this->menu = array(
			array(
				'label' => 'Список новостей',
				'url' => array('/admin/news/'),

			),
			array(
				'label' => 'Добавить новость',
				'url' => array('/admin/news/create')
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::MANAGER || Yii::app()->user->role == Users::ADMIN)
			),
			array(
				'label' => 'Статьи',
				'url' => array('/admin/posts/'),
				'visible' => (Yii::app()->user->role == Users::MANAGER || Yii::app()->user->role == Users::ADMIN)
			),
			array(
				'label' => 'Добавить статью',
				'url' => array('/admin/posts/create'),
				'visible' => (Yii::app()->user->role == Users::MANAGER || Yii::app()->user->role == Users::ADMIN)
			),
			array(
				'label' => '<hr>',
				'visible' => (Yii::app()->user->role == Users::MANAGER || Yii::app()->user->role == Users::ADMIN)
			),
			array(
				'label' => 'Комментарии',
				'url' => array('/admin/commentaries/'),
				'visible' => (Yii::app()->user->role == Users::MANAGER || Yii::app()->user->role == Users::ADMIN)
			),
		);

		return true;

	}

	public function accessRules()
	{
		return array(

			array('allow', 'actions' => array('index', 'delete'), 'roles' => array(Users::MANAGER)),
			array('deny', 'users' => array('*')),
		);
	}


	public function actionDelete($id)
	{
		Commentaries::model()->findByPk($id)->delete();
		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}


	public function actionIndex()
	{
		$model = new Commentaries('search');
		$model->unsetAttributes();
		$model->columns = array(
			array(
				'name' => 'user_id',
				'value' => '$data->user->profile->surname." ".$data->user->profile->name'
			),
			'date_add',
			array(
				'name' => 'content',
				'value' => 'UrlHelper::cutString($data->content)'
			),
			array(
				'name' => 'parent_id',
				'value' => '$data->getParent()'
			),
			array
			(
				'class' => 'CButtonColumn',
				'template' => '{delete}',
				'htmlOptions' => array(
					'style' => 'width:30px;',
					'class' => 'text_align_center'
				),
				'header' => '',
				'deleteButtonImageUrl' => false,
				'deleteButtonOptions' => array(
					'class' => 'icon-remove',
					'title' => 'Удалить'
				),
				'buttons' => array(
					'delete' => array(
						'label' => '',
					),
				),
			)
		);
		if (isset($_GET['Commentaries'])) {
			$model->attributes = $_GET['Commentaries'];
		}
		$this->title = 'Комментарии';
		$this->render('admin', array('model' => $model));
	}
}
