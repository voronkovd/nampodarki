<?php

class OrdersController extends Controller
{

	public $layout = '//layouts/column2';

	public function filters()
	{
		return array('accessControl', 'postOnly + delete');
	}

	public function accessRules()
	{
		return array(
			array('allow', 'actions' => array('index', 'view', 'update'), 'roles' => array(Users::MANAGER)),
			array('deny', 'users' => array('*')),
		);
	}

	public function actionView($id)
	{
		$this->title = 'Заказ';
		$this->render('view', array('model' => $this->loadModel($id)));
	}


	public function actionUpdate($id, $order)
	{
		$model = $this->loadModel($id);
		$model->current_status = (int)$order;
		$model->manager_id = Yii::app()->user->id;
		if ($model->save()) {
			$this->redirect(array('view', 'id' => $model->id));
		}
		$this->render('update', array('model' => $model));
	}

	public function actionIndex()
	{
		$model = new Orders('search');
		$model->unsetAttributes();
		$model->columns = array(
			'id',
			array(
				'name' => 'user_id',
				'value' => '$data->user->profile->surname." ".$data->user->profile->name." ".$data->user->profile->patronymic'
			),
			array(
				'name' => 'manager_id',
				'value' => '!empty($data->manager_id)?$data->manager->profile->surname." ".$data->manager->profile->name." ".$data->manager->profile->patronymic:""'
			),
			array(
				'name' => 'current_status',
				'filter' => Yii::app()->params['orders_roles'],
				'value' => 'Yii::app()->params["orders_roles"][$data->current_status]'
			),
			array
			(
				'class' => 'CButtonColumn',
				'template' => '{update}',
				'htmlOptions' => array(
					'style' => 'width:30px;',
					'class' => 'text_align_center'
				),
				'header' => '',
				'updateButtonImageUrl' => false,
				'updateButtonOptions' => array(
					'class' => 'icon-eye-open',
					'title' => 'подробнее...'
				),
				'buttons' => array(
					'update' => array(
						'label' => '',
						'url' => 'Yii::app()->createUrl("admin/orders/view", array("id"=>$data->id))',
					),
				),
			)
		);
		if (isset($_GET['Orders'])) {
			$model->attributes = $_GET['Orders'];
		}
		$this->title = 'Заказы';
		$this->render('admin', array('model' => $model));
	}

	public function loadModel($id)
	{
		$model = Orders::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}

		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'orders-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
