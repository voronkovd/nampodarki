<?php

class DistributionCommand extends CConsoleCommand
{

	// usage php protected/yiic.php Distribution News
	public function actionNews()
	{
		$news = new News();
		$data = $news->getDistribution();
		$users = $this->getUsers('news_subscription');
		if (!empty($users) && !empty($data)) {
			foreach ($users as $user) {
				$name = '=?UTF-8?B?"Нам подарки"?=';
				$subject = '=?UTF-8?B?Рассылка новостей с сайта "Нам подарки"?=';
				$headers = "From: $name <robot@nampodarki.ru>\r\n" .
					"Reply-To: robot@nampodarki.ru\r\n" .
					"MIME-Version: 1.0\r\n" .
					"Content-Type: text/html; charset=UTF-8";
				$body = "Здравствуйте, {$user->surname} {$user->name} {$user->patronymic}!<br />";
				$body = $body . " У нас есть для вас новости: <br />";
				foreach ($data as $new) {
					$body = $body . "<a href='http://nampodarki.ru/news/" . $new->id . "' >" . $new->title . "</a><br />";
				}
				mail($user->user->email, $subject, $body, $headers);

			}
		}


	}

	// usage php protected/yiic.php Distribution Certificates
	public function actionCertificates()
	{
		$news = new CertificatePrices();
		$data = $news->getDistribution();
		if (!empty($users) && !empty($data)) {
			foreach ($users as $user) {
				$name = '=?UTF-8?B?"Нам подарки"?=';
				$subject = '=?UTF-8?B?Рассылка новых сертификатов с сайта "Нам подарки"?=';
				$headers = "From: $name <robot@nampodarki.ru>\r\n" .
					"Reply-To: robot@nampodarki.ru\r\n" .
					"MIME-Version: 1.0\r\n" .
					"Content-Type: text/html; charset=UTF-8";
				$body = "Здравствуйте, {$user->surname} {$user->name} {$user->patronymic}!<br />";
				$body = $body . " У нас появились новые сертификаты: <br />";
				foreach ($data as $new) {
					$body = $body . "<a href='http://nampodarki.ru/certificates/" . $new->certificate->id . "' >" . $new->certificate->name . "</a><br />";
				}
				mail($user->user->email, $subject, $body, $headers);

			}
		}
	}

	public function getUsers($type = 'certificates_subscription')
	{
		$criteria = new CDbCriteria();
		if ($type == 'news_subscription') {
			$criteria->compare('news_subscription', 2);
		} else {
			$criteria->compare('certificates_subscription', 2);
		}

		return UserProfiles::model()->findAll($criteria);


	}
}