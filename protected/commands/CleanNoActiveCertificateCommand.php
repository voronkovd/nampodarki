<?php

class CleanNoActiveCertificateCommand extends CConsoleCommand
{

	// usage php protected/yiic.php CleanNoActiveCertificate Run
	public function run()
	{
		$today = date('Y-m-d');
		$criteria = new CDbCriteria();
		$criteria->compare('status', Yii::app()->params['visible_status_yes']);
		$certificates = Certificates::model()->findAll($criteria);
		if (!empty($certificates)) {
			foreach ($certificates as $certificate) {
				if (strtotime($certificate->lifetime) < strtotime($today . ' 00:00:00')) {
					$this->changeStatus($certificate->id);
				}
			}
		}
	}

	protected function changeStatus($id)
	{
		$new_model = Certificates::model()->findByPk($id);
		$new_model->status = Yii::app()->params['visible_status_no'];
		$new_model->save();
	}

}