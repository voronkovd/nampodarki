<?php

return array(
	'salt' => '0y1u6^YCfmS3t!C9cK9EM14gp-(c!Fnz_LJ(3s!i',
	'adminEmail' => 'mail@nampodarki.ru',
	'visible_status_yes' => 1,
	'visible_status_no' => 2,
	'partner_user_head' => 1,
	'partner_user_manager' => 2,
	'user_client' => 1,
	'user_manager' => 2,
	'user_admin' => 3,
	'order_new' => 1,
	'order_purchased' => 2,
	'order_cancel' => 3,
	'visible_statuses' => array(1 => 'Да', 2 => 'Нет'),
	'partner_roles' => array(3 => 'Сотрудник', 4 => 'Руководитель'),
	'user_roles' => array(
		1 => 'Пользователь',
		2 => 'Менеджер',
		5 => 'Администратор',
		3 => 'Сотрудник',
		4 => 'Руководитель'
	),
	'orders_roles' => array(1 => 'Новый', 2 => 'Оплачен', 3 => 'Отказ'),
);