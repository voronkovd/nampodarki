<?php

return array(
	'guest' => array(
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Guest',
		'bizRule' => null,
		'data' => null
	),
	Users::AUTH_USER => array(
		'type' => CAuthItem::TYPE_ROLE,
		'children' => array('guest'),
		'description' => 'auth_user',
		'bizRule' => null,
		'data' => null
	),
	Users::MANAGER => array(
		'type' => CAuthItem::TYPE_ROLE,
		'children' => array(Users::AUTH_USER),
		'description' => 'manager',
		'bizRule' => null,
		'data' => null
	),
	Users::PARTNER_MANAGER => array(
		'type' => CAuthItem::TYPE_ROLE,
		'children' => array('guest'),
		'description' => 'partner_manager',
		'bizRule' => null,
		'data' => null
	),
	Users::PARTNER_ADMIN => array(
		'type' => CAuthItem::TYPE_ROLE,
		'children' => array(Users::PARTNER_MANAGER),
		'description' => 'partner_admin',
		'bizRule' => null,
		'data' => null
	),
	Users::ADMIN => array(
		'type' => CAuthItem::TYPE_ROLE,
		'children' => array(Users::MANAGER),
		'description' => 'admin',
		'bizRule' => null,
		'data' => null
	)
);