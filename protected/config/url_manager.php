<?php

return array(
	'urlFormat' => 'path',
	'showScriptName' => false,
	'rules' => array(
		# users
		'login' => 'users/auth/login',
		'logout' => 'users/User/logout',
		'registration' => 'users/auth/registration',
		'forgotpassword' => 'users/auth/forgotAuth',
		'changepassword' => 'users/User/ChangePassword',
		'changeprofile' => 'users/User/Update',
		'self' => 'users/user/self',
		# static
		'about' => 'site/stat/view/about',
		'clients' => 'site/stat/view/clients',
		'faq' => 'site/stat/view/faq',
		'partnership' => 'site/stat/view/partnership',
		'payment' => 'site/stat/view/payment',
		# other
		'basket' => 'orders/basket',
		'<controller:\w+>/<id:\d+>' => '<controller>/view',
		'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
		'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
	),
);