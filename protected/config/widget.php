<?php

return array(
	'widgets' => array(
		'CJuiDatePicker' => array(
			'language' => 'ru',
			'defaultOptions' => array(
				'dateFormat' => 'yy-mm-dd',
				'yearRange' => '2013:' . date('Y'),
			),
			'htmlOptions' => array('size' => 15)
		),
		'CGridView' => array(
			'cssFile' => '/themes/admin/css/griedview.css',
			'summaryText' => '',
			'pagerCssClass' => 'pagination',
			'pager' => array(
				'class' => 'CLinkPager',
				'cssFile' => '/themes/admin/css/griedview.css',
				'header' => '',
			),
		),
		'CListView' => array(
			'summaryText' => '',
			'pagerCssClass' => 'pagination',
			'pager' => array(
			//				'cssFile' => '/themes/default/css/griedview.css',
			),
		),
	),
);