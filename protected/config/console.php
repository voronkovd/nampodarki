<?php

return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => '',
	'preload' => array('log'),
	'import' => require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'imports.php'),
	'components' => array(
		'db' => require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'db.php'),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
	),
);