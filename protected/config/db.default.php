<?php

return array(
	'connectionString' => 'mysql:host=localhost;dbname=nam',
	'emulatePrepare' => true,
	'schemaCachingDuration' => 3600,
	'username' => 'root',
	'password' => 'password',
	'charset' => 'utf8',
);