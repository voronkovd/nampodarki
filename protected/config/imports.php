<?php

return array(
	'application.models.*',
	'application.components.*',
	'application.models.certificates.*',
	'application.models.orders.*',
	'application.models.users.*',
	'application.models.partners.*',
	'application.models.writer.*',
	'system.zii.widgets.*',
);