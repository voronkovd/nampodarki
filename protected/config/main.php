<?php

return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => '',
	'preload' => array('log'),
	'sourceLanguage' => 'ru',
	'theme' => 'default',
	'language' => 'ru',
	'import' => require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'imports.php'),
	'modules' => array(
		'admin',
		'users',
		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'password' => 'pass',
			'ipFilters' => array('127.0.0.1', '::1'),
		),
	),
	'components' => array(
		'cache' => require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'cache.php'),
		'clientScript' => array(
			'scriptMap' => array(
			    //   'jquery.js' => '/js/jquery-1.9.1.js',
				//   'jquery.min.js' => '/js/jquery-1.9.1.js',
				//   'jquery-ui.js' => '/js/jquery-ui-1.10.3.custom.js',
				//   'jquery-ui.min.js' => '/js/jquery-ui-1.10.3.custom.js',
			),
		),
		'widgetFactory' => require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'widget.php'),
		'user' => array(
			'allowAutoLogin' => true,
			'class' => 'WebUser',
			'stateKeyPrefix' => 'partner',
			'loginUrl' => array('/login'),
		),
		'authManager' => array(
			'class' => 'MyAuthManager',
			'defaultRoles' => array('guest'),
		),
		'urlManager' => require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'url_manager.php'),
		'db' => require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'db.php'),
		'errorHandler' => array('errorAction' => 'site/error'),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'profile',
					'categories' => 'system.*'
				),
				array('class' => 'CProfileLogRoute', 'report' => 'summary'),
			),
		),
	),
	'params' => require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'params.php')
);