<?php echo  !empty($message)?$message:'';?>
<?php if (!empty($model) && empty($message)): ?>
	<form method="POST">
		<ul>
			<?php foreach ($model['goods'] as $data): ?>
				<li id="good<?php echo $data['id']; ?>">
					<input name="Orders[certificate_id][]" type="hidden" value="<?php echo $data['id']; ?>"/>
					<input name="Orders[price_id][]" type="hidden" value="<?php echo $data['price_id']; ?>"/>
					<input name="Orders[price_value][]" type="hidden" value="<?php echo $data['price']; ?>"/>
					<?php echo $data['name']; ?> по цене: <?php echo $data['price']; ?>
					<a id="<?php echo $data['id']; ?>" price_id="<?php echo $data['price_id']; ?>" href="#"
					   class="remove">Убрать с корзины</a>
				</li>

			<?php endforeach ?>
		</ul>
		<input onclick="checkAccess();" class="order-button rounded" type="button" value="Оформить заказа">
	</form>
	<script>
		function checkAccess() {
			$.ajax({
				url: '/ajax/isAuth',
				success: function (data) {
					if (data == 0) {
						alert('Пожалуйста авторизуйтесь');
						return false;
					} else {
						$('form').submit();
					}
				}
			});
		}
		$(document).ready(function () {
			$('.remove').click(function () {
				id = $('.remove').attr('id');
				price_id = $('.remove').attr('price_id');
				data = {'id': id, 'price_id': price_id}
				$.ajax({
					url: '/ajax/RemoveFromBasket',
					data: data,
					success: function (html) {
						$('#good' + id).remove();
						$('.view-basket').html(html);
					}
				});

			});
		});
	</script>
<?php else: ?>
<?php endif; ?>