<div class="column lf-column inline">
	<div class="column">
		<div class="thumb"><?php echo $data->image(184, 184); ?></div>
		<div class="price">
			<span class="nominal">300</span> рублей
		</div>
	</div>
	<div class="column info">
		<h3><a href="<?php echo Yii::app()->createUrl('certificates/view', array('id' => $data->id)); ?>">Сертификат
				"<?php echo $data->name; ?>"</a></h3>

		<p>Только вы знаете, что ей нужно 8 марта. Но только она сама снова должна это выбрать для себя...</p>
		<a href="#" class="detail" onclick="showDetailPopup(1);return false;">Подробнее</a><br/>
		<a id="<?php echo $data->id; ?>" price_id="<?php echo $data->price[0]->price->id; ?>"
		   name="<?php echo $data->name; ?>"
		   price="<?php echo $data->price[0]->price->price; ?>"
		   href="#" class="order-button rounded addtoBasket ">Заказать</a>
	</div>
	<div class="clear"></div>
</div>