<div class="view">
	<?php echo CHtml::link(
		$data->title,
		Yii::app()->createUrl('posts/view', array('id' => $data->id))
	); ?>
	<br />

	<?php echo $data->image(100, 100); ?>
	<br />

	<?php echo CHtml::encode($data->anons); ?>
	<br />

	<?php echo $data->content; ?>
	<br />

	<?php echo CHtml::encode($data->date_add); ?>
	<br />
	<?php echo CHtml::encode($data->partner_id); ?>
	<br />
</div>