<h1>View Posts #<?php echo $model->id; ?></h1>
<?php echo $model->image(200, 200); ?>
<?php $this->widget(
	'zii.widgets.CDetailView',
	array(
		'data' => $model,
		'attributes' => array(
			'id',
			'title',
			'anons',
			'content',
			'date_add',
		),
	)
); ?>
