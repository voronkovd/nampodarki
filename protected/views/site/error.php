<?php if (YII_DEBUG != true): ?>
	<h1>404</h1>
	<b>СТРАНИЦА НЕ НАЙДЕНА</b>
<?php else: ?>
	<h1>Ошибка <?php echo $code; ?></h1>
	<div class="error">
		<?php echo CHtml::encode($message); ?>
	</div>
<?php endif; ?>