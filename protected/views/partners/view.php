<h1><?php echo $model->name; ?></h1>
<?php echo $model->image(200, 200); ?>
<br/>
<?php echo CHtml::link('Сертификаты', Yii::app()->createUrl('certificates/partners', array('id' => $model->id))); ?>
<br/>
<?php if (!empty($model->galleries)): ?>
	<?php foreach ($model->galleries as $gallery): ?>
		<?php echo $gallery->image(200, 200); ?><br/>
	<?php endforeach; ?>
<?php endif; ?>
