<div class="view">
	<?php echo $model->image(200, 200); ?>
	<br/>
	<b>Наименование</b>: <?php echo CHtml::encode($model->name); ?>
	<br/>
	<b>Анонс</b>: <?php echo $model->anons; ?>
	<br/>
	<b>Скидка</b>: <?php echo CHtml::encode($model->discount); ?> %
	<br/>
	<b>Дата окончания</b>: <?php echo $model->lifetime; ?>
	<br/>
	<?php echo CHtml::link('Сертификаты', Yii::app()->createUrl('certificates/sellouts', array('id' => $model->id))); ?>
</div>