<div class="view">
	<?php echo $data->image(); ?>
	<br/>
	<b>Наименование</b>: <?php echo CHtml::link(
		$data->name,
		Yii::app()->createUrl('sellouts/view', array('id' => $data->id))
	); ?>
	<br/>
	<b>Анонс</b>: <?php echo $data->anons; ?>
	<br/>
	<b>Дата окончания</b>: <?php echo $data->lifetime; ?>
	<br/>
</div>