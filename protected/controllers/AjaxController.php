<?php

class AjaxController extends Controller
{
	public $layout = '//layouts/main';

	public function actionCalendar()
	{
		$model = new WhenIsCertificates();
		$data = array();
		foreach ($model as $when) {
			$data[] = array(
				'data' => date('Y') . '-' . $when['date_when'] . ' 00:00:00',
				"type" => "meeting",
				"title" => $when['name']
			);
		}
		echo json_encode($data);
	}

	public function actionAddToBasket($id = null, $price_id = null, $price = 0, $count = 1, $name='')
	{
		$cookie = (isset(Yii::app()->request->cookies['goods']->value)) ?
			Yii::app()->request->cookies['goods']->value : '';
		$cookie = json_decode($cookie, true);
		$all = 0;
		$all_price = 0;
		$data = array(array('id' => $id, 'price_id' => $price_id, 'price' => $price, 'count' => $count, 'name'=>$name));
		if (!empty($cookie) && !empty($cookie['goods'])) {
			$data = array_merge($cookie['goods'], $data);
		}
		foreach ($data as $good) {
			$all = $all + $good['count'];
			$all_price = $all_price + $good['price'];
		}
		$class = new stdClass();
		$class->goods = $data;
		$message = '<b>' . $all . '</b> СЕРТИФИКАТА<br/> НА СУММУ<br/><b>' . $all_price . '</b> РУБЛЕЙ';
		$class->in_basket = $message;
		$class = json_encode($class);
		$cookie = new CHttpCookie('goods', $class);
		$cookie->expire = time() + 60 * 60 * 24 * 180;
		Yii::app()->request->cookies['goods'] = $cookie;
		echo $message;

	}

	public function actionRemoveFromBasket($id = null, $price_id = null)
	{
		$cookie = (isset(Yii::app()->request->cookies['goods']->value)) ?
			Yii::app()->request->cookies['goods']->value : '';
		$cookie = json_decode($cookie, true);
		$all = 0;
		$all_price = 0;
		if (!empty($cookie) && !empty($cookie['goods'])) {
			$data = $cookie['goods'];
			$key_del = null;
			foreach ($data as $key => $good) {
				if ($good['id'] == $id && $good['price_id'] == $price_id) {
					$key_del = $key;
				}
			}
			unset($data[$key_del]);
			foreach ($data as $good) {
				$all = $all + $good['count'];
				$all_price = $all_price + $good['price'];
			}
			$class = new stdClass();
			$class->goods = $data;
			$class->in_basket = $all . ' СЕРТИФИКАТА НА СУММУ ' . $all_price . ' РУБЛЕЙ';
			$class = json_encode($class);
			$cookie = new CHttpCookie('goods', $class);
			$cookie->expire = time() + 60 * 60 * 24 * 180;
			Yii::app()->request->cookies['goods'] = $cookie;
			echo $all . ' СЕРТИФИКАТА НА СУММУ ' . $all_price . ' РУБЛЕЙ';
		}

	}

	public function actionIsAuth()
	{
		if (Yii::app()->user->id) {
			echo 1;
		} else {
			echo 0;
		}
	}
}