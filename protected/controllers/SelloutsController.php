<?php

class SelloutsController extends Controller
{
	public $layout = '//layouts/main';

	public function actionView($id)
	{
		$this->render('view', array('model' => Sellouts::model()->findByPk($id)));
	}

	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('Sellouts', array('pagination' => array('pageSize' => 25)));
		$this->render('index', array('dataProvider' => $dataProvider));
	}
}