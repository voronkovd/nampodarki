<?php

class OrdersController extends Controller
{

	public function filters()
	{
		return array('accessControl', 'postOnly + delete');
	}


	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions' => array('my'),
				'roles' => array(Users::AUTH_USER)
			),
			array(
				'allow',
				'actions' => array('basket'),
				'users' => array('*')
			),
			array('deny', 'users' => array('*')),
		);
	}

	public function actionMy()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('user_id', Yii::app()->user->id);
		$model = Orders::model()->findAll($criteria);
		$this->render('my', array('model' => $model));
	}

	public function actionBasket()
	{
		$message = '';
		$model = (isset(Yii::app()->request->cookies['goods']->value)) ?
			Yii::app()->request->cookies['goods']->value : '';
		$model = json_decode($model, true);
		if (isset($_POST['Orders'])) {
			$cookie = (isset(Yii::app()->request->cookies['goods']->value)) ?
				Yii::app()->request->cookies['goods']->value : '';
			$cookie = json_decode($cookie, true);
			if (!empty($cookie)) {
				$new = new Orders();
				$new->user_id = Yii::app()->user->id;
				$new->current_status = Yii::app()->params['order_new'];
				$new->save();
				foreach ($cookie['goods'] as $good) {
					$goods = new OrderCertificates();
					$goods->order_id = $new->getPrimaryKey();
					$goods->certificate_id = $good['id'];
					$goods->price_id = $good['price_id'];
					$goods->save();

				}
				unset(Yii::app()->request->cookies['goods']);
				$message = 'Спасибо за заказ, наши операторы свяжутся с вами в ближайшее время';
			}
		}
		$this->render('basket', array('model' => $model, 'message' => $message));
	}

}