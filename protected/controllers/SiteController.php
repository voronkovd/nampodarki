<?php

class SiteController extends Controller
{
	public $layout = '//layouts/main';

	public function actions()
	{
		return array(
			'captcha' => array('class' => 'CCaptchaAction', 'backColor' => 0xFFFFFF),
			'stat' => array('class' => 'CViewAction'),
		);
	}

	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('Certificates');
		$this->render('//certificates/index', array('dataProvider' => $dataProvider));
	}

	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				$this->render('error', $error);
			}
		}
	}

	public function actionContact()
	{
		$model = new ContactForm;
		if (isset($_POST['ContactForm'])) {
			$model->attributes = $_POST['ContactForm'];
			if ($model->validate()) {
				$name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
				$subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
				$headers = "From: $name <{$model->email}>\r\n" .
					"Reply-To: {$model->email}\r\n" .
					"MIME-Version: 1.0\r\n" .
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
				Yii::app()->user->setFlash(
					'contact',
					'Ваше письмо отправлено. Мы неприменно вам ответим в ближайшее время.'
				);
				$this->refresh();
			}
		}
		$this->render('contact', array('model' => $model));
	}

	public function actionSearch()
	{
		$model = new SearchCertificates();
		$model->attributes = $_GET['SearchCertificates'];
		$dataProvider = $model->getResult();
		$this->render('//certificates/index', array('dataProvider' => $dataProvider, 'model' => $model));
	}
}