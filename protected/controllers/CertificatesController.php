<?php

class CertificatesController extends Controller
{


	public function actionView($id)
	{
		$this->render('view', array('model' => Certificates::model()->findByPk($id)));
	}

	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('Certificates');
		$this->render('index', array('dataProvider' => $dataProvider));
	}
}
