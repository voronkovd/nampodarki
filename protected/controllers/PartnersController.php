<?php

class PartnersController extends Controller
{

	public function actionView($id)
	{
		$this->render('view', array('model' => Partners::model()->findByPk($id)));
	}

	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('Partners', array('pagination' => array('pageSize' => 25)));
		$this->render('index', array('dataProvider' => $dataProvider));
	}
}