<?php

class PostsController extends Controller
{

	public function actionView($id)
	{
		$this->render('view', array('model' => News::model()->findByPk($id)));
	}

	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('Posts', array('pagination' => array('pageSize' => 25)));
		$this->render('index', array('dataProvider' => $dataProvider));
	}
}