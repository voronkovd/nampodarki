<?php

class m130922_220442_add_certificates extends CDbMigration
{
	public function up()
	{
		$this->createTable(
			"categories",
			array(
				"id" => "pk",
				"name" => "VARCHAR(100) NOT NULL COMMENT 'Наименование категории'",
				"status" => "TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Статус: 1 - активна, 2 - заблокирована'",
			),
			"COMMENT 'Категории сертификатов'"
		);

		$this->createTable(
			"certificate_categories",
			array(
				"id" => "pk",
				"category_id" => "INT(11) NOT NULL COMMENT 'Связь с таблицей категорий'",
				"certificate_id" => "INT(11) NOT NULL COMMENT 'Связь с таблицей сертификатов'",
			),
			"COMMENT 'Связь Категории сертификатов'"
		);

		$this->createTable(
			"certificates",
			array(
				"id" => "pk",
				"name" => "VARCHAR(100) NOT NULL COMMENT 'Наименование сертификата'",
				"desc" => "VARCHAR(255) NULL COMMENT 'Описание'",
				"image" => "VARCHAR(36) DEFAULT NULL COMMENT 'Изображение'",
				"status" => "TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Статус: 1 - активен, 2 - заблокирован'",
				"delivery" => "TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Статус: 1 - нет, 2 - да'",
				"date_add" => "DATETIME NOT NULL COMMENT 'Дата добавления'",
				"lifetime" => "DATE NOT NULL COMMENT 'Время жизни сертификата'",
				"partner_id" => "INT(11) DEFAULT NULL COMMENT 'Связь с таблицей партнеров'",
				"user_id" => "INT(11) NOT NULL COMMENT 'Связь с таблицей пользователей партнеров'",
			),
			"COMMENT 'Таблица сертификатов'"
		);

		$this->createTable(
			"sellouts",
			array(
				"id" => "pk",
				"name" => "VARCHAR(100) NOT NULL COMMENT 'Название'",
				"anons" => "VARCHAR(255) NULL COMMENT 'Описание'",
				"image" => "VARCHAR(36) DEFAULT NULL COMMENT 'Изображение'",
				"discount" => "TINYINT(2) DEFAULT 99 COMMENT 'процент скидки'",
				"status" => "TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Статус: 1 - активен, 2 - заблокирован'",
				"date_add" => "DATETIME NOT NULL COMMENT 'Дата добавления'",
				"lifetime" => "DATE NOT NULL COMMENT 'Время жизни сертификата'",
				"partner_id" => "INT(11) DEFAULT NULL COMMENT 'Связь с таблицей партнеров'",
				"user_id" => "INT(11) NOT NULL COMMENT 'Связь с таблицей пользователей партнеров'",
			),
			"COMMENT 'Таблица акций'"
		);

		$this->createTable(
			"who_is_certificates",
			array(
				"id" => "pk",
				"name" => "VARCHAR(100) NOT NULL COMMENT 'Тэг Кому'",
			),
			"COMMENT 'Таблица тэгов кому'"
		);

		$this->createTable(
			"when_is_certificates",
			array(
				"id" => "pk",
				"name" => "VARCHAR(100) NOT NULL COMMENT 'Тэг когда'",
				"date_when" => "VARCHAR(5) NOT NULL COMMENT 'когда'",
			),
			"COMMENT 'Таблица тэгов когда'"
		);

		$this->createTable(
			"when_is_certificate_relations",
			array(
				"id" => "pk",
				"when_id" => "INT(11) NOT NULL COMMENT 'Тэг когда'",
				"certificate_id" => "INT(11) NOT NULL COMMENT 'Связь с сертификатом'",
			),
			"COMMENT 'Связь таблиц когда'"
		);

		$this->createTable(
			"who_is_certificate_relations",
			array(
				"id" => "pk",
				"who_id" => "INT(11) NOT NULL COMMENT 'Тэг кому'",
				"certificate_id" => "INT(11) NOT NULL COMMENT 'Связь с сертификатом'",
			),
			"COMMENT 'Связь таблиц кому'"
		);

		$this->createTable(
			"certificate_prices",
			array(
				"id" => "pk",
				"price" => "FLOAT(11, 2) DEFAULT 0.00 COMMENT 'Цена'",
			),
			"COMMENT 'Таблица цен'"
		);

		$this->createTable(
			"certificate_price_relations",
			array(
				"id" => "pk",
				"price_id" => "INT(11) NOT NULL COMMENT 'Тэг кому'",
				"certificate_id" => "INT(11) NOT NULL COMMENT 'Связь с сертификатом'",
			),
			"COMMENT 'Связь таблиц кому'"
		);

		$this->createTable(
			"sellout_certificates",
			array(
				"id" => "pk",
				"sellout_id" => "INT(11) NOT NULL COMMENT 'Связь с таблицей акций'",
				"certificate_price_relation_id" => "INT(11) NOT NULL COMMENT 'Связь с таблицей сертификатов'",
			),
			"COMMENT 'Таблица сертификатов для акции'"
		);

		$this->createIndex('certificates_user', 'certificates', 'user_id');
		$this->addForeignKey('certificates_user_key', 'certificates', 'user_id', 'users', 'id');
		$this->createIndex('certificates_partner', 'certificates', 'partner_id');
		$this->addForeignKey('certificates_partner_key', 'certificates', 'partner_id', 'partners', 'id');
		$this->createIndex('sellout_user_id_index', 'sellouts', 'user_id');
		$this->addForeignKey('sellout__user_key', 'sellouts', 'user_id', 'users', 'id');
		$this->createIndex('sellout_partner_id_index', 'sellouts', 'partner_id');
		$this->addForeignKey('sellout_partner_key', 'sellouts', 'partner_id', 'partners', 'id');
		$this->createIndex('sellout_certificates_sellout_index', 'sellout_certificates', 'sellout_id');
		$this->addForeignKey(
			'sellout_certificates_sellout_key',
			'sellout_certificates',
			'sellout_id',
			'sellouts',
			'id'
		);
		$this->createIndex(
			'sellout_certificates_certificate_index',
			'sellout_certificates',
			'certificate_price_relation_id'
		);
		$this->addForeignKey(
			'sellout_certificates_certificate_key',
			'sellout_certificates',
			'certificate_price_relation_id',
			'certificate_price_relations',
			'id'
		);
		$this->createIndex('certificates_who_id_index', 'who_is_certificate_relations', 'who_id');
		$this->addForeignKey(
			'certificates_who_key',
			'who_is_certificate_relations',
			'who_id',
			'who_is_certificates',
			'id'
		);
		$this->createIndex('who_is_certificate_relations_index', 'who_is_certificate_relations', 'certificate_id');
		$this->addForeignKey(
			'who_is_certificate_relations_key',
			'who_is_certificate_relations',
			'certificate_id',
			'certificates',
			'id'
		);
		$this->createIndex('certificates_when_id_index', 'when_is_certificate_relations', 'when_id');
		$this->addForeignKey(
			'certificates_when_key',
			'when_is_certificate_relations',
			'when_id',
			'when_is_certificates',
			'id'
		);
		$this->createIndex('when_is_certificate_relations_index', 'when_is_certificate_relations', 'certificate_id');
		$this->addForeignKey(
			'when_is_certificate_relations_key',
			'when_is_certificate_relations',
			'certificate_id',
			'certificates',
			'id'
		);
		$this->createIndex('price_certificate_price_relations_index', 'certificate_price_relations', 'price_id');
		$this->addForeignKey(
			'price_certificate_price_relations_key',
			'certificate_price_relations',
			'price_id',
			'certificate_prices',
			'id'
		);
		$this->createIndex(
			'certificate_certificate_price_relations_index',
			'certificate_price_relations',
			'certificate_id'
		);
		$this->addForeignKey(
			'certificate_certificate_price_relations_key',
			'certificate_price_relations',
			'certificate_id',
			'certificates',
			'id'
		);

		$this->createIndex('certificate_categories_category_relations_index', 'certificate_categories', 'category_id');
		$this->addForeignKey(
			'certificate_categories_category_relations_key',
			'certificate_categories',
			'category_id',
			'categories',
			'id'
		);
		$this->createIndex('certificate_categories_relations_index', 'certificate_categories', 'certificate_id');
		$this->addForeignKey(
			'certificate_categories_relations_key',
			'certificate_categories',
			'certificate_id',
			'certificates',
			'id'
		);
	}

	public function down()
	{
		echo "m130922_220442_add_certificates does not support migration down.\n";

		return false;
	}
}
