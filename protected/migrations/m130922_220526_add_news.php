<?php

class m130922_220526_add_news extends CDbMigration
{
	public function up()
	{
		$this->createTable(
			"news",
			array(
				"id" => "pk",
				"title" => "VARCHAR(50) NOT NULL COMMENT 'Заголовок'",
				"image" => "VARCHAR(36) DEFAULT NULL COMMENT 'Изображение'",
				"anons" => "VARCHAR(128) NOT NULL COMMENT 'Анонс'",
				"content" => "TEXT NOT NULL  COMMENT 'Текст новости'",
				"date_add" => "DATETIME NOT NULL COMMENT 'Дата добавления'",
				"partner_id" => "INT(11) DEFAULT NULL COMMENT 'Связь с таблицей пользователей партнеров'",
			),
			"COMMENT 'Таблица новостей'"
		);
		$this->createTable(
			"posts",
			array(
				"id" => "pk",
				"title" => "VARCHAR(50) NOT NULL COMMENT 'Заголовок'",
				"image" => "VARCHAR(36) DEFAULT NULL COMMENT 'Изображение'",
				"anons" => "VARCHAR(128) NOT NULL COMMENT 'Анонс'",
				"content" => "TEXT NOT NULL  COMMENT 'Текст статьи'",
				"date_add" => "DATETIME NOT NULL COMMENT 'Дата добавления'",
			),
			"COMMENT 'Таблица статей'"
		);
		$this->createTable(
			"commentaries",
			array(
				"id" => "pk",
				"parent_id" => "INT(11) DEFAULT 0 COMMENT 'Родитель комментария'",
				"content" => "TEXT NOT NULL  COMMENT 'Текст новости'",
				"date_add" => "DATETIME NOT NULL COMMENT 'Дата добавления'",
				"user_id" => "INT(11) DEFAULT NULL COMMENT 'Связь с таблицей пользователей партнеров'",
			),
			"COMMENT 'Таблица новостей'"
		);

		$this->createTable(
			"gallery_news",
			array(
				"id" => "pk",
				"news_id" => "INT(11) NOT NULL COMMENT 'Связь с таблицей партнеров'",
				"image_name" => "VARCHAR(36) NOT NULL COMMENT 'сслыка на логотип'",
				"status" => "TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Статус: 1 - активен, 2 - заблокирован'",
			),
			"COMMENT 'Таблица компаний партнеров'"
		);
		$this->createTable(
			"gallery_posts",
			array(
				"id" => "pk",
				"posts_id" => "INT(11) NOT NULL COMMENT 'Связь с таблицей партнеров'",
				"image_name" => "VARCHAR(36) NOT NULL COMMENT 'сслыка на логотип'",
				"status" => "TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Статус: 1 - активен, 2 - заблокирован'",
			),
			"COMMENT 'Таблица компаний партнеров'"
		);
		$this->createIndex('news_user', 'news', 'partner_id');
		$this->addForeignKey('news_user_key', 'news', 'partner_id', 'partners', 'id');
		$this->createIndex('comment_user', 'commentaries', 'user_id');
		$this->addForeignKey('comment_user_key', 'commentaries', 'user_id', 'users', 'id');
		$this->createIndex('gallery_news_index', 'gallery_news', 'news_id');
		$this->addForeignKey(
			'gallery_news_key',
			'gallery_news',
			'news_id',
			'news',
			'id'
		);
		$this->createIndex('gallery_posts_index', 'gallery_posts', 'posts_id');
		$this->addForeignKey(
			'gallery_posts_key',
			'gallery_posts',
			'posts_id',
			'posts',
			'id'
		);
	}

	public function down()
	{
		echo "m130922_220526_add_news does not support migration down.\n";

		return false;
	}
}