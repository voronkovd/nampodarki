<?php

class m130922_220433_add_users extends CDbMigration
{
	public function up()
	{
		$this->createTable(
			"users",
			array(
				"id" => "pk",
				"email" => "VARCHAR(100) NOT NULL COMMENT 'Email'",
				"password" => "VARCHAR(32) NOT NULL COMMENT 'Пароль (md5 + salt)'",
				"role" => "TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Роли: 1 - Пользователь, 2 - менеджер, 3 - суперпользователь'",
				"status" => "TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Статус: 1 - активен, 2 - заблокирован'",
				"partner_id" => "INT(11) DEFAULT NULL COMMENT 'Связь с таблицей партнеров'",
			),
			"COMMENT 'Таблица пользователей'"
		);

		$this->createTable(
			"user_profiles",
			array(
				"id" => "pk",
				"surname" => "VARCHAR(50) NOT NULL COMMENT 'Фамилия'",
				"name" => "VARCHAR(50) NOT NULL COMMENT 'Имя'",
				"patronymic" => "VARCHAR(50) NOT NULL COMMENT 'Отчество'",
				"phone" => "VARCHAR(11) DEFAULT NULL COMMENT 'Телефон'",
				"news_subscription" => "TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Подписка на новости: 1 - нет, 2 - да'",
				"certificates_subscription" => "TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Подписка на сертификаты: 1 - нет, 2 - да'",
				"posts_subscription" => "TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Подписка на статьи: 1 - нет, 2 - да'",
				"user_id" => "INT(11) NOT NULL COMMENT 'Связь с таблицей пользователей'",
			),
			"COMMENT 'Таблица профайлов пользователей'"
		);
		$this->createIndex('user_profile', 'user_profiles', 'user_id');
		$this->addForeignKey('user_profile_key', 'user_profiles', 'user_id', 'users', 'id');
		$this->createIndex('user_partner', 'users', 'partner_id');
		$this->addForeignKey('partner_user_key', 'users', 'partner_id', 'partners', 'id');
	}

	public function down()
	{
		echo "m130922_220433_add_users does not support migration down.\n";

		return false;
	}
}
