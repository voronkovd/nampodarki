<?php

class m130922_220520_add_orders extends CDbMigration
{
	public function up()
	{
		$this->createTable(
			"orders",
			array(
				"id" => "pk",
				"user_id" => "INT(11) NOT NULL COMMENT 'Связь с таблицей пользователей'",
				"manager_id" => "INT(11) DEFAULT NULL COMMENT 'Связь с таблицей пользователей (менеджер)'",
				"current_status" => "TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Текущий статус'",
			),
			"COMMENT 'Таблица заказов'"
		);

		$this->createTable(
			"order_statuses",
			array(
				"id" => "pk",
				"order_id" => "INT(11) NOT NULL COMMENT 'Связь с таблицей заказов'",
				"status" => "TINYINT(1) DEFAULT 1 COMMENT 'статус заказа: 1 - Новый, 2 - Оплачен, 3 - Отказ'",
				"date_add" => "DATETIME NOT NULL COMMENT 'дата статуса'",
			),
			"COMMENT 'Таблица стасуов заказов'"
		);
		$this->createTable(
			"order_certificates",
			array(
				"id" => "pk",
				"order_id" => "INT(11) NOT NULL COMMENT 'Связь с таблицей заказов'",
				"certificate_id" => "INT(11) DEFAULT NULL COMMENT 'Связь с таблицей сертификатов'",
				"price_id" => "INT(11) NOT NULL COMMENT 'Связь с таблицей цен'",
				"price_value" => "FLOAT(11, 2) DEFAULT 0.00 COMMENT 'Статичная цена'",
			),
			"COMMENT 'Таблица товаров в заказе'"
		);
		$this->createIndex('order_user', 'orders', 'user_id');
		$this->addForeignKey('orders_user_key', 'orders', 'user_id', 'users', 'id');
		$this->createIndex('manager_user', 'orders', 'manager_id');
		$this->addForeignKey('manager_user', 'orders', 'manager_id', 'users', 'id');
		$this->createIndex('order_statuses_order', 'order_statuses', 'order_id');
		$this->addForeignKey('order_statuses_order_key', 'order_statuses', 'order_id', 'orders', 'id');
		$this->createIndex('order_certificates_order', 'order_certificates', 'order_id');
		$this->addForeignKey('order_certificates_order_key', 'order_certificates', 'order_id', 'orders', 'id');
		$this->createIndex('order_certificates_price', 'order_certificates', 'price_id');
		$this->addForeignKey(
			'order_certificates_price_key',
			'order_certificates',
			'price_id',
			'certificate_prices',
			'id'
		);
		$this->createIndex('order_certificates_certificate_index', 'order_certificates', 'certificate_id');
		$this->addForeignKey(
			'order_certificates_certificate_key',
			'order_certificates',
			'certificate_id',
			'certificates',
			'id'
		);
	}

	public function down()
	{
		echo "m130922_220520_add_orders does not support migration down.\n";

		return false;
	}
}