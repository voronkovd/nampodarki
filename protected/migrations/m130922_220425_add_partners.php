<?php

class m130922_220425_add_partners extends CDbMigration
{
	public function up()
	{
		$this->createTable(
			"partners",
			array(
				"id" => "pk",
				"name" => "VARCHAR(100) NOT NULL COMMENT 'Наименование компании партнера'",
				"image_name" => "VARCHAR(36) NOT NULL COMMENT 'сслыка на логотип'",
				"status" => "TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'Статус: 1 - активен, 2 - заблокирован'",
			),
			"COMMENT 'Таблица компаний партнеров'"
		);
		$this->createTable(
			"partner_galleries",
			array(
				"id" => "pk",
				"file_name" => "VARCHAR(36) NOT NULL COMMENT 'Изображение'",
				"partner_id" => "INT(11) NOT NULL COMMENT 'Связь с таблицей партнеров'",
			),
			"COMMENT 'Таблица галлерии партнеров'"
		);
		$this->createIndex('partner_names', 'partners', 'name');
		$this->createIndex('partner_gallery', 'partner_galleries', 'partner_id');
		$this->addForeignKey(
			'partner_gallery_key',
			'partner_galleries',
			'partner_id',
			'partners',
			'id'
		);
	}

	public function down()
	{
		echo "m130922_220425_add_partners does not support migration down.\n";

		return false;
	}
}
