<?php

class m130922_222406_add_default extends CDbMigration
{
	public function up()
	{
		$user = array(
			'email' => 'admin@nampodarki.ru',
			'password' => 'e83913267c0c76690ceffe4799e1cdf8',
			'role' => 5,
			'status' => 1
		);
		$userProfile = array(
			"surname" => "Админов",
			"name" => "Админ",
			"patronymic" => "Админыч",
			"phone" => null,
			"user_id" => 1,
		);
		$this->insert('users', $user);
		$this->insert('user_profiles', $userProfile);
	}

	public function down()
	{
		echo "m130922_222406_add_default does not support migration down.\n";

		return false;
	}
}