<?php $this->beginContent('//layouts/main'); ?>
<?php if (!empty($this->menu)): ?>
	<div class="span3">
		<div class="well sidebar-nav">
			<?php
			$this->beginWidget(
				'zii.widgets.CPortlet',
				array('title' => 'Управление:', 'titleCssClass' => 'nav-header')
			);
			$this->widget(
				'zii.widgets.CMenu',
				array(
					'encodeLabel' => false,
					'items' => $this->menu,
					'htmlOptions' => array('class' => 'nav nav-list')
				)
			);
			$this->endWidget();
			?>
		</div>
	</div>
<?php endif; ?>
<?php if (!empty($this->menu)) { ?>
	<div class="span9">
<?php } else { ?>
	<div class="span12">
<?php } ?>
	<div class="hero-unit">
		<br/>
		<?php $this->widget(
			'zii.widgets.CBreadcrumbs',
			array(
				'homeLink' => CHtml::link('Главная', '/', array('target' => '_blank')),
				'separator' => ' &rarr; ',
				'links' => $this->breadcrumbs,
			)
		); ?>
		<?php echo $content ?>
	</div>
	</div>
<?php $this->endContent(); ?>