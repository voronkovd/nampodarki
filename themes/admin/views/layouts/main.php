<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<title><?php echo $this->title; ?></title>
</head>
<body>
<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<?php if (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::MANAGER) {
				$name_admin = 'Админка 1.0';
			} else {
				$name_admin = 'Партнерка 1.0';
			} ?>
			<span class="brand"><span
					class="second"><?php echo $name_admin; ?></span></span>

			<div class="nav-collapse">
				<?php
				$this->widget(
					'ext.MyMenu',
					array(
						'htmlOptions' => array('class' => 'nav'),
						'activeCssClass' => 'active',
						'items' => array(
							array(
								'label' => 'Сайт',
								'url' => array('/site/'),
								'linkOptions' => array('target' => '_BLANK')
							),
							array(
								'label' => 'Пользователи',
								'url' => array('/admin/Users'),
								'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app(
									)->user->role == Users::PARTNER_ADMIN)
							),
							array(
								'label' => 'Сертификаты',
								'url' => array(
									'/admin/certificates'
								)
							),
							array(
								'label' => 'Новости',
								'url' => array('/admin/news')
							),
							array(
								'label' => 'Заказы',
								'url' => array('/admin/orders'),
								'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app(
									)->user->role == Users::MANAGER)
							),
//                            array(
//                                'label' => 'Статистика',
//                                'url' => array('/admin/statistic'),
//                                'visible' => (Yii::app()->user->role == Users::ADMIN || Yii::app()->user->role == Users::PARTNER_ADMIN)
//                            ),
							array(
								'label' => 'Выход',
								'url' => array('/users/auth/logout')
							)
						),
					)
				);
				?>
			</div>
		</div>
	</div>
</div>
<div class="row-fluid">
	<?php echo $content ?>
</div>
</body>
</html>