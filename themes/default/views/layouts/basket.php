<div class="cart inline">
	<div class="column label">
		В корзине:
	</div>
	<div class="column counter">
		<div class="view-basket">
		<?php $cookie = (isset(Yii::app()->request->cookies['goods']->value)) ?
			Yii::app()->request->cookies['goods']->value : '';
		$cookie = json_decode($cookie, true);
		if (!empty($cookie) && !empty($cookie['in_basket'])) {
			echo $cookie['in_basket'];
		}
		?>
			</div>
		<br/><br/>
		<a href="<?php echo Yii::app()->createUrl('basket'); ?>" class="order-button rounded">Заказать</a>
	</div>
</div>