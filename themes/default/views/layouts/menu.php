<div class="menu clear">
	<?php $this->widget(
		'ext.MyMenu',
		array(
			'itemCssClass' => 'column',
			'activeCssClass' => 'current',
			'items' => array(
				array('label' => 'О компании', 'url' => array('/about')),
				array('label' => 'Каталог сертификатов', 'url' => array('/site')),
				array('label' => 'Наши партнеры', 'url' => array('/partners')),
				array('label' => 'Сотрудничество', 'url' => array('/partnership')),
				array('label' => 'Клиентам', 'url' => array('/clients')),
			),
		)
	); ?>
</div>