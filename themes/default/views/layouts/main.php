﻿<?php
Yii::app()->clientscript
	->registerCssFile(Yii::app()->theme->baseUrl . '/_design/css/site.css')
	->registerCssFile(Yii::app()->theme->baseUrl . '/_design/css/styles.css')
	->registerCssFile(Yii::app()->theme->baseUrl . '/_design/css/common_use.css')
	->registerCssFile(Yii::app()->theme->baseUrl . '/_design/css/eventCalendar.css')
	->registerCoreScript('jquery')
	->registerCoreScript('jquery.ui')
	->registerScriptFile(Yii::app()->theme->baseUrl . '/js/site.js')
	->registerScriptFile(Yii::app()->theme->baseUrl . '/js/pageObject.js')
	->registerScriptFile(Yii::app()->theme->baseUrl . '/js/globals.js')
	->registerScriptFile(Yii::app()->theme->baseUrl . '/js/UI.js')
	->registerScriptFile(Yii::app()->theme->baseUrl . '/js/reconstructFields.js')
	->registerScriptFile(Yii::app()->theme->baseUrl . '/js/hashComponent.js')
	->registerScriptFile(Yii::app()->theme->baseUrl . '/js/storageComponent.js')
	->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.eventCalendar.js')
?>﻿
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="Content-Style-Type" content="text/css">
	<meta http-equiv="Access-Control-Allow-Origin" content="*">
	<meta content='True' name='HandheldFriendly'/>
	<meta content='width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;' name='viewport'/>
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<meta name="viewport" content="width=device-width"/>
	<title>Нам подарки</title>
</head>
<script>
	$(document).ready(function () {
		$('.addtoBasket').click(function () {
			id = $('.addtoBasket').attr('id');
			price_id = $('.addtoBasket').attr('price_id');
			price = $('.addtoBasket').attr('price');
			name = $('.addtoBasket').attr('name');
			data = {'id': id, 'price_id': price_id, 'price': price, 'name':name}
			$.ajax({
				url: '/ajax/addToBasket',
				data: data,
				success: function (html) {
					$('.view-basket').html(html);
				}
			});

		});
	});
</script>
<body>
<div id="Body" class="table">
	<div class="row" id="PnTop">
		<div class="column lf-column">&nbsp;</div>
		<div class="column rt-column">&nbsp;</div>
	</div>
	<div class="row" id="PnMn">
		<?php echo $this->renderPartial('//layouts/left'); ?>
		<div class="column rounded" id="PnRt">
			<div id="PnTopBlock" class="inline">
				<div class="column socials">
					&nbsp;
				</div>
				<div class="column quote">
					<h2>"Нет более ценного и желанного подарка, чем положительная эмоция"</h2>
					<span>Александр Дюма</span>
				</div>
				<div class="column login-block">
					<?php echo $this->renderPartial('//layouts/auth'); ?>
					<?php echo $this->renderPartial('//layouts/basket'); ?>
				</div>
				<?php echo $this->renderPartial('//layouts/menu'); ?>
				<?php echo $this->renderPartial('//layouts/filter'); ?>
			</div>
			<div id="PnCn">
				<?php echo $content; ?>
			</div>
		</div>
		<?php echo $this->renderPartial('//layouts/footer'); ?>
	</div>
</div>
</body>
</html>