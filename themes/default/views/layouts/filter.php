<div class="small-filter">
	<?php echo CHtml::beginForm(Yii::app()->createUrl('site/search'), 'GET'); ?>
	<label>Найти сертификат: от</label>
	<?php echo CHtml::textField(
		'SearchCertificates[start]',
		!empty($_GET['SearchCertificates']['start']) ? $_GET['SearchCertificates']['start'] : '',
		array(
			'class' => 'input-text'
		)
	); ?>
	<span class="separator">&nbsp;</span>
	<label>до</label>
	<?php echo CHtml::textField(
		'SearchCertificates[end]',
		!empty($_GET['SearchCertificates']['end']) ? $_GET['SearchCertificates']['end'] : '',
		array('class' => 'input-text')
	); ?>
	<?php echo CHtml::submitButton('Найти', array('class' => 'input-button rounded-rt')); ?>
	<a href="<?php echo Yii::app()->createUrl('site/fullSearch'); ?>">Расширенный поиск</a>
	<?php echo CHtml::endForm(); ?>
</div>