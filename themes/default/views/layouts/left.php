<div class="column rounded" id="PnLf">
	<div class="calendar_main"></div>
	<div class="lf-block logo"><img src=""/></div>
	<div class="lf-block">
		<h3>АКЦИИ</h3>
		<?php $sellots = Sellouts::getLastThree(); ?>
		<?php if (!empty($sellots)): ?>
			<?php foreach ($sellots as $sellout): ?>
				<p><?php echo UrlHelper::cutString($sellout->name); ?><br/>
					<a href="<?php echo Yii::app()->createUrl('sellouts/view', array('id' => $sellout->id)); ?>">подробнее...</a>
				</p>
			<?php endforeach; endif; ?>
		<br/>
		<a href="<?php echo Yii::app()->createUrl('sellouts'); ?>">ВСЕ АКЦИИ</a>
	</div>
	<div class="lf-block">
		<h3>Новости</h3>
		<?php $news = News::getLastThree(); ?>
		<?php if (!empty($news)): ?>
			<?php foreach ($news as $new): ?>
				<div class="text">
					<p class="date"><?php echo date('Y-m-d', strtotime($new->date_add)); ?></p>

					<p><?php echo UrlHelper::cutString($new->title); ?><br/>
						<a href="<?php echo Yii::app()->createUrl('news/view', array('id' => $new->id)); ?>">
							<?php echo UrlHelper::cutString($new->anons, 100); ?>...
						</a>
					</p>
				</div>
			<?php endforeach; endif; ?>
		<a href="<?php echo Yii::app()->createUrl('news'); ?>">ВСЕ НОВОСТИ</a>
	</div>
	<div class="lf-block">
		<h3>Интересное</h3>
		<?php $posts = Posts::getLastThree(); ?>
		<?php if (!empty($posts)): ?>
			<?php foreach ($posts as $post): ?>
				<p class="date"><?php echo date('Y-m-d', strtotime($post->date_add)); ?></p>

				<p><?php echo UrlHelper::cutString($post->title); ?><br/>
					<a href="<?php echo Yii::app()->createUrl('posts/view', array('id' => $post->id)); ?>">
						<?php echo UrlHelper::cutString($post->anons, 100); ?>...
					</a>
				</p>
			<?php endforeach; endif; ?>
		<a href="<?php echo Yii::app()->createUrl('posts'); ?>">ВСЕ Статье</a>
	</div>
</div>
<script>
	$('.calendar_main').eventCalendar({
		eventsjson: '/ajax/calendar',
		onlyOneDescription: false
	});
</script>