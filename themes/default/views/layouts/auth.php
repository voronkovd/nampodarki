<div class="links">
	<span class="starter">&nbsp;</span>
	<?php if (Yii::app()->user->isGuest) { ?>
		<a href="<?php echo Yii::app()->createUrl('login'); ?>" id="loginButton">Войти</a>
		<a href="<?php echo Yii::app()->createUrl('contact'); ?>">Задать вопрос</a>
	<?php } elseif (Yii::app()->user->role == Users::PARTNER_MANAGER || Yii::app()->user->role == Users::PARTNER_ADMIN
	) { ?>
		<a href="<?php echo Yii::app()->createUrl('admin'); ?>" id="loginButton">Пaртнерка</a>
		<a href="<?php echo Yii::app()->createUrl('logout'); ?>" id="loginButton">Выйти</a>
	<?php } elseif (Yii::app()->user->role == Users::MANAGER || Yii::app()->user->role == Users::ADMIN) { ?>
		<a href="<?php echo Yii::app()->createUrl('admin'); ?>" id="loginButton">Админка</a>
		<a href="<?php echo Yii::app()->createUrl('logout'); ?>" id="loginButton">Выйти</a>
	<?php } else { ?>
		<a href="<?php echo Yii::app()->createUrl('self'); ?>" id="loginButton">Профиль</a>
		<a href="<?php echo Yii::app()->createUrl('logout'); ?>" id="loginButton">Выйти</a>
	<?php } ?>
	<div class="clear"></div>
</div>