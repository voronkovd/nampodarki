﻿/*class*/ //Work with form fields
function storageComponent(pageObject, settings) {
	this.fields = {};
	this.pageObject = null;
	this.presetValues = {};
	var This = this;
	var PageObj = null;
	
	function __construct(pObject, settingsList) {
		pageObject = pageObject;
		pageObject.storage = This;
	}
	
	this.mount = function() {
	};
	
	this.setItem = function( name, value ) {
		if( isObject(value) || isArray(value) ) {
			value = JSON.stringify(value);
		}
		window.localStorage.setItem(name, value);
	};
	
	this.deleteItem = function( name ) {
		try {
			window.localStorage.removeItem(name);
		} catch(e) {
		}
	};
	
	this.getItem = function( name, default_val ) {
		try {
			var itemValue = window.localStorage.getItem(name);
			try {
				return $.parseJSON(itemValue);
			} catch (e) {
				return itemValue;
			}
		} catch( e ) {
			if( !empty(default_val) ) {
				return default_val;
			} else {
				return false;
			}
		}
	};
	
	function _isLocalStorageAvailable() {
		try {
			return 'localStorage' in window && window['localStorage'] !== null;
		} catch (e) {
			return false;
		}
	}
	
	__construct(pageObject, settings);
}