﻿/**/
function templateReplace( /*object*/replaces, /*string*/template ) {
	var html = template;
	jQuery.extend(replaces, gTemplateReplaces);
	if( !empty(replaces) ) {
		for( var i in replaces ) {
			var str = '%%%' + i + '%%%';
			html = html.replace(new RegExp(str, 'g'), replaces[i]);
		}
	}
	return html;
}
/**/
function getAppRoot() {
	return location.protocol + '//' + location.hostname;
}

function empty( x ) {
	if( isObject(x) ) {
		for( var i in x ) {
			return false;
		}
		return true;
	}
	return ( typeof x == 'undefined' || x == false || x == null || x == '' || x == 0 || (isArray(x) && x.length == 0) );
}
/**/
function hasProperty(obj, property) {
	if( isArray(obj) ) {
		return typeof obj[property] != 'undefined';
	}
	return !empty(obj) && property in obj;
}
/**/
function getElemFromList(list, key, value) {
	for( var i = 0, l = list.length; i < l; i++ ) {
		var elem = list[i];
		if( elem[key] == value ) {
			return i;
		}
	}
	return -1;
}
/**/
function elemLength(list) {
	if( isObject(list) ) {
		var l = 0;
		for( var i in list ) {
			l ++;
		}
		return l;
	}
	if( isArray(list) ) {
		return list.length;
	}
	return 0;
}
/**/
function capitalize(/*string*/s){
	if(!empty(s)) {
		return s.toLowerCase().replace( /\b./g, function(a){ return a.toUpperCase(); } );
	} else {
		return s;
	}
};
/**/
(function(){
    var types = ['Array', 'Function', 'Object', 'String', 'Number'],
        typesLength = types.length;
    while (typesLength--) {
        window['is' + types[typesLength]] = (function(type){
            return function(o) {
                return !!o && ( Object.prototype.toString.call(o) === '[object ' + type + ']' );
            }
        })(types[typesLength]);
    }
})();
/**/
function getObjectParams(/*{}*/default_object, /*[]*/list) {
	var index = 0;
	for( var i in default_object ) {
		default_object.i = !empty(list[index]) ? list[index] : null;
	}
	return default_object;
}
/**/
function checkEmpty(value, default_val) {
	if(!empty(value)) {
		return value;
	}
	if(!empty(default_val)) {
		return default_val;
	}
	return '';
}
/**/
function getZero(/*int*/num) {
	return (num < 10 ? '0' : '') + num;
}