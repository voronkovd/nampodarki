﻿/*class*/
function pageObject() {
	this.container = null;
	this.tempContainer = null;
	this.mainContainer = null;
	this.errorBlock = null;
	this.loader = null;
	this.slideEffect = 'replace';
	this.errorAutoFade = false;
	this.slideDuration = 600;
	this.variables = {};
	this.variables_tmp = {};
	this.templates = {};
	this.templateLoaded = false;
	this.scrollers = {};
	
	this.isDisabled = false;	
	this.content = '';
	this.oldContent = '';
	var This = this;	
	
	var POPUP_BUTTON_TAG = '<a />';
	var POPUP_BUTTON_SET = {href: '#'};
	
	/**///Mount object
	this.mount = function( /*{main_cont, cont, error_block, main_loader, errorAutoFade}*/settings ) {
		if( !empty(settings['main_cont']) ) {
			if( isString(settings['main_cont']) && $('#' + settings['main_cont']).length > 0 ) {
				this.mainContainer = $('#' + settings['main_cont']);
			} else if( $(settings['main_cont']).length > 0 ) {
				this.mainContainer = $(settings['main_cont']);
			}
		}
		if( !empty(settings['cont']) ) {
			this.container = $('#PnMn');
		}
		if( !empty(settings['error_block']) && $('#' + settings['error_block']).length > 0 ) {
			this.container = $('#' + settings['main_cont']);
		}
		if( !empty(settings['main_loader']) && $('#' + settings['main_loader']).length > 0 ) {
			this.container = $('#' + settings['main_cont']);
		}
		if( !empty(settings['errorAutoFade']) ) {
			this.errorAutoFade = settings['errorAutoFade'];
		}
		this.getTemplates('main_templates');
	};
	/**/
	this.extendAddon = function(addon_name, settings) {
		if( empty(addon_name) || empty(window[addon_name]) ) {
			return;
		}
		if( !empty(settings) ) {
			This[addon_name] = new window[addon_name](this, settings);
		} else {
			This[addon_name] = new window[addon_name](this, {});
		}
	};
	/*Function takes a templates by ajax*/
	this.getTemplates = function(/*string*/templateName, callback) {
		var appRoot = '/themes/default/_design/';//(gAppRoot || '.') + '/_design/';
		$.ajax({
			url: appRoot + 'templates/' + templateName + '.html',
			async: false,
			dataType: 'html',
			success: function(ret) {
				This.templates = {};
				if( $('#PnTemplates').length === 0 ) {
					$(document.body).append('<div id="PnTemplates" />');
					$('#PnTemplates').css({height: '0px', overflow: 'hidden'});
				}
				$('#PnTemplates').html(ret);
				for( var i = 0, childs = $('#PnTemplates').children(), l = childs.length; i < l; i++ ) {
					if( $(childs[i]).attr('id').indexOf('template_') != -1 ) {
						if( $.browser.msie && $.browser.version < 9 ) {
							This.templates[$(childs[i]).attr('id')] = _IEReplaceInnerHTML(childs[i], false);
						} else {
							This.templates[$(childs[i]).attr('id')] = $(childs[i]).html();
						}
					}
				}
				$('#PnTemplates').html('');
				This.templateLoaded = true;
			}
		});
	};
	
	function _IEReplaceInnerHTML(obj) {
		var zz = obj.innerHTML ? String(obj.innerHTML) : obj;
		var z  = zz.match(/(<.+[^>])/g);
		if(z) {
			for ( var i = 0; i < z.length; (i = i+1) ) {
				var y;
				var zSaved = z[i];
				var attrRE = /\=[a-zA-Z\.\:\[\]_\(\)\&\$\%#\@\!0-9\/]+[?\s+|?>]/g;
				y = z[i].match(attrRE);
				if(y) {
					var j = 0, len = y.length;
					while(j < len) {
						var replaceRE = /(\=)([a-zA-Z\.\:\[\]_\(\)\&\$\%#\@\!0-9\/]+)?([\s+|?>])/g;
						var replacer = function() {
							var args = Array.prototype.slice.call(arguments);
							return '="' + args[2] + '"' + args[3];
						};
						z[i] = z[i].replace(y[j],y[j].replace(replaceRE,replacer));
						j += 1;
					}
				}
				zz = zz.replace(zSaved,z[i]);
			}
		}
		return zz;
	}
	
	this.getTemplate = function(template_name) {
		if( !empty(this.templates['template_' + template_name]) ) {
			return this.templates['template_' + template_name];
		}
		return '';
	};
	
	this.setVariables = function(/*{}*/variables) {
		for(var i in variables) {
			this.variables_tmp[i] = variables[i];
			this.variables[i] = variables[i];
		}
	};
	
	this.resetVariables = function( variables ) {
		var new_variables = {};
		for( var i = 0, l = variables.length; i < l; i++ ) {			
			if( This.getVariable(variables[i]) ) {
				new_variables[variables[i]] = This.getVariable(variables[i]);
			}
		}
		This.setVariables(new_variables);
	};
	
	this.unsetVariables = function() {
		for( var i in this.variables ) {
			delete this.variables[i];
		}
		this.variables = {};
	};
	
	this.unsetVariablesTmp = function() {
		for( var i in this.variables_tmp ) {
			delete this.variables_tmp[i];
		}
		this.variables_tmp = {};
	};
	
	this.setVariablesFromBuffer = function() {
		this.unsetVariables();
		for(var i in this.variables_tmp) {
			this.variables[i] = this.variables_tmp[i];
			delete this.variables_tmp[i];
		}
		this.variables_tmp = {};
	};
	
	this.getVariable = function(varName) {
		if(!empty(this.variables)) {
			if(hasProperty(this.variables, varName)) {
				return this.variables[varName];
			}
			return null;
		}
		return null;
	};
	
	this.createPopup = function(/*{id, title, text, buttons, parentCont}*/settings, redraw) {
		if( !empty(redraw) ) {
			$('#' + settings['id']).remove();			
		}
		var popupVual;
		var popupBlock
		if( $('#' + settings['id']).length === 0 ) {
			popupVual = $('<div/>', {
				id: 'popupVual_' + settings['id']
			});
			popupVual.addClass('popupVual');
			popupBlock = $('<div/>', {
				id: settings['id']
			});
			popupBlock.addClass('popup');
			
			if( !empty(settings['title']) ) {
				var h1 = $('<h3/>');
				h1.html(settings['title']);
				$(popupBlock).append(h1);
			}
			if( !empty(settings['text']) ) {
				var p = $('<p/>');
				p.html(settings['text']);
				$(popupBlock).append(p);
			}
			if( !empty(settings['content']) ) {
				var cont = $('<div/>', {id: settings['id'] + '_cont'});
				if(!empty(settings['content']['className'])) {
					cont.addClass(settings['content']['className']);
				}
				if(!empty(settings['content']['html'])) {
					$(cont).html(settings['content']['html']);
				}
				$(popupBlock).append(cont);
			}
			var buttons = $('<div/>', {});			
			buttons.addClass('buttons');
			if( empty( settings['hideButtons'] ) ) {
				var button;
				if( !empty(settings['buttons']) ) {
					var buttons_list = [];
					for( var i in settings['buttons'] ) {
						var func = settings['buttons'][i];
						button = $(POPUP_BUTTON_TAG, POPUP_BUTTON_SET);
						button.addClass('button');
						if( hasProperty(settings['buttons'][i], 'text') ) {
							button.html(settings['buttons'][i]['text']);
						} else {
							button.html(i);
						}
						buttons_list.push({button: button, func: settings['buttons'][i]});
					}
					
					$(buttons_list).each(function(i) {
						$(buttons).append(this.button);
						if( !empty(this.func) ) {
							$(this.button).click(function() {
								buttons_list[i].func.call(this);
							});
						} else {
							$(this.button).click(function() {
								This.hidePopup(settings['id']);
							});
						}
					});
				} else {
					button = $('<div/>', {});
					button.addClass('button');
					button.html('Cancel');
					$(button).click(function() {
						This.hidePopup(settings['id'], redraw);
					});
					$(buttons).append(button);
				}
			} else {
				buttons = '';
			}
			$(popupBlock).append(buttons);
			if( !hasProperty(settings, 'withoutVual') || empty(settings['withoutVual']) ) {
				if( hasProperty(settings, 'parentCont') && $(settings['parentCont']).length > 0 ) {
					$(settings['parentCont']).append(popupVual);
				} else {
					This.mainContainer.append(popupVual);
				}
			}
		}
		$(document.body).css('overflow', 'hidden');
		if( hasProperty(settings, 'withoutVual') && !empty(settings['withoutVual']) ) {
			_showPopup( settings, popupBlock );
		} else {
			popupVual
				.css('top', parseInt($(document).scrollTop(), 10))
				.fadeIn(function() {
					_showPopup( settings, popupBlock );
				});
		}
	};
	
	function _showPopup( settings, popupBlock ) {
		if( hasProperty(settings, 'parentCont') && $(settings['parentCont']).length > 0 ) {
			$(settings['parentCont']).append(popupBlock);
		} else {
			This.mainContainer.append(popupBlock);
		}
		This.showPopup(settings['id']);
		This.unSetScrolls();
		if( !empty(settings['afterCreate']) ) {
			if( !empty(settings['scrollbar']) ) {
				for( var j = 0, k = settings['scrollbar'].length; j < k; j++ ) {
					var scrollbarSettings = settings['scrollbar'][j];
					if( $('#' + scrollbarSettings['mainWrap']).length > 0 ) {
						var scrollTemp = new dw_scrollObj(scrollbarSettings['mainWrap'], scrollbarSettings['innerWrap']);
						This.setScroll(scrollbarSettings['mainWrap'], scrollTemp);
						scrollTemp.buildScrollControls(scrollbarSettings['scroller'], 'v', 'mouseover', true);
						scrollTemp.updateDims();
					}
				}
			}
			settings['afterCreate'].apply(this);
		}
	}
	
	this.setScroll = function( name, scroller ) {
		This.scrollers[name] = scroller;
	};
	
	this.unSetScrolls = function() {
		for( var i in This.scrollers ) {
			delete This.scrollers[i];
		}
	};
	
	this.showPopup = function(popupName) {
		if( $('#' + popupName).length > 0 ) {
			$('#' + popupName).css('margin-left', -parseInt($('#' + popupName).width(), 10)/2);
			$('#' + popupName).css('margin-top', parseInt($(document).scrollTop(), 10) - parseInt($('#' + popupName).height(), 10)/2);
			$('#' + popupName).css('visibility', 'visible');
		}
	};
	
	this.hidePopup = function(popupName, redraw, withoutVual) {
		if( $('#' + popupName).length > 0 ) {
			var popupId = '#popupVual_' + popupName;
			if( !empty(redraw) ) {
				if( empty(withoutVual) ) {
					$(popupId).remove();
				}
				$('#' + popupName).remove();
			} else {
				if( empty(withoutVual) ) {
					$(popupId).hide();
				}
				$('#' + popupName).hide();
			}
			$(document.body).css('overflow', 'visible');
		}
	};
	
	this.removePopup = function(popupName) {
		delete gScrollWindow;
		gScrollWindow = null;
		$('#' + popupName).remove();
	};
	
	this.setErrorBlock = function(block) {
		this.errorBlock = block;
	};
	
	this.setErrorText = function(text) {
		this.errorBlock.html(text);
	};
	
	this.showLoader = function() {
		if(!empty(this.loader)) {
			this.container.css('opacity', 0.4);
			this.loader.vual.show();
			this.loader.block.show();
		} else {
			this.loader = {};
			var vual = $('<div/>', {
					id: 'loaderVual'
				});
			var loader = $('<div/>', {
					id: 'loaderBlock'
				});
			var top = parseInt($('#PnTop').find('#topBar').height(), 10);
			var cont_height = parseInt(this.mainContainer.height(), 10);
			$(vual)
				.height(cont_height - top)
				.css('top', top);
			this.mainContainer.append(vual);
			this.mainContainer.append(loader);
			$(loader).css({
				'top': (cont_height + $('#PnTop').height() - parseInt($(loader).height(), 10))/2,
				'left': (parseInt(this.mainContainer.width(), 10) - parseInt($(loader).width(), 10))/2
			});
			$(this.mainContainer.children()[0]).before(vual);
			$(this.mainContainer.children()[0]).before(loader);
			this.loader.vual = $(vual);
			this.loader.block = $(loader);
		}
	};
	
	this.hideLoader = function() {
		if(!empty(this.loader)) {
			this.container.css('opacity', 1);
			this.loader.vual.hide();
			this.loader.block.hide();
		}
	};
	
	this.showError = function() {
		this.errorBlock.show();
	};
	
	this.hideError = function() {
		this.errorBlock.hide();
	};
	
	this.hideDisplay = function() {
		this.container.hide();
	};
	
	this.remount = function(/*{content, error_block, main_loader, errorAutoFade, effect, orient}*/settings ) {
		this.oldContent = this.content;
		this.content = settings['content'];
		this.errorAutoFadeError = false;
		this.slideEffect = settings['effect'];
		window.scrollTo(0, 0);
		this.changeDisplay(settings['orient'], settings['callback']);
	};
	
	this.changeDisplay = function( orient, callback ) {
		switch(this.slideEffect) {
			case 'fade':
				break;
			case 'slide':
				if( orient == 'left' ) {
					This.tempContainer.html(this.content);
					This.tempContainer.css('margin-left', -1600);
					This.container.before(This.tempContainer);
					var tmp = This.tempContainer;
					This.tempContainer = This.container;
					This.container = tmp;
					This.container.animate({'margin-left': 0}, This.slideDuration, function() {
						This.tempContainer.html('');
						if( !empty(callback) ) {
							(callback).call(this);
						}
					});
				} else {
					This.tempContainer.html(this.content);
					This.container.animate({'margin-left': -1600}, This.slideDuration, function() {
						var tmp = This.tempContainer;
						This.tempContainer = This.container;
						This.container = tmp;
						This.container.after(This.tempContainer);
						This.tempContainer.css({'margin-left': 0});
						This.tempContainer.html('');
						if( !empty(callback) ) {
							(callback).call(this);
						}
					});
				}
				break;
			case 'replace':
				This.unsetVariablesTmp();
				This.container.html(this.content);
				if(!empty(this.oldContent)) {
					this.hideLoader();
				}
				if( !empty(callback) ) {
					(callback).call(this);
				}
				This.setVariablesFromBuffer();
			default:
				break;
		}
		This.isDisabled = false;
	};
	
	this.changeDisplayAJAX = function( id, content, callback, callback_args ) {
		switch(this.slideEffect) {
			case 'fade':
			case 'slide':
			case 'replace':
				if( $('#' + id).length > 0 ) {
					$('#' + id).html(content);
					if( !empty(callback) ) {
						(callback).apply(this, callback_args);
					}
				}
			default:
				break;
		}
		This.isDisabled = false;
	};
	/*Function makes network operations, and performs a callback function*/
	this.networkConnection = function( /*{url, params, hideLoader}*/AJAXSettings, /*{func, params}*/callbackSettings, /*{funcName, params}*/ hashSettings ) {
		This.menu.setDisabled(true);
		clearTimeout(gIntTimer);
		if(empty(AJAXSettings) || empty(AJAXSettings['hideLoader']) ) {
			this.showLoader();
		}
		if( this.templateLoaded ) {
			if( !empty(AJAXSettings) ) {
				$.ajax({
					 url: AJAXSettings['url'],
					 type: 'POST',
					 dataType: 'JSON',
					 data: requestString(AJAXSettings['params']),
					 success: function(response) {
						if( hasProperty(response, 'status') && response['status'] == 'error' ) {
							createErrorPopup(response['msg'], true);
						} else {
							gPageObject.hashController.setLocationHash(hashSettings);
							if(!empty(callbackSettings['params'])) {
								callbackSettings['func'].call(this, response, callbackSettings['params']);
							} else {
								callbackSettings['func'].call(this, response);
							}
							gIsDisabled = false;
							This.hideLoader();
							This.menu.setDisabled(false);
						}
					 },
					 error: function(e) {
						This.hideLoader();
						createErrorPopup('Error: request is incorrect', true);
						hidePopupWithFunc('error', true, function() {});
						This.menu.previousItemSelect();
						This.menu.setDisabled(false);
					 }
				});
			} else {
				var response = null;
				gPageObject.hashController.setLocationHash(hashSettings);
				if(!empty(callbackSettings['params'])) {
					callbackSettings['func'].call(this, response, callbackSettings['params']);
				} else {
					callbackSettings['func'].call(this, response, null);
				}
				This.hideLoader();
				This.menu.setDisabled(false);
			}
		} else {
			gIntTimer = setTimeout( function() { This.networkConnection(AJAXSettings, callbackSettings, hashSettings) }, 100 );
		}
	};
}