﻿function reconstructFields(pageObject, classSettings) {
	var This = this;
	var PageObj = null;
	this.presets = {};
	
	var selectTemplate = '<div class="select" id="%%%ID%%%">'
	+ '%%%TITLE%%%'
	+ '%%%OPTIONS%%%'
	+ '<input type="hidden" value="%%%VALUE%%%" name="%%%NAME%%%" />'
	+ '</div>';
	
	var selectTitleTemplate = '<div class="title">'
	+ '<div class="title_block">%%%TITLE%%%</div>'
	+ '</div>';
	
	var selectOptionsBlockTemplate = '<div class="options">'
	+ '<div class="select-items">%%%OPTIONS%%%</div>'
	+ '</div>';
	
	var selectOptionTemplate = '<div class="option%%%CURRENT%%%" sb:value="%%%VALUE%%%">%%%OPTION%%%</div>';
	
	function __construct(pObject, settings) {
		PageObj = pObject;
		pageObject.reconstructFields = This;
		if( empty(settings) || empty(settings['notAutoSet']) ) {
			var oldChangeDisplay = PageObj.changeDisplay;
			PageObj.changeDisplay = function() {
				oldChangeDisplay.apply(this, arguments);
				This.mount(settings);
			};
		}
	}
	
	this.presetHandlers = function( settings ) {
		if( !empty(settings) ) {
			for( var i in settings ) {
				if( isFunction(settings[i]) ) {
					this.presets[i] = settings[i];
				}
			}
		}
	};
	
	this.mount = function( settings, id ) {
		if( !empty(settings) ) {
			for( var i in settings ) {
				switch(i) {
					case 'hidden':
						break;
					case 'checkbox':
						_setCheckboxHandlers(id);
						break;
					case 'radio':
						_setRadioHandlers(id);
						break;
					case 'textarea':
						_setTextareaHandlers(id);
						break;
					case 'select':
						_setSelectHandlers(id);
						break;
					case 'text':
						_setTextHandlers(id);
						break;
					default:
						break;
				}
			}
		} else {
			_setAllHandlers(id);
		}
		$(document).on('click touchstart', function(event) {
			if( $(event.target).parents('.select').length === 0  ) {
				closeAllSelects();
			}
		});
	};
	
	function closeAllSelects() {
		$('.select').find('.drop-down').each(function() {
			$(this).slideUp(300);
			$(this).removeClass('drop-down');
		});		
	}
	
	function _setAllHandlers(id) {
		_setCheckboxHandlers(id);
		_setRadioHandlers(id);
		_setTextareaHandlers(id);
		_setSelectHandlers(id);
		_setTextHandlers(id);
	}
	
	function _setTextHandlers(id) {
	}
	
	function _setTextareaHandlers(id) {
	}
	
	function _setSelectHandlers(id) {
		var elemsList = !empty(id) ? $('#' + id).find('select') : $('select');
		elemsList.each(function(index) {
			var sel = $(this);
			var select_width = sel.innerWidth();
			var title = 'Select';
			var value = sel.val();
			var current_value = '';
			var name;
			if( !empty(this.name) ) {
				name = this.name;
			} else {
				name = 'custom_select_' + index;
			}
			
			var optionsList = sel.find('option');
			var default_title = '';
			if( sel.find('option.select_title') ) {
				default_title = sel.find('option.select_title').text();
			}
			
			var createOptBlock = function( val, text, current ) {
				return templateReplace({'VALUE': val, 'OPTION': text, 'CURRENT': current ? ' current' : '' }, selectOptionTemplate);
			};			
			
			var select_options_str = '';
			if( sel.hasClass('auto-fill') ) {
				var selected = parseInt($(optionsList[optionsList.length - 1]).val(), 10);
				var minVal = parseInt($(optionsList[0]).val(), 10);
				var step = parseInt($(optionsList[1]).val(), 10);
				var maxVal = parseInt($(optionsList[2]).val(), 10);
				if( empty($(optionsList[3]).val()) ) {
					select_options_str += createOptBlock('', '', false);
				}
				for( var i = parseInt(minVal, 10); i <= parseInt(maxVal, 10); i += step ) {
					select_options_str += createOptBlock(i, i, false);
					if( i == selected ) {
						title = i;
					}
				}
				if( !empty(selected) ) {
					value = selected;
				}
			} else {
				optionsList.each(function() {
					if( !$(this).hasClass('no-detect') ) {
						select_options_str += createOptBlock($(this).val(), $(this).text(), $(this).is(':selected'));
						if( $(this).is(':selected') ) {
							current_value = $(this).text();
						}
						if( empty(default_title) && $(this).val() == sel.val() ) {
							title = $(this).text();
						}
					}
				});
				value = sel.val();
			}
			if( default_title === '' ) {
				if( current_value !== '' ) {
					title = current_value;
				} else {
					title = $(optionsList[0]).text();
				}
			} else {
				title = default_title;
			}
			var select_title = templateReplace({'TITLE': title}, selectTitleTemplate);
			var select_options = templateReplace({'OPTIONS': select_options_str}, selectOptionsBlockTemplate);
			var select_temp = templateReplace({'TITLE': select_title, 'OPTIONS': select_options, 'VALUE': value, 'NAME': name, 'ID': name}, selectTemplate);
			sel.before(select_temp);
			sel.remove();
			var select_block = $('#' + name);
			var title_block = select_block.find('.title_block');
			var input = select_block.find('input[name="' + name + '"]');
			var options_block = select_block.find('.options');
			var options_list = select_block.find('.option');
			var options_items_block = select_block.find('.select-items');
			var paddings = parseInt(title_block.innerWidth(), 10) - parseInt(title_block.width(), 10);
			if( parseInt($('#' + name).css('min-width'), 10) > 0 ) {
				if( parseInt($('#' + name).css('min-width'), 10) < (select_width + paddings) ) {
					$('#' + name).width(select_width + paddings);
				} else {
					$('#' + name).width(parseInt($('#' + name).css('min-width'), 10));
				}
			} else if( select_width > 0 ) {
				$('#' + name).width(select_width + paddings);
			}
			title_block.click(function() {
				if( $(select_block).hasClass('disabled') ) {
					return;
				}
				if( $(this).width() > 0 ) {
					options_block.width($(this).innerWidth());
				}
				if( !options_block.hasClass('drop-down') ) {
					closeAllSelects();
					options_block.slideDown(300);
					options_block.addClass('drop-down');
					if( options_block.find('.current').length > 0 ) {
						var scrollTop = 0;
						options_list.each(function(ind) {
							if( $(this).hasClass('current') ) {
								scrollTop = ind * $(this).height();
							}
						});
						$(options_items_block).scrollTop(scrollTop);
					}
				} else {
					options_block.slideUp(300);
					options_block.removeClass('drop-down');
				}
			});
			
			options_list.each(function() {
				$(this).bind('click', function() {
					options_list.removeClass('current');
					if( default_title === '' ) {
						title_block.text($(this).text());
					} else if( !empty($(this).attr('sb:value')) ) {
						title_block.addClass('selected_value');
					} else {
						title_block.removeClass('selected_value');
					}
					$(this).addClass('current');
					input.val($(this).attr('sb:value'));
					_setPresets(name, $(input).val(), select_block[0]);
					options_block.slideUp(300);
					options_block.removeClass('drop-down');
				});
			});
			if( options_list.length > 10 ) {
				options_items_block.addClass('select-scrolled');
			}
			options_block.hide();
		});
	}
	
	function _setPresets(name, val, e, checked) {
		if( !empty(This.presets) && !empty(This.presets[name]) ) {
			This.presets[name].call(this, val, e, checked);
		}
	}
	
	function _setCheckboxHandlers(id) {
		var elemsList = !empty(id) ? $('#' + id).find('input[type="checkbox"]') : $('input[type="checkbox"]');
		elemsList.each(function() {
			var input = this;
			var name = input.name;
			$(this).parent().addClass('checkbox');
			if( $(this).parent('label').length === 0 ) {
				var label = $('<label />', {'class': 'checkbox', 'for': $(this).attr('id')});
				$(this).before(label);
				$(label).append(this);
				$(label).append(input.value);
			}
			if ($(this).attr('checked') === 'checked') {
				$(this).parent().addClass('clicked-label');
			}
			$(this).parent().click(function() {
				var label = $(this);
				label.toggleClass('checkbox-clicked');
				if(label.find('input:checked').length === 0 && !label.hasClass('checkbox-clicked')) {
					label.removeClass('clicked-label');
					if( !empty(name) ) {
						_setPresets(name, $(input).val(), input, false);
					}
				} else if(label.find('input:checked').length == 1 && !label.hasClass('checkbox-clicked')) {
					label.addClass('clicked-label');
					if( !empty(name) ) {
						_setPresets(name, $(input).val(), input, true);
					}
				}
			});
		});
	}
	
	function _setRadioHandlers(id) {
		var elemsList = !empty(id) ? $('#' + id).find('input[type="radio"]') : $('input[type="radio"]');
		elemsList.each(function() {
			var input = this;
			var name = input.name;
			$(this).parent().addClass('radio');
			if( $(this).parent('label').length === 0 ) {
				var label = $('<label />', {'class': 'radio', 'for': $(this).attr('id')});
				$(this).before(label);
				$(label).append(this);
			}
			if( $(this).is(':checked') ) {
				$(this).parent().addClass('radio-checked');
			}
			var radio = $(this);
			radio.parent().click(function() {
				if( !empty(radio.attr('disabled')) ) {
					return;
				}
				$(this).toggleClass('checked-radio');
				if($(this).find('input:checked').length === 0 && !$(this).hasClass('checked-radio')) {
					$(this).removeClass('radio-checked');
					setTimeout(function() {_setPresets(name, $(input).val(), input)}, 100);
				} else if($(this).find('input:checked').length == 1 && !$(this).hasClass('checked-radio')) {
					$('input[name="' + name + '"]').parent().removeClass('radio-checked');
					$(this).addClass('radio-checked');
					setTimeout(function() {_setPresets(name, $(input).val(), input);}, 100);
				}
			});
		});
	}
	
	this.setCheckboxChecked = function(checkbox) {
		$(checkbox).find('input').attr('checked', true);
		$(checkbox).addClass('clicked-label');
	};
	
	this.setCheckboxUnchecked = function(checkbox) {
		$(checkbox).find('input').attr('checked', false);
		$(checkbox).removeClass('clicked-label');
	};
	
	this.isCheckedCheckbox = function(checkbox) {
		return $(checkbox).attr('checked');
	};
	
	__construct(pageObject, classSettings);
}