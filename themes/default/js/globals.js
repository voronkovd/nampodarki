﻿var gIntTimer = 0;
var gIsDisabled = false;
var gAppRoot = getAppRoot();
var gAppAJAXUrl = '/';

var gTemplateReplaces = {
	'IMAGETAG': '<img',
	'DEFCLOSETAG': '/>'
};

var gHashSettings = {
	callback: function(funcName, params) {
		if( empty(params) ) {
			return;
		}
		if( hasProperty(params, funcName) ) {
			gPageObject.menu.selectItem(params[funcName]);
		}
	},
	params: {
		'doSearchEvent': 'SearchEvent',
		'doSearchResults': 'SearchEvent',
		'doSelectEventType': 'SearchEvent',
		'doSelectCity': 'SearchEvent',
		'doAppening': 'Appening',
		'doFavorites': 'Favorites'
	},
	separator: '/'
};

var gPageObjectSettings = {
	main_cont: 'Body',
	cont: 'mainContainer',
	errorAutoFade: false
};

var histAPI = !!(window.history && history.pushState);
var gPageObject = new pageObject();