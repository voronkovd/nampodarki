﻿/**/
function hashController( pageObject, settingsList ) {
	this.currentHash = '';
	this.disabled = false;
	this.separator = '';
	this.callback = null;
	this.params = null;
	var This = this;
	
	function __constructor(pObject, Settings) {
		pageObject.hashController = This;
		This.mount(Settings);
	
		window.onhashchange = function() {
			if( This.disabled === true ) {
				This.disabled = false;
				return;
			}
			var parsedHash = This.parseHashParams();
			if( empty(parsedHash['funcName']) ) {
				return;
			}
			window[parsedHash['funcName']].call(this, parsedHash['params']);
			if( !empty(This.callback) ) {
				This.callback.call(this, parsedHash['funcName'], This.params);
			}
		};
	}
	
	this.mount = function( settings ) {
		if(hasProperty(settings, 'separator')) {
			this.separator = settings['separator'];
		}
		if( hasProperty(settings, 'callback') ) {
			This.callback = settings['callback'];
		}
		if( hasProperty(settings, 'params') ) {
			This.params = settings['params'];
		}
	};
	
	this.setLocationHash = function(hashSettings) {//*string*/funcName, /*{prnum, dayId, itemId}*/params
		if( empty(hashSettings) || !hasProperty(hashSettings, 'funcName') ) {
			return;
		}
		var funcName = hashSettings['funcName'];
		var params = hashSettings['params'];
		this.disabled = true;
		var hash = funcName;
		if( isObject(params) ) {
			for( var i in params ) {
				if( isObject(params[i]) ) {
					hash += '/' + i + '=' + JSON.stringify(params[i]);
				} else {
					hash += '/' + i + '=' + params[i];
				}	
			}
		}
		if (location.hash == "#" + hash) {this.disabled = false; return false;}
		var url = window.location.href;
		url = url.replace(/(#.*)/, "");
		url += "#" + hash;
		if (histAPI) {
			history.pushState(null, null, url);
		}
		window.location.href = url;
		return false;
	};
	
	this.parseHash = function(separator) {
		if( empty(separator) ) {
			separator = this.separator;
		}
		var hash = location.hash;
		hash = hash.replace(/#/, "");
		return hash.split(separator);
	};
	
	this.parseHashParams = function() {
		var keys_values = this.parseHash();
		var funcName = keys_values.shift();
		if( empty(funcName) || !hasProperty(window, funcName) ) {
			funcName = null;
		}
		var params = {};
		for( var i = 0, l = keys_values.length; i < l; i ++ ) {
			var key_value = keys_values[i].split('=');
			if( key_value.length > 1 ) {
				var newhtml = key_value[1].replace(/(")(')(\w+)/gi, "'$3'");
				if( !empty(key_value[1]) ) {
					try {
						eval('value = ' + newhtml);
					} catch(e) {
						eval('value = \'' + newhtml + '\'');
					}
				} else {
					value = '';
				}
				params[key_value[0]] = value;
			}
		}
		return {funcName: funcName, params: params};
	};
	
	this.hashHasParameter = function(paramName) {
		var parsedHash = this.parseHashParams();
		return !empty(parsedHash['params']) && hasProperty(parsedHash['params'], paramName);
	};
	
	this.hashSpecParameterValue = function(paramName, separator) {
		if( empty(paramName) ) {
			return null;
		}
		var keys_values = this.parseHash(separator);
		for( var i = 0, l = keys_values.length; i < l; i ++ ) {
			var key_value = keys_values[i].split('=');
			if( key_value.length > 1 ) {
				var newhtml = key_value[1].replace(/(")(')(\w+)/gi, "'$3'");
				if( !empty(key_value[1]) ) {
					try {
						eval('value = ' + newhtml);
					} catch(e) {
						eval('value = \'' + newhtml + '\'');
					}
				} else {
					value = '';
				}
				if( key_value[0] == paramName ) {
					return key_value[1];
				}
			}
		}
		return null;
	};
	
	this.hashParameterValue = function(paramName) {
		var parsedHash = this.parseHashParams();
		if(empty(parsedHash['params']) || !hasProperty(parsedHash['params'], paramName)) {
			return null;
		}
		return parsedHash['params'][paramName];
	};
	
	this.urlSearchParameterValue = function(paramName) {
		var urlSearch = location.search;
		if( empty(urlSearch) || urlSearch == '?' ) {
			return null;

		}
		urlSearch = urlSearch.replace(/\?/, "");
		var keys_values = urlSearch.split('&');
		var params = {};
		for( var i = 0, l = keys_values.length; i < l; i ++ ) {
			var key_value = keys_values[i].split('=');
			if( key_value[0] == paramName ) {
				return key_value[1];
			}
		}
		return null;
	};
	__constructor(pageObject, settingsList);
}