﻿$(document).ready(function() {
	gPageObjectSettings.main_cont = document.body;
	gPageObject.mount(gPageObjectSettings);
	gPageObject.extendAddon('hashController', gHashSettings);
	gPageObject.extendAddon('reconstructFields', {});
	gPageObject.extendAddon('storageComponent', {});
});

function showDetailPopup(id) {
	if( $('#giftItemDetail').length > 0 ) {
		gPageObject.hidePopup('giftItemDetail', true);
		gPageObject.createPopup({
				id: 'giftItemDetail',
				content: {
					html: gPageObject.getTemplate('detail_gift')
				},
				hideButtons: true,
				afterCreate: function() {
					gPageObject.reconstructFields.mount();
					$('#giftItemDetail').find('a.close').click(function() {
						gPageObject.hidePopup('giftItemDetail', true);
						return false;
					});
					$('#popupVual_giftItemDetail').height($('#Body').height());
				}
		}, true);
	} else {
		gPageObject.createPopup({
			id: 'giftItemDetail',
			content: {
				html: gPageObject.getTemplate('detail_gift')
			},
			hideButtons: true,
			afterCreate: function() {
				gPageObject.reconstructFields.mount();
				$('#giftItemDetail').find('a.close').click(function() {
					gPageObject.hidePopup('giftItemDetail', true);
					return false;
				});
				$('#popupVual_giftItemDetail').height($('#Body').height());
			}
		}, true);
	}
	return false;
}